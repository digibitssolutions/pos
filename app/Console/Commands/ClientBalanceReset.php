<?php

namespace App\Console\Commands;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Enums\EnumsList;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

use function App\shopId;

class ClientBalanceReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:reset {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reseting Clients Balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $clients = Client::where('shop_id', $id)->update(['total_remaining_balance' => '0']);
    }
}
