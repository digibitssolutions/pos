<?php

namespace App\Http\Builders;

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

use function App\shopId;

class ClientsReportsQueryBuilder implements Builder
{

    private $queryBuilder_;
    private $request_;

    public function __construct(BaseModel $queryBuilder, $request)
    {
        $this->queryBuilder_ = $queryBuilder;
        $this->request_ = $request;
    }

    public function build()
    {
        $queryBuilder = $this->queryBuilder_::from('clients')
            ->where('clients.shop_id', shopId())
            ->select([
                DB::raw(
                    'CONCAT(clients.first_name," ",clients.last_name) as client_name'
                ),
                'clients.phone',
                'clients.city',
                'clients.total_remaining_balance',
            ]);
        return $queryBuilder;
    }

    public function reset()
    {
        $this->queryBuilder_ = $this->queryBuilder_->newInstance();
    }
}
