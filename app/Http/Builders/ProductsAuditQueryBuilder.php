<?php

namespace App\Http\Builders;

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

use function App\shopId;

class ProductsAuditQueryBuilder implements Builder
{

    private $queryBuilder_;
    private $request_;

    public function __construct(BaseModel $queryBuilder, $request)
    {
        $this->queryBuilder_ = $queryBuilder;
        $this->request_ = $request;
    }

    public function build()
    {
        $queryBuilder = $this->queryBuilder_::from('products')
            ->Join('master_stock as ms', 'ms.product_id', '=', 'products.id')
            ->select([
                DB::raw(
                    'products.purchase_price * ms.master_stock as amount'
                ),
                'products.product_name',
                'ms.master_stock as quantity',
                'products.purchase_price',
            ])
            ->groupBy('ms.product_id')
            ->where('products.shop_id', shopId());
        return $queryBuilder;
    }

    public function reset()
    {
        $this->queryBuilder_ = $this->queryBuilder_->newInstance();
    }
}
