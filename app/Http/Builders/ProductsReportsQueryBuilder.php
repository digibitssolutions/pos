<?php

namespace App\Http\Builders;

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

use function App\shopId;

class ProductsReportsQueryBuilder implements Builder
{

    private $queryBuilder_;
    private $request_;

    public function __construct(BaseModel $queryBuilder, $request)
    {
        $this->queryBuilder_ = $queryBuilder;
        $this->request_ = $request;
    }

    public function build()
    {
        $queryBuilder = $this->queryBuilder_::from('products')
            ->where('products.shop_id', shopId())
            ->leftJoin('shops as sh', 'sh.id', '=', 'products.shop_id')
            ->select([
                'sh.shop_name as shop_id',
                'products.product_name',
                'products.sale_price',
                'products.expiry_date',
                'products.created_at',
            ])
            ->when(!empty($this->request_->product_name), function ($queryBuilder_) {
                return $queryBuilder_->where('products.product_name', 'like', '%' . $this->request_->product_name . '%');
            })
            ->when(!empty($this->request_->code), function ($queryBuilder_) {
                return $queryBuilder_->where('products.code', $this->request_->code);
            })
            ->when(!empty($this->request_->unit_price), function ($queryBuilder_) {
                return $queryBuilder_->where('products.unit_price', $this->request_->unit_price);
            })
            ->when(!empty($this->request_->category_id), function ($queryBuilder_) {
                return $queryBuilder_->where('products.category_id', $this->request_->category_id);
            })
            ->when(!empty($this->request_->measurement_unit), function ($queryBuilder_) {
                return $queryBuilder_->where('products.measurement_unit', $this->request_->measurement_unit);
            })
            ->when(!empty($this->request_->from_expiry_date), function ($queryBuilder_) {
                return $queryBuilder_->whereDate('products.expiry_date', '>=', $this->request_->from_expiry_date);
            })
            ->when(!empty($this->request_->to_expiry_date), function ($queryBuilder_) {
                return $queryBuilder_->whereDate('products.expiry_date', '<=', $this->request_->to_expiry_date);
            })
            ->when(!empty($this->request_->from), function ($queryBuilder_) {
                return  $queryBuilder_->whereDate('products.created_at', '>=', $this->request_->from);
            })
            ->when(!empty($this->request_->to), function ($queryBuilder_) {
                return  $queryBuilder_->whereDate('products.created_at', '<=', $this->request_->to);
            });
        return $queryBuilder;
    }

    public function reset()
    {
        $this->queryBuilder_ = $this->queryBuilder_->newInstance();
    }
}
