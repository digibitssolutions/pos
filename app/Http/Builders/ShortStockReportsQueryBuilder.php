<?php

namespace App\Http\Builders;

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

use function App\shopId;

class ShortStockReportsQueryBuilder implements Builder
{

    private $queryBuilder_;
    private $request_;

    public function __construct(BaseModel $queryBuilder, $request)
    {
        $this->queryBuilder_ = $queryBuilder;
        $this->request_ = $request;
    }

    public function build()
    {
        $queryBuilder = $this->queryBuilder_::from('short_stocks as ss')
            ->where('ss.shop_id', shopId())
            ->leftJoin('products as p', 'p.id', '=', 'ss.product_id')
            ->select([
                'p.product_name',
                'ss.old_stock as current_stock',
                'ss.required_stock',
            ]);
        return $queryBuilder;
    }

    public function reset()
    {
        $this->queryBuilder_ = $this->queryBuilder_->newInstance();
    }
}
