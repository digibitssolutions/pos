<?php

namespace App\Http\Builders;

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

use function App\shopId;

class SupplyReportQueryBuilder implements Builder
{

    private $queryBuilder_;
    private $request_;

    public function __construct(BaseModel $queryBuilder, $request)
    {
        $this->queryBuilder_ = $queryBuilder;
        $this->request_ = $request;
    }

    public function build()
    {
        $queryBuilder = $this->queryBuilder_::from('invoices')
            ->Join('clients as c', 'invoices.client_id', '=', 'c.id')
            ->leftjoin('users as u', 'c.user_id', 'u.id')
            ->select([
                DB::raw(
                    'CONCAT(u.first_name," ",u.last_name) as customer'
                ),
                'invoices.invoice_no',
                'invoices.invoice_date',
                'invoices.total_amount as amount',
                'c.total_remaining_balance as prev_balance'
            ])
            ->where('invoices.shop_id', shopId())
            ->whereNull('invoices.deleted_at')
            ->orderByDesc('invoices.created_at')
            ->when(!empty($this->request_->filter['client_id']), function ($query) {
                return $query->where('c.id', $this->request_->filter['client_id']);
            })
            ->when(!empty($this->request_->filter['from']), function ($query) {
                return $query->whereDate('invoices.created_at', '>=' ,$this->request_->filter['from']);
            })
            ->when(!empty($this->request_->filter['to']), function ($query) {
                return $query->where('invoices.created_at', '<=' , $this->request_->filter['to']);
            });
        return $queryBuilder;
    }

    public function reset()
    {
        $this->queryBuilder_ = $this->queryBuilder_->newInstance();
    }
}
