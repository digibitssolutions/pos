<?php

namespace App\Http\Controllers\Brands;

use Exception;
use Illuminate\Http\Request;
use App\Http\Services\BrandService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Brands\Requests\CreateBrandRequest;
use App\Http\Controllers\Brands\Requests\UpdateBrandRequest;

class BrandsController extends Controller
{

    private $brandService_;

    public function __construct(BrandService $brandService)
    {
        //$this->middleware('auth:api');
        $this->brandService_ = $brandService;
    }

    public function index(Request $request)
    {
        try {
            return $this->brandService_->brandsList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateBrandRequest $request)
    {
        try {
            return $this->brandService_->createBrand($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($brandId)
    {
        try {
            return $this->brandService_->singleBrand($brandId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateBrandRequest $request)
    {
        try {
            return $this->brandService_->updateBrand($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->brandService_->deleteBrand($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function toDigest()
    {
        return $this->brandService_->todigest();
    }
}
