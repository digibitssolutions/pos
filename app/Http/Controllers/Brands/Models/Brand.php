<?php

namespace App\Http\Controllers\Brands\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\Groups\Models\Group;

class Brand extends Model
{

    protected $table = 'brands';

    protected $fillable = [
        'shop_id',
        'group_id',
        'brand_name',
        'code',
        'short_description'
    ];

    public function brandsFilter($request){
       return Brand::when(!empty($request->brand_name), function ($query) use ($request) {
            return $query->where('brand_name', 'like', '%' . $request->brand_name. '%');
        })
        ->when(!empty($request->code), function ($query) use ($request) {
            return $query->where('code', $request->code);
        })
        ->when(!empty($request->from), function ($query) use ($request) {
            return  $query->whereDate('created_at', '>=', $request->from);
        })
        ->when(!empty($request->to), function ($query) use ($request) {
            return  $query->whereDate('created_at', '<=', $request->to);
        });
    }

    public function shop(){
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

}
