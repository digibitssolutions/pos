<?php
namespace App\Http\Controllers\Brands\Requests;

use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateBrandRequest extends FormRequest
{
    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;    
    }
   
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'brand_name' => ['required', Rule::unique('brands')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'code' => ['required', Rule::unique('brands')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'group_id' => ['required', 'exists:groups,id'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
    }

}

?>
