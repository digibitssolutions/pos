<?php

namespace App\Http\Controllers\Categories;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CategoryService;
use App\Http\Controllers\Categories\Requests\CreateCategoryRequest;
use App\Http\Controllers\Categories\Requests\UpdateCategoryRequest;

class CategoriesController extends Controller
{

    private $categoryService_;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService_ = $categoryService;
    }

    public function index(Request $request)
    {
        try {
            return $this->categoryService_->categoriesList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateCategoryRequest $request)
    {
        try {
            return $this->categoryService_->createCategory($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($categoryId)
    {
        try {
            return $this->categoryService_->singleCategory($categoryId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateCategoryRequest $request)
    {
        try {
            return $this->categoryService_->updateCategory($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->categoryService_->deleteCategory($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function toDigest()
    {
        return $this->categoryService_->todigest();
    }
}
