<?php

namespace App\Http\Controllers\Categories\Models;

use App\Http\Controllers\Brands\Models\Brand;
use App\Http\Controllers\Groups\Models\Group;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;

class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = [
        'shop_id',
        'category_name',
        'group_id',
        // 'brand_id',
        'code',
        'short_description'
    ];

    public function categoriesFilter($request){
        return Category::when(!empty($request->category_name), function($query) use ($request){
            return $query->where('category_name', 'like', '%'. $request->category_name. '%');
        })
        ->when(!empty($request->code), function($query) use ($request){
            return $query->where('code', $request->code);
        })
        ->when(!empty($request->group_id), function($query) use ($request){
            return $query->where('group_id', $request->group_id);
        })
        ->when(!empty($request->from), function ($query) use ($request) {
            return  $query->whereDate('created_at', '>=', $request->from);
        })
        ->when(!empty($request->to), function ($query) use ($request) {
            return  $query->whereDate('created_at', '<=', $request->to);
        });
    }

    public function shop(){
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }
}
