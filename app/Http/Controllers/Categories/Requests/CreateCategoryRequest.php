<?php
namespace App\Http\Controllers\Categories\Requests;

use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateCategoryRequest extends FormRequest
{
    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;    
    }
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'category_name' => ['required', Rule::unique('categories')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'code' => ['required', Rule::unique('categories')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'group_id' => ['required', 'exists:groups,id'],
            // 'brand_id' => ['nullable', 'exists:brands,id'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
    }
}

?>
