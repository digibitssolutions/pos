<?php

namespace App\Http\Controllers\ClientLedgers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ClientLedgers\Models\ClientLedger;
use App\Http\Controllers\ClientLedgers\Requests\CreateClientLedgerRequest;
use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\LogActivities\Models\RecentActivity;
use App\Http\Controllers\Modules\File\Modules;
use App\Http\Services\BaseService;
use Illuminate\Support\Facades\DB;

use function App\{
    addToLog,
    clientName,
    loggedUserName,
    shopId
};

class ClientLedgerController extends Controller
{
    private $thisModel_;

    public function __construct(ClientLedger $model)
    {
        $this->thisModel_ = $model;
    }

    public function index(Request $request)
    {
        $ledgers = DB::table('client_ledgers as cl')->leftJoin('clients as c', 'c.id', 'cl.client_id')
            ->rightJoin('users as u', 'u.id', 'c.user_id')
            ->where('cl.shop_id', shopId())
            ->select(['cl.*', 'u.first_name', 'u.last_name'])
            ->when(!empty($request->client_id), function ($query) use ($request) {
                return $query->where('cl.client_id', $request->client_id);
            })
            ->get();

        $clients = DB::table('clients as c')->join('users as u', 'u.id', 'c.user_id')
            ->where('c.shop_id', shopId())
            ->select(['c.*', 'u.first_name', 'u.last_name'])
            ->get();
        return view('admin.client-ledgers.client-ledgers-listing', compact('ledgers', 'clients'));
    }

    public function store(CreateClientLedgerRequest $request)
    {
        $data = $request->all();
        $ledger = $this->thisModel_->where('shop_id', shopId())->where('client_id', $request->client_id)->first();
        if (!$ledger) {
            return response(['message' => "Record doesn't exists against this Client."]);
        }
        $transaction = new ClientLedger();
        $transaction->shop_id = shopId();
        $transaction->client_id = $ledger->client_id;
        $transaction->invoice_no = null;
        $transaction->paid = $data['paid'];
        $transaction->status = 'paid';
        $transaction->payment_type = $data['payment_type'];
        $transaction->invoice_date = $data['invoice_date'];
        $transaction->description = $data['description'];
        if ($transaction->save()) {
            $log = [];
            $log['relation_id'] = $ledger->client_id;
            $log['shop_id'] = shopId();
            $log['message'] = loggedUserName() . ' Deposits ' . $data['paid'] . ' in ' . clientName($ledger->client_id) . ' account';
            $log['created_by'] =  BaseService::userId();
            $activity = RecentActivity::create($log);
            $transaction['account_name'] = $transaction->client->user->first_name . ' ' . $transaction->client->user->last_name;
            addToLog(LogTypeEnum::Info, null, $transaction, $transaction, LogAction::Created, ModuleEnum::CLIENT_LEDGER);
            addToLog(LogTypeEnum::Info, null, null, $activity, LogAction::Created, ModuleEnum::CLIENT_LEDGER);
            return response(['message' => 'Transaction Added successfully']);
        }
        return response(['error-message' => 'Something went wrong!']);
    }

    public function generateReport(Request $request)
    {
        $ledgers = $this->thisModel_->where('shop_id', shopId())->where('client_id', $request->client_id)
            ->get();
        $client = Client::where('shop_id', shopId())->where('id', $request->client_id)
            ->with('user.userDetail')
            ->first();
        $pdf = \PDF::loadView('admin.client-ledgers.client-ledger-pdf', ['ledgers' => $ledgers, 'client' => $client]);
        return $pdf->download($client->user->first_name . " " . $client->user->last_name . '.pdf');
    }

    public function clientInvoice(int $id)
    {
        $invoice = Invoice::where('shop_id', shopId())->where('invoice_no', $id)
            ->first();
        if ($invoice) {
            $invoiceProducts = InvoiceProduct::where('invoice_id', $invoice->id)->get();
            return view('admin.invoices.show-invoice', compact('invoice', 'invoiceProducts'));
        }
        return back()->with(['error-message' => 'Record not found.']);
    }
}
