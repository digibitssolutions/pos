<?php

namespace App\Http\Controllers\ClientLedgers\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ClientLedger extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id', 'client_it', 'invoice_id', 'paid', 'payable', 'status', 'payment_type', 'invoice_date', 'description'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

}
