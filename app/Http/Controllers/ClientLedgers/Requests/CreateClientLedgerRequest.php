<?php

namespace App\Http\Controllers\ClientLedgers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientLedgerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => ['required', 'exists:clients,id'],
            'paid' => ['required', 'integer'],
            'payment_type' => ['required']
        ];
    }
}
