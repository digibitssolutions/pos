<?php

namespace App\Http\Controllers\Clients;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ClientService;
use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Clients\Requests\CreateClientRequest;
use App\Http\Controllers\Clients\Requests\UpdateClientRequest;

use function App\shopId;

class ClientController extends Controller
{
    const MODULE_NAME = 'client';
    const CLIENT_UPDATE = 'Client has been updated successfully';
    const CLIENT_UPDATE_FAILED = 'Failed to update client';
    const CLIENT_DELETE = 'Client has been deleted successfully';

    private $clientService_;
    private $clientModel_;

    public function __construct(ClientService $clientService, Client $model)
    {
        $this->clientService_ = $clientService;
        $this->clientModel_ = $model;
        // $this->middleware('loogedInShop');
    }

    public function clientsList(Request $request)
    {
        try {
            return $this->clientService_->clientsList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        return view('admin.clients.add-client');
    }

    public function store(CreateClientRequest $request)
    {
        try {
            return $this->clientService_->createClientAccount($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        }
    }

    public function show($id)
    {
        try {
            return $this->clientService_->singleClientRecord($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function client($id)
    {
        try {
            return $this->clientService_->client($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit($id)
    {
        return $client = $this->clientModel_::where('shop_id', shopId())
        ->withUser()
        ->where('id', $id)->first();
    }

    public function update(UpdateClientRequest $request)
    {
        try {
            return $this->clientService_->updateClientAccount($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function clientTransaction($id)
    {
        return $this->clientService_->clientTransaction($id);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function storeClientTransaction(Request $request)
    {
        try {
            return $this->clientService_->storeClientTransaction($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function destroy($id)
    // {
    //     try {
    //         return $this->clientService_->deleteClientAccount($id);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         return $this->serverSQLError($ex);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }

    public function clientAccountStatement($id)
    {
        try {
            return $this->clientService_->clientAccountStatement($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
    
}
