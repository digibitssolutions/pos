<?php

namespace App\Http\Controllers\Clients\Models;

use App\Http\Controllers\Invoices\Models\Invoice;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\UserDetails\Models\UserDetail;
use App\Models\BaseModel;
use App\User;

class Client extends BaseModel
{

    protected $table = 'clients';

    protected $fillable = [
        'id',
        'user_id',
        'shop_id',
        'total_remaining_balance'
    ];


    public function clientsFilter($request)
    {
        return Client::when(!empty($request->first_name), function ($query) use ($request) {
            return $query->whereRelation('user', 'first_name', 'like', '%' . $request->first_name . '%');
        })
            ->when(!empty($request->last_name), function ($query) use ($request) {
                return $query->whereRelation('user', 'last_name', 'like', '%' . $request->last_name) . '%';
            })
            ->when(!empty($request->mobile), function ($query) use ($request) {
                return $query->whereHas('user', function ($query) use ($request) {
                    return  $query->whereRelation('userDetail', 'mobile', $request->mobile);
                });
            })
            ->when(!empty($request->email), function ($query) use ($request) {
                return $query->whereRelation('user', 'email', $request->email);
            })
            ->when(!empty($request->city), function ($query) use ($request) {
                return $query->whereHas('user', function ($query) use ($request) {
                    return  $query->whereRelation('userDetail', 'city', $request->city);
                });
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function toDigest()
    {
        return $this->select(['id', 'code', 'first_name', 'last_name'])->where('company_id', auth()->user()->id)->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function scopeWithUser($query)
    {
        return $query->with(['user' => function ($query) {
            return $query->with('userDetail');
        }]);
    }
}
