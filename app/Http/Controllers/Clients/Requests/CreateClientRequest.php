<?php

namespace App\Http\Controllers\Clients\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:20'],
            'last_name' => ['required', 'string', 'max:20'],
            'email' => ['required', 'unique:users', 'email'],
            // 'password' => ['min:6', 'confirmed'],
            // 'password_confirmation' => ['min:6'],
            'mobile' => ['required', 'string', 'max:40'],
        ];
    }
}
