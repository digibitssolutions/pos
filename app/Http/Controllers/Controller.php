<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Response;
use App\Http\Controllers\Enums\LogTypeEnum;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use function App\addToLog;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function serverError(Exception $exception, int $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        $exceptionData = [
            'message' => $exception->getMessage(),
            'line_number' => $exception->getLine(),
            'previous' => $exception->getPrevious(),
            'file_where_error' => $exception->getFile(),
            'other_info' => $exception,
        ];

        addToLog(LogTypeEnum::Error, $exceptionData);
        return back()->with(
            [
                'error' => $exception->getMessage(),
                'ex' => $exception,
            ],
            $code
        );
    }

    protected function serverSQLError(Exception $exception, int $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        addToLog(LogTypeEnum::Error, $exception);
        return back()->with(['error-message' => $this->getDatabaseErrorCodeInfo($exception)]);
    }

    protected function getDatabaseErrorCodeInfo(Exception $exception)
    {
        $message = $exception->errorInfo[2];
        switch ($exception->errorInfo[1]) {
            case 1451:
                $message = 'Unable to delete record, related data to this record needs to be removed first';
                break;
            case 1452:
                $message = 'Cannot add or update a child row: a foreign key constraint fails';
                break;
        }
        return $message;
    }

    public function type()
    {
        return Auth::user()->type;
    }
}
