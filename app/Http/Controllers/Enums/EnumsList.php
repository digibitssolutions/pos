<?php

namespace App\Http\Controllers\Enums;

class EnumsList
{

    const SHOP            = 'Shop';
    const ADMIN           = 'Admin';
    const Pagination      = 10;
    const MaxPagination   = 100;
    const Pdf             = 1;
    const Excel           = 2;
    const Csv             = 3;
    const Json            = 4;
}
