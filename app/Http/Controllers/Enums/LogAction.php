<?php

namespace App\Http\Controllers\Enums;

use App\Http\Controllers\Enums\EnumsList;

class LogAction extends EnumsList
{
    const Display      = 'Display';
    const Created      = 'Created';
    const Updated      = 'Updated';
    const Deleted      = 'Deleted';
}
