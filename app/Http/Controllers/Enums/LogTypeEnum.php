<?php

namespace App\Http\Controllers\Enums;

use App\Http\Controllers\Enums\EnumsList;

class LogTypeEnum extends EnumsList
{
    const Info      = 'Info';
    const Error      = 'Error';
}
