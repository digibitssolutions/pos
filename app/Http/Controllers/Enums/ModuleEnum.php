<?php

namespace App\Http\Controllers\Enums;

class ModuleEnum extends EnumsList
{

    const Groups = 1;
    const Categories = 2;
    const INVOICES = 3;
    const FieldOptionType = 4;
    const Clients = 5;
    const Party = 6;
    const SalePerson = 7;
    const Products = 8;
    const CLIENT_LEDGER = 9;
    const PURCHASE_LEDGER = 10;
    const STOCK = 11;
}
