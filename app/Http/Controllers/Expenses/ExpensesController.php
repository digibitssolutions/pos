<?php

namespace App\Http\Controllers\Expenses;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ExpenseService;

class ExpensesController extends Controller
{

    private $expenseService_;

    public function __construct(ExpenseService $expenseService)
    {
        $this->expenseService_ = $expenseService;
    }

    public function index(Request $request)
    {
        try {
            return $this->expenseService_->expensesList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->expenseService_->createExpense();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->expenseService_->storeExpense($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->expenseService_->deleteExpense($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
