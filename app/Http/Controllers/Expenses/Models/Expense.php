<?php

namespace App\Http\Controllers\Expenses\Models;

use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct;
use App\Http\Controllers\Transactions\Models\Transaction;

class Expense extends Model
{

    protected $fillable = [
        'shop_id',
        'expense_name',
        'expense_type',
        'expense_amount',
        'expense_date',
        'description',
        'note'
    ];

    // public function invoicesFilter($request)
    // {
    //     return Invoice::when(!empty($request->invoice_no), function ($query) use ($request) {
    //         return $query->where('invoice_no', 'like', '%' . $request->invoice_no . '%');
    //     })
    //         ->when(!empty($request->order_no), function ($query) use ($request) {
    //             return $query->where('order_no', $request->order_no);
    //         })
    //         ->when(!empty($request->invoice_date), function ($query) use ($request) {
    //             return $query->whereDate('invoice_date', $request->invoice_date);
    //         })
    //         ->when(!empty($request->delivery_date), function ($query) use ($request) {
    //             return $query->whereDate('delivery_date', $request->delivery_date);
    //         })
    //         ->when(!empty($request->payment_type), function ($query) use ($request) {
    //             return $query->where('payment_type', $request->payment_type);
    //         })
    //         ->when(!empty($request->from), function ($query) use ($request) {
    //             return  $query->whereDate('created_at', '>=', $request->from);
    //         })
    //         ->when(!empty($request->to), function ($query) use ($request) {
    //             return  $query->whereDate('created_at', '<=', $request->to);
    //         });
    // }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
