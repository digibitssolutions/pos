<?php

namespace App\Http\Controllers\FieldOptionType\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldOption extends Model
{
    use HasFactory;

    protected $fillable = [
        'type_id',
        'name',
        'short_name',
        'description',
        'sort_order'
    ];
}
