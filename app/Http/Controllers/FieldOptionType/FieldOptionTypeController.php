<?php

namespace App\Http\Controllers\FieldOptionType;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\FieldOptionTypeService;
use App\Http\Controllers\FieldOptionType\Requests\CreateFieldOptionTypes as RequestsCreateFieldOptionTypes;

class FieldOptionTypeController extends Controller
{
    private $fieldOptionTypeService_;

    public function __construct(FieldOptionTypeService $fieldOptionTypeService)
    {
        //$this->middleware('auth:api');
        $this->fieldOptionTypeService_ = $fieldOptionTypeService;
    }

    public function index(Request $request)
    {
        try {
            return $this->fieldOptionTypeService_->fieldOptionTypeList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(RequestsCreateFieldOptionTypes $request)
    {
        try {
            return $this->fieldOptionTypeService_->createFieldOptionType($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function show($categoryId)
    // {
    //     try {
    //         return $this->categoryService_->singleCategory($categoryId);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }

    public function update(Request $request)
    {
        try {
            return $this->fieldOptionTypeService_->updateFieldTypeOption($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->fieldOptionTypeService_->deleteFieldOptionType($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
