<?php

namespace App\Http\Controllers\FieldOptionType\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldOptionType extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id',
        'type_description',
        'comment'
    ];

    // public function fieldOptions(){
    //     return $this->hasMany(FieldOption::class);
    // }
}
