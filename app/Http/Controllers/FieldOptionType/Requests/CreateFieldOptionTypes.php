<?php

namespace App\Http\Controllers\FieldOptionType\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFieldOptionTypes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "type_description" => "required",
        ];
    }
}
