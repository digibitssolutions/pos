<?php

namespace App\Http\Controllers\Generics\Builders;

use App;

class GenericModelBuilder
{
    private $modelType_ ;

    public function __construct(string $modelType)
    {
        $this->modelType_ = $modelType;
    }

    public function getModel()
    {
        $model = null;
        switch ($this->modelType_) {
            case 'measurement_units':
                $model = new App\Http\Controllers\Generics\Models\MeasurementUnit();
            break;
            case 'usage_units':
                $model = new App\Http\Controllers\Generics\Models\UsageUnit();
            break;
            case 'vat_codes':
                $model = new App\Http\Controllers\Generics\Models\VatCode();
            break;
        }

        return $model;
    }
}
