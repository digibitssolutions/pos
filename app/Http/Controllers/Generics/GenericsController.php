<?php

namespace App\Http\Controllers\Generics;

use Exception;
use Illuminate\Http\Request;
use App\Http\Services\BaseService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Enums\EnumsList;
use Symfony\Component\HttpFoundation\Response;
use App\Exceptions\GenericNotSupportedException;
use App\Http\Controllers\Generics\Builders\GenericModelBuilder;
use App\Http\Controllers\Generics\Requests\CreateGenericRequest as CreateRequest;
use App\Http\Controllers\Generics\Requests\UpdateGenericRequest as UpdateRequest;

class GenericsController extends Controller
{
    // const ITEM = 'generic';
    const COLLECTION_NAME = 'generics';
    const RECORD_CREATED = 'Record created successfully';
    const RECORD_NOT_FOUND = 'Record not found';
    const RECORD_UPDATED = 'Record updated successfully';
    const RECORD_DELETED = 'Record deleted successfully';
    const GENERIC_TYPE_NOT_PROVIDED = 'Generic type not found';
    const GENERIC_TYPE_ALREADY_EXISTS = 'Generic type already exists';

    private $baseService_;
    private $model;

    public function __construct(Request $request, BaseService $base)
    {
        if (empty($request->route()->parameter('type'))) {
            throw new Exception(GenericsController::GENERIC_TYPE_NOT_PROVIDED);
        }

        $type = $request->route()->parameter('type');
        $genericModelBuilder = new GenericModelBuilder($type);
        $model = $genericModelBuilder->getModel();
        if ($model == null) {
            throw new GenericNotSupportedException('Generic not supported');
        }
        $this->baseService_ = $base;
        $this->model_ = $model;
        // parent::__construct($model);
    }

    public function index(Request $request)
    {
        try {
            // these are generic filters
            $data = [];
            $data = $this->model_->when(!empty($request->name), function ($query) use ($request) {
                return $query->where('name', 'like', '%' . $request->name . '%');
            })
            ->when(!empty($request->param_1), function ($query) use ($request) {
                return $query->where('param_1', 'like', '%' . $request->param_1 . '%');
            })
            ->when(!empty($request->param_2), function ($query) use ($request) {
                return $query->where('param_2', 'like', '%' . $request->param_2 . '%');
            });
            if($this->baseService_->type() == EnumsList::SHOP){
                $data = $data->where('shop_id', $this->baseService_->shopId())->paginate(EnumsList::Pagination);
            }else{
                $data = $data->paginate(EnumsList::Pagination);
            }
            if ($data->isNotEmpty()) {
                return $this->baseService_->created([GenericsController::COLLECTION_NAME => $data]);
            }
            return $this->baseService_->noRecord(['message' => GenericsController::RECORD_NOT_FOUND],Response::HTTP_OK);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateRequest $request)
    {
        try {
            $data = $request->only(['name', 'param_1', 'param_2']);
            $data['shop_id'] = $this->baseService_->shopId();
            $result = $this->model_->create($data);
            if ($result) {
                return $this->baseService_->created(['message' => GenericsController::RECORD_CREATED],Response::HTTP_OK);
            }
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function show($type, $id)
    // {
    //     try {
    //         if ($this->baseService_->type() == EnumsList::SHOP) {
    //             $record = $this->model_::where('shop_id', $this->baseService_->shopId())->where('id', $id)->first();
    //         }else{
    //             $record = $this->model_->find($id);
    //         }
    //         if ($record) {
    //             return $this->baseService_->created([GenericsController::COLLECTION_NAME => $record]);
    //         }
    //         return $this->baseService_->noRecord(['message' => GenericsController::RECORD_NOT_FOUND],200);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         return $this->serverSQLError($ex);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }

    public function update(UpdateRequest $request, $type, $id)
    {
        try {
            $data = $request->all();
            if ($this->baseService_->type() == EnumsList::SHOP) {
                $record = $this->model_::where('shop_id', $this->baseService_->shopId())->where('id', $id)->first();
            }else{
                $record = $this->model_->find($id);
            }
            if($record){
                $record->update($data);
                return $this->baseService_->created(['message' => GenericsController::RECORD_UPDATED],Response::HTTP_OK);
            }
            return $this->baseService_->noRecord(['message' => GenericsController::RECORD_NOT_FOUND],Response::HTTP_OK);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($type, $id)
    {
        try {
            if ($this->baseService_->type() == EnumsList::SHOP) {
                $record = $this->model_::where('shop_id', $this->baseService_->shopId())->where('id', $id)->first();
            }else{
                $record = $this->model_->find($id);
            }
            if($record){
                $record->delete();
                return $this->baseService_->created(['message' => GenericsController::RECORD_DELETED],Response::HTTP_OK);
            }
            return $this->baseService_->noRecord(['message' => GenericsController::RECORD_NOT_FOUND],Response::HTTP_OK);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}

