<?php

namespace App\Http\Controllers\Generics\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;


class MeasurementUnit extends Model implements IGeneric
{
	protected $table = 'generic_measurement_units';

	protected $fillable = [
		'shop_id',
		'name',
		'param_1',
		'param_2',
	];

	public function shop()
	{
		return $this->belongsTo(Shop::class, 'shop_id');
	}
}
