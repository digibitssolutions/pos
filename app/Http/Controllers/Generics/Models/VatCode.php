<?php

namespace App\Http\Controllers\Generics\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;

class VatCode extends Model implements IGeneric
{
    protected $table = 'generic_vat_codes';

    protected $fillable = [
        'shop_id',
		'name',
		'param_1',
        'param_2'
    ];

    public function shop(){
		return $this->belongsTo(Shop::class, 'shop_id');
	}

}
