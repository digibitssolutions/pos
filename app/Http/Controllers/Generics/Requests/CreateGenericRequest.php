<?php

namespace App\Http\Controllers\Generics\Requests;

use App\Traits\GetModelName;
use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;

class CreateGenericRequest extends FormRequest
{

    use GetModelName;

    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $table = $this->route()->parameter('type');

        return [

            'name' => ['required','string','max:150',Rule::unique('generic_'.$table)->where(function($query) {
                $query->where('shop_id', '=', auth()->user()->shop_id);
             })],
          
            'param_1' => ['nullable','string'],
            'param_2' => ['nullable','string'],
        ];
    }

    public function messages(){
        $type = $this->route()->parameter('type');

        return [
            'name.required' => 'The '.$this->getModelName($type) .' field is required',
            'name.unique' => 'The '.$this->getModelName($type) .' already exists in the system',
        ];
    }

   
}
