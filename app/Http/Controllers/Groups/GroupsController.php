<?php

namespace App\Http\Controllers\Groups;

use Exception;
use Illuminate\Http\Request;
use App\Http\Services\GroupService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Groups\Requests\GroupRequest;
use App\Http\Controllers\Groups\Requests\UpdateGroupRequest;

class GroupsController extends Controller
{

    private $groupService_;

    public function __construct(GroupService $groupService)
    {
        //$this->middleware('auth:api');
        $this->groupService_ = $groupService;
    }

    public function index(Request $request)
    {
        try {
            return $this->groupService_->groupsList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(GroupRequest $request)
    {
        try {
            return $this->groupService_->createGroup($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($groupId)
    {
        try {
            return $this->groupService_->singleGroup($groupId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateGroupRequest $request)
    {
        try {
            return $this->groupService_->updateGroup($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->groupService_->deleteGroup($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function toDigest()
    {
        return $this->groupService_->toDigest();
    }
}
