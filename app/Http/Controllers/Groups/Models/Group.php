<?php

namespace App\Http\Controllers\Groups\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;

class Group extends Model
{

    protected $table = 'groups';

    protected $fillable = [
        'shop_id',
        'group_name',
        'code',
        'short_description'
    ]; 

    public function groupsFilter($request)
    {
       return Group::when(!empty($request->group_name), function ($query) use ($request) {
            return $query->where('group_name', 'like', '%' . $request->group_name. '%');
        })
        ->when(!empty($request->code), function ($query) use ($request) {
            return $query->where('code', $request->code);
        })
        ->when(!empty($request->from), function ($query) use ($request) {
            return  $query->whereDate('created_at', '>=', $request->from);
        })
        ->when(!empty($request->to), function ($query) use ($request) {
            return  $query->whereDate('created_at', '<=', $request->to);
        }); 
    }

    public function shop(){
        return $this->belongsTo(Shop::class);
    }

}
