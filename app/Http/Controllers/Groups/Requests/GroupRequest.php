<?php
namespace App\Http\Controllers\Groups\Requests;

use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GroupRequest extends FormRequest
{
    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;    
    }
   
    public function authorize()
    {
        return true;
    }
   
    public function rules()
    {
        return [
            'group_name' => ['required', Rule::unique('groups')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'code' => ['required', Rule::unique('groups')->where(function($query){
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
    }
}
