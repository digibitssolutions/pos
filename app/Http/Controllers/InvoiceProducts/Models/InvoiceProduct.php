<?php

namespace App\Http\Controllers\InvoiceProducts\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Products\Models\Product;

class InvoiceProduct extends Model
{

    use SoftDeletes;

    protected $table = 'invoice_products';

    protected $fillable = [
        'invoice_id',
        'product_id',
        'item_details',
        'quantity',
        'price',
        'discount',
        'tax',
        'amount',
    ];

    // public function invoicesFilter($request)
    // {
    //    return Invoice::when(!empty($request->invoice_no), function ($query) use ($request) {
    //         return $query->where('invoice_no', 'like', '%' . $request->invoice_no. '%');
    //    });
    // }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
