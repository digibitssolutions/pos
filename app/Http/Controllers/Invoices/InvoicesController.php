<?php

namespace App\Http\Controllers\Invoices;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\InvoiceService;
use App\Http\Controllers\Invoices\Requests\InvoiceMassDeleteRequest;
use App\Http\Controllers\Invoices\Requests\CreateInvoiceRequest;
use App\Http\Controllers\Invoices\Requests\UpdateInvoiceRequest;

class InvoicesController extends Controller
{

    private $invoiceService_;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService_ = $invoiceService;
    }

    public function index(Request $request)
    {
        try {
            return $this->invoiceService_->invoicesList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            return $this->invoiceService_->changeStatus($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->invoiceService_->createInvoice();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateInvoiceRequest $request)
    {
        try {
            return $this->invoiceService_->storeInvoice($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($invoiceId)
    {
        try {
            return $this->invoiceService_->singleInvoice($invoiceId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit($id)
    {
        return $this->invoiceService_->editInvoice($id);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateInvoiceRequest $request, $id)
    {
        return $this->invoiceService_->updateInvoice($request, $id);
        try {
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->invoiceService_->deleteInvoice($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function invoiceReturn()
    {
        try {
            return $this->invoiceService_->invoiceReturn();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function storeInvoiceReturn(Request $request)
    {
        try {
            return $this->invoiceService_->storeInvoiceReturn($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function singleProduct($product_id)
    {
        try {
            return $this->invoiceService_->singleProduct($product_id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function salesList(Request $request)
    {
        return $this->invoiceService_->salesList($request);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function saveInvoicePDF($id)
    {
        return $this->invoiceService_->saveInvoicePDF($id);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function deleteInoiveProduct($id)
    {
        try {
            return $this->invoiceService_->deleteProduct($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function printInvoice($id)
    {
        return $this->invoiceService_->printInvoice($id);
    }

    public function getLastProductPrice(int $cId, int $pId){
        return $this->invoiceService_->getLastProductPrice($cId, $pId);
    }
}
