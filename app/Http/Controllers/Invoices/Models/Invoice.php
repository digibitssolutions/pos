<?php

namespace App\Http\Controllers\Invoices\Models;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Shops\Models\Shop;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\SalesPerson\Models\SalePerson;
use App\Http\Controllers\Transactions\Models\Transaction;
use App\Models\BaseModel;

class Invoice extends BaseModel
{

    use SoftDeletes;

    protected $table = 'invoices';

    protected $fillable = [
        'shop_id',
        'client_id',
        'sale_person_id',
        'invoice_no',
        'order_no',
        'invoice_status',
        'invoice_date',
        'delivery_date',
        'payment_type',
        'subject',
        'customer_notes',
        'terms_conditions',
        'total_amount',
        'shipping_charges',
        'remaining_amount',
        'received_amount',
    ];

    public function invoicesFilter($request)
    {
        return Invoice::when(!empty($request->invoice_no), function ($query) use ($request) {
            return $query->where('invoice_no', $request->invoice_no);
        })
        ->when(!empty($request->client_id), function ($query) use ($request) {
                return $query->where('client_id', $request->client_id);
            })
            ->when(!empty($request->order_no), function ($query) use ($request) {
                return $query->where('order_no', $request->order_no);
            })
            ->when(!empty($request->invoice_date), function ($query) use ($request) {
                return $query->whereDate('invoice_date', $request->invoice_date);
            })
            ->when(!empty($request->delivery_date), function ($query) use ($request) {
                return $query->whereDate('delivery_date', $request->delivery_date);
            })
            ->when(!empty($request->payment_type), function ($query) use ($request) {
                return $query->where('payment_type', $request->payment_type);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function scopeWithSalePerson($query)
    {
        return $query->with(['salePerson.user' => function ($query) {
            $query->select(['id', 'first_name', 'last_name']);
        }]);
    }

    public function scopeWithTransactions($query)
    {
        return $query->with('transactions');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function scopeWithClient($query)
    {
        return $query->with(['client.user' => function ($query) {
            return $query->with('userDetail');
        }]);
    }

    public function invoiceProducts()
    {
        return $this->hasMany(InvoiceProduct::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function salePerson()
    {
        return $this->belongsTo(SalePerson::class, 'sale_person_id');
    }

    public function saleReports()
    {
        return $this->hasOne(SaleReport::class, 'invoice_id');
    }
}
