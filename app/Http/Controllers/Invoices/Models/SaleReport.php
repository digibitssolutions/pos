<?php

namespace App\Http\Controllers\Invoices\Models;

use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleReport extends Model
{
    use HasFactory;

    protected $table = 'sale_reports';

    protected $fillable = [
        'shop_id',
        'client_id',
        'invoice_id',
        'profit',
        'loss'
    ];

    public function saleReportFilters($request)
    {
        return SaleReport::when(!empty($request->invoice_no), function ($query) use ($request) {
            return $query->whereRelation('invoice', 'id', $request->invoice_no);
        })
            ->when(!empty($request->invoice_date), function ($query) use ($request) {
                return $query->whereRelation('invoice', 'invoice_date', $request->invoice_date);
            })
            ->when(!empty($request->delivery_date), function ($query) use ($request) {
                return $query->whereRelation('invoice', 'delivery_date', $request->delivery_date);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            })
            ->when(!empty($request->client_id), function ($query) use ($request) {
                return  $query->whereRelation('client', 'id', $request->client_id);
            });
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
