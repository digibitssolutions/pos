<?php

namespace App\Http\Controllers\Invoices\Requests;

use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateInvoiceRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sales_person' => ['nullable'],
            'subject' => ['nullable'],
            'invoice_date' => ['nullable', 'date'],
            'delivery_date' => ['nullable', 'date'],
            'product_id' => ['required'],
            'item_details' => ['required'],
            'quantity' => ['required'],
            'price' => ['required'],
            'amount' => ['required'],
            'total_amount' => ['required'],
        ];
    }
}
