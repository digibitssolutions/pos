<?php

namespace App\Http\Controllers\LogActivities;

use App\Http\Controllers\Clients\Models\Client;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\LogActivities\Models\LogActivity;
use App\Http\Controllers\LogActivities\Models\RecentActivity;
use App\Http\Controllers\Modules\Models\Module;
use App\Http\Controllers\Parties\Models\Party;
use App\Http\Services\BaseService;
use App\User;

use function App\addToLog;
use function App\clientName;
use function App\loggedUserName;
use function App\shopId;

class LogsController extends Controller
{
    const CREATED = 'created ';
    const DISPLAY = 'viewed ';
    const UPDATED = 'updated ';
    const DELETED = 'deleted ';

    private $recentActivitymodel_;

    public function __construct(RecentActivity $recentActivity)
    {
        $this->recentActivitymodel_ = $recentActivity;
    }

    public function index(Request $request)
    {
        try {
            $clients = Client::where('shop_id', shopId())->latest()->get();
            $parties = Party::where('shop_id', shopId())->latest()->get();
            $activities = $this->recentActivitymodel_->filters($request)->where('shop_id', shopId())->latest()->paginate(20);
            return view('admin.LogActivities.recent-activities-listing', compact('activities', 'clients', 'parties'));
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function showLogDetail($logId)
    {
        $isFound = LogActivity::find($logId);
        if ($isFound) {
            return view('admin.LogActivities.log-detail', compact('isFound'));
        }
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(int $relationId = null, array $data, string $logAction, $moduleId)
    {
        $log = [];
        $log['relation_id'] = $relationId;
        $log['shop_id'] = shopId();
        $log['message'] = loggedUserName() . ' Deposits ' . $data['paid'] . ' in ' . clientName($relationId) . ' account';
        $log['created_by'] =  BaseService::userId();
        $activity = RecentActivity::create($log);
        addToLog(LogTypeEnum::Info, null, null, $activity, $logAction, $moduleId);
    }


    public function logResponse($record)
    {
        $loggedUser = auth()->user()->first_name . ' ' . auth()->user()->last_name;
        if ($record->old_data != null) {
            $oldUser = json_decode($record->old_data);
            $oldUserAccount = isset($oldUser->account_name) ? $oldUser->account_name : 'User';
            $oldUserId = isset($oldUser->id) ? $oldUser->id : 'Id';
            $moduleId = isset($record->module_id) ? $record->module_id : 0;
            $moduleName = $this->getModuleName($moduleId);
            switch ($record->action) {
                case LogAction::Display:
                    $client = $loggedUser . ' ' . $this::DISPLAY . $moduleName . ' Record Ref (#id:' . $oldUserId . ') @' . $oldUserAccount;
                    break;
                case LogAction::Created:
                    $client = $loggedUser . ' ' . $this::CREATED . $moduleName . ' Record Ref (#id:' . $oldUserId . ') @' . $oldUserAccount;
                    break;
                case LogAction::Updated:
                    $client = $loggedUser . ' ' . $this::UPDATED . $moduleName . ' Record Ref (#id:' . $oldUserId . ') @' . $oldUserAccount;
                    break;
                case LogAction::Deleted:
                    $client = $loggedUser . ' ' . $this::DELETED . $moduleName . ' Record Ref (#id:' . $oldUserId . ') @' . $oldUserAccount;
                    break;
                default:
                    $client = $record->message;
            }
            return $client;
        }
    }

    private function getModuleName(int $moduleId_)
    {
        $module = Module::where('id', $moduleId_)->first();
        if ($module) return $module->name;
        return $moduleId_;
    }

    public function logActivities()
    {
        $logs = [];
        $records = LogActivity::latest()->paginate(EnumsList::MaxPagination);
        foreach ($records as $record) {
            $logs[] = [
                'id' => $record->id,
                'type' => $record->type,
                'created_at' => $record->created_at,
                'message' => $this->logResponse($record)
            ];
        }

        return view('admin.LogActivities.logs-listing', compact('logs'));
    }
}
