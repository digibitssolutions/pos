<?php

namespace App\Http\Controllers\LogActivities\Models;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{

    protected $table = 'log_activities';

    protected $fillable = [
        'type',
        'message',
        'url',
        'method',
        'ip',
        'old_data',
        'new_data',
        'module_id',
        'relation_id',
        'action',
        'created_by_type',
        'created_by',
    ];
}
