<?php

namespace App\Http\Controllers\LogActivities\Models;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Parties\Models\Party;
use App\User;
use Illuminate\Database\Eloquent\Model;

class RecentActivity extends Model
{

    protected $table = 'recent_activities';

    protected $fillable = [
        'message',
        'created_by',
        'shop_id',
        'relation_id',
    ];

    public function filters($request)
    {
        return RecentActivity::when(isset($request->created_by), function ($query) use ($request) {
            return $query->where('created_by', $request->created_by);
        })
            ->when(isset($request->client_id), function ($query) use ($request) {
                return $query->whereRelation('client', 'relation_id', $request->client_id);
            })
            ->when(isset($request->party_id), function ($query) use ($request) {
                return $query->whereRelation('party', 'relation_id', $request->party_id);
            })
            ->when(isset($request->from), function ($query) use ($request) {
                return $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(isset($request->to), function ($query) use ($request) {
                return $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'relation_id');
    }

    public function party()
    {
        return $this->belongsTo(Party::class, 'relation_id');
    }
}
