<?php

namespace App\Http\Controllers\LoginHistory;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\LoginLogoutHistoryService;

class LoginHistoryController extends Controller
{
    private $loginHistoryService_;

    public function __construct(LoginLogoutHistoryService $loginHistoryService)
    {
        //$this->middleware('auth:api');
        $this->loginHistoryService_ = $loginHistoryService;
    }

    public function index(Request $request)
    {
        try {
            return $this->loginHistoryService_->historyList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy()
    {
        try {
            return $this->loginHistoryService_->deleteAllHistory();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function deleteSelectedHistory(Request $request)
    {
        try {
            return $this->loginHistoryService_->deleteSelected($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
