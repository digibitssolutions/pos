<?php

namespace App\Http\Controllers\LoginHistory\Models;

use App\Http\Controllers\Users\Models\User;
use Illuminate\Database\Eloquent\Model;


class LoginHistory extends Model
{
	protected $table = 'login_history';

	protected $fillable = [

		'user_id',
		'ip',
        'device',
        'status',
        'login_time',
        'logout_time',
        'shop_id'
    ];

    public function loginHistoryFilter($request)
    {
       return LoginHistory::when(!empty($request->group_name), function ($query) use ($request) {
            return $query->where('group_name', 'like', '%' . $request->group_name. '%');
        })
        ->when(!empty($request->code), function ($query) use ($request) {
            return $query->where('code', $request->code);
        })
        ->when(!empty($request->from), function ($query) use ($request) {
            return  $query->whereDate('created_at', '>=', $request->from);
        })
        ->when(!empty($request->to), function ($query) use ($request) {
            return  $query->whereDate('created_at', '<=', $request->to);
        });
    }

    public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function scopeWithUser($query) {
        return $query->with(['user' => function($query){ $query->select(['id', 'name', 'type', 'email']);}]);
    }

}
