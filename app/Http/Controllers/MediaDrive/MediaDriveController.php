<?php

namespace App\Http\Controllers\MediaDrive;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\MediaDriveService;
use App\Http\Controllers\MediaDrive\Models\MediaDrive;

class MediaDriveController extends Controller
{

    private $mediaDriveService_;
    private $model_;

    public function __construct(MediaDrive $model, MediaDriveService $mediaDriveService)
    {
        $this->model_ = $model;
        $this->mediaDriveService_ = $mediaDriveService;
    }

    public function index(Request $request)
    {
        try {
            return $this->userService_->usersList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->userService_->addUser();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(Request $request)
    {
        return $this->mediaDriveService_->storeFile($request);
        try {
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show(Request $request)
    {
        return $this->mediaDriveService_->fetchMediaByEntity($request->module_id, $request->relation_id, $request->media_id);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function fetchMediaByEntities(Request $request)
    {
        try {
            return $this->mediaDriveService_->fetchMediaByEntities($request->module_id, $request->relation_id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($moduleId, $realtionId, $mediaId)
    {
        try {
            return $this->mediaDriveService_->deleteMediaEntity($moduleId, $realtionId, $mediaId);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
