<?php

namespace App\Http\Controllers\MediaDrive\Models;

use App\Http\Controllers\Users\Models\User;
use Illuminate\Database\Eloquent\Model;


class MediaDrive extends Model
{
	protected $table = 'media_drive';

	protected $fillable = [
		'shop_id',
		'module_id',
		'relation_id',
		'bill_no',
        'file_name',
        'media_title',
        'extension',
        'media_type'
    ];

    // public function loginHistoryFilter($request)
    // {
    //    return LoginHistory::when(!empty($request->group_name), function ($query) use ($request) {
    //         return $query->where('group_name', 'like', '%' . $request->group_name. '%');
    //     })
    //     ->when(!empty($request->code), function ($query) use ($request) {
    //         return $query->where('code', $request->code);
    //     })
    //     ->when(!empty($request->from), function ($query) use ($request) {
    //         return  $query->whereDate('created_at', '>=', $request->from);
    //     })
    //     ->when(!empty($request->to), function ($query) use ($request) {
    //         return  $query->whereDate('created_at', '<=', $request->to);
    //     });
    // }

    public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function scopeWithUser($query) {
        return $query->with(['user' => function($query){ $query->select(['id', 'name', 'type', 'email']);}]);
    }

}
