<?php

namespace App\Http\Controllers\Modules\Models;


use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name', 'namespace', 'status'
    ];
}
