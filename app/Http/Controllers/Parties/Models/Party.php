<?php

namespace App\Http\Controllers\Parties\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Party extends Model
{

    protected $table = 'parties';

    protected $fillable = [
        'user_id',
        'shop_id',
        'party_name',
    ];


    public function partiesFilter($request)
    {
        return Party::when(!empty($request->party_name), function ($query) use ($request) {
            return $query->where('party_name', 'like', '%' . $request->party_name . '%');
        })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
