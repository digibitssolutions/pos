<?php

namespace App\Http\Controllers\Parties;

use Exception;
use Illuminate\Http\Request;
use App\Http\Services\PartyService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Parties\Requests\CreatePartyRequest;
use App\Http\Controllers\Parties\Requests\UpdatePartyRequest;

class PartyController extends Controller
{

    private $partyService_;

    public function __construct(PartyService $partyService)
    {
        $this->partyService_ = $partyService;
    }

    public function index(Request $request)
    {
        return $this->partyService_->partiesList($request);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->partyService_->createPartyAccount();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreatePartyRequest $request)
    {
        try {
            return $this->partyService_->storePartyAccount($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        }
    }

    public function show($id)
    {
        try {
            return $this->partyService_->showPartyAccount($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit($id)
    {
        try {
            return $this->partyService_->editPartyAccount($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdatePartyRequest $request)
    {
        return $this->partyService_->updatePartyAccount($request);
        try {
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function destroy($id)
    // {
    //     try {
    //         return $this->partyService_->deleteClientAccount($id);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         return $this->serverSQLError($ex);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }
}
