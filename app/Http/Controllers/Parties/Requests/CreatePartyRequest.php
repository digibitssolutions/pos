<?php

namespace App\Http\Controllers\Parties\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePartyRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => [ 'required', 'unique:users,email', 'email'],
            'first_name' => [ 'required', 'string'],
            'last_name' => [ 'required', 'string'],
        ];
    }
}
