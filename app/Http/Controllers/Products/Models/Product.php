<?php

namespace App\Http\Controllers\Products\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\Groups\Models\Group;
use App\Http\Controllers\Brands\Models\Brand;
use App\Http\Controllers\Generics\Models\VatCode;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Categories\Models\Category;
use App\Http\Controllers\Generics\Models\MeasurementUnit;
use App\Http\Controllers\StockIntake\Models\MasterStock;
use App\Http\Controllers\StockIntake\Models\StockIntake;
use App\Models\BaseModel;

class Product extends BaseModel
{

    protected $table = 'products';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'shop_id',
        'group_id',
        'brand_id',
        'category_id',
        'product_name',
        'short_description',
        'measurement_unit',
        'image',
        'code',
        'unit_price',
        'purchase_price',
        'sale_price',
        'discount',
        'special_price',
        'expiry_date',
        'varient',
        'tax',
        'tax_type'
    ];

    public function productsFilter($request)
    {
        return Product::when(!empty($request->product_name), function ($query) use ($request) {
            return $query->where('product_name', 'like', '%' . $request->product_name . '%');
        })
            ->when(!empty($request->code), function ($query) use ($request) {
                return $query->where('code', $request->code);
            })
            ->when(!empty($request->unit_price), function ($query) use ($request) {
                return $query->where('unit_price', $request->unit_price);
            })
            ->when(!empty($request->category_id), function ($query) use ($request) {
                return $query->where('category_id', $request->category_id);
            })
            ->when(!empty($request->measurement_unit), function ($query) use ($request) {
                return $query->where('measurement_unit', $request->measurement_unit);
            })
            ->when(!empty($request->from_expiry_date), function ($query) use ($request) {
                return $query->whereDate('expiry_date', '>=', $request->from_expiry_date);
            })
            ->when(!empty($request->to_expiry_date), function ($query) use ($request) {
                return $query->whereDate('expiry_date', '<=', $request->to_expiry_date);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function vatCode()
    {
        return $this->belongsTo(VatCode::class, 'vat_code_id');
    }

    public function measurementUnit()
    {
        return $this->belongsTo(MeasurementUnit::class, 'measurement_unit');
    }

    public function stockIntake()
    {
        return $this->hasMany(StockIntake::class, 'product_id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function masterStock()
    {
        return $this->hasOne(MasterStock::class);
    }
}
