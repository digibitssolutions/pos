<?php

namespace App\Http\Controllers\Products;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ProductService;
use App\Http\Controllers\Products\Requests\CreateProductRequest;
use App\Http\Controllers\Products\Requests\UpdateProductRequest;

class ProductsController extends Controller
{

    private $productService_;

    public function __construct(ProductService $productService)
    {
        $this->productService_ = $productService;
    }

    public function index(Request $request)
    {
        try {
            return $this->productService_->productsList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function fetchPagination(Request $request)
    {
        try {
            return $this->productService_->fetchPagination($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->productService_->createProduct();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateProductRequest $request)
    {
        try {
            return $this->productService_->storeProduct($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($productId)
    {
        try {
            return $this->productService_->singleProduct($productId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit($productId)
    {
        try {
            return $this->productService_->editProduct($productId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateProductRequest $request)
    {
        try {
            return $this->productService_->updateProduct($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->productService_->deleteProduct($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        }catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function toDigest()
    {
        return $this->productService_->todigest();
    }

    public function getProductNameId($code)
    {
        try {
            return $this->productService_->getCurrentProductName($code);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        }
    }

    public function updateProductById($id, $price, $salePrice)
    {
        try {
            return $this->productService_->updateProductById($id, $price, $salePrice);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        }
    }
}
