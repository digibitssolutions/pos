<?php

namespace App\Http\Controllers\Products\Requests;

use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateProductRequest extends FormRequest
{

    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'group_id' => ['nullable', 'exists:groups,id'],
            'category_id' => ['nullable', 'exists:categories,id'],
            'measurement_unit' => ['nullable', 'exists:generic_measurement_units,id'],
            'product_name' => ['required'],
            'code' => ['required', Rule::unique('products')->where(function ($query) {
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'unit_price' => ['required'],
            'discount' => ['nullable', 'integer'],
            'tax' => ['nullable', 'integer'],
            'tax_type' => ['nullable'],
            'code' => ['nullable']
        ];
    }
}
