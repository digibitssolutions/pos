<?php

namespace App\Http\Controllers\Products\Requests;

use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{

    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // 'group_id' => ['exists:groups,id'],
            // 'category_id' => ['exists:categories,id'],
            'tax_type' => ['nullable'],
            // 'measurement_unit' => ['nullable', 'exists:generic_measurement_units,id'],
            'unit_price' => ['required'],
            'purchase_price' => ['required'],
            'sale_price' => ['required'],
            // 'code' => ['nullable', Rule::unique('products')->ignore($this->id)->where(function ($query) {
            //     return $query->where('shop_id', $this->baseService_->shopId())
            //         ->where('id', '!=', $this->id);
            // })],
        ];
    }
}
