<?php

namespace App\Http\Controllers\PurchaseLedgers\Models;

use App\Http\Controllers\Parties\Models\Party;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseLedger extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id', 'party_id', 'bill_no', 'paid', 'payable', 'status', 'payment_type', 'bill_date', 'description'
    ];

    public function party()
    {
        return $this->belongsTo(Party::class);
    }
}
