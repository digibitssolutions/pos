<?php

namespace App\Http\Controllers\PurchaseLedgers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\LogActivities\Models\RecentActivity;
use App\Http\Controllers\Parties\Models\Party;
use App\Http\Controllers\PurchaseLedgers\Models\PurchaseLedger;
use App\Http\Controllers\PurchaseLedgers\Requests\CreatePurchaseLedgerRequest;
use App\Http\Services\BaseService;
use Illuminate\Support\Facades\DB;

use function App\{
    addToLog, loggedUserName, partyName, shopId
};

class PurchaseLedgerController extends Controller
{
    private $thisModel_;

    public function __construct(PurchaseLedger $model)
    {
        $this->thisModel_ = $model;
    }

    public function index(Request $request)
    {
        $ledgers = DB::table('purchase_ledgers as pl')->leftJoin('parties as p', 'p.id', 'pl.party_id')
            ->where('pl.shop_id', shopId())
            ->when(!empty($request->party_id), function ($query) use ($request) {
                return $query->where('pl.party_id', $request->party_id);
            })
            ->get();
        $parties = DB::table('parties')->where('shop_id', shopId())->get();
        return view('admin.purchase-ledgers.purchase-ledger-listing', compact('ledgers', 'parties'));
    }

    public function store(CreatePurchaseLedgerRequest $request)
    {
        $data = $request->all();
        $ledger = $this->thisModel_->where('shop_id', shopId())->where('party_id', $request->party_id)->first();
        if (!$ledger) {
            return response(['message' => "Record doesn't exists against this Party."]);
        }
        $purchase = new PurchaseLedger();
        $purchase->shop_id = shopId();
        $purchase->party_id = $ledger->party_id;
        $purchase->bill_no = $ledger->bill_no;
        $purchase->paid = $data['paid'];
        $purchase->status = 'paid';
        $purchase->payment_type = $data['payment_type'];
        $purchase->bill_date = $data['bill_date'];
        $purchase->description = $data['description'];
        if ($purchase->save()) {
            $log = [];
            $log['relation_id'] = $ledger->party_id;
            $log['shop_id'] = shopId();
            $log['message'] = loggedUserName() . ' Deposits ' . $data['paid'] . ' in ' . partyName($ledger->party_id) . ' account';
            $log['created_by'] =  BaseService::userId();
            $activity = RecentActivity::create($log);
            addToLog(LogTypeEnum::Info, null, $purchase, null, LogAction::Created, ModuleEnum::PURCHASE_LEDGER);
            addToLog(LogTypeEnum::Info, null, null, $activity, LogAction::Created, ModuleEnum::PURCHASE_LEDGER);
            return response(['message' => 'Transaction Added successfully']);
        }
        return response(['error-message' => 'Something went wrong!']);
    }

    public function generateReport(Request $request)
    {
        $ledgers = PurchaseLedger::where('shop_id', shopId())->where('party_id', $request->party_id)
            ->with('party:id,party_name')
            ->get();
        $party = Party::where('shop_id', shopId())->where('id', $request->party_id)
            ->with('user.userDetail')
            ->first();
        $pdf = \PDF::loadView('admin.purchase-ledgers.ledger-pdf', ['ledgers' => $ledgers, 'party' => $party]);
        return $pdf->download($party->party_name . '.pdf');
    }
}
