<?php

namespace App\Http\Controllers\PurchaseLedgers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePurchaseLedgerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'party_id' => ['required', 'exists:parties,id'],
            'paid' => ['required'],
            'payment_type' => ['required']
        ];
    }
}
