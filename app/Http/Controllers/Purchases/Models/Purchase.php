<?php

namespace App\Http\Controllers\Purchases\Models;

use App\Http\Controllers\Parties\Models\Party;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    protected $table = 'purchases';

    protected $fillable = [
        'shop_id',
        'party_id',
        'invoice_no',
        'images',
        'invoice_date',
        'total_amount',
        'discount',
        'net_amount',
        'note'
    ];


    public function purchasesFilter($request)
    {
        return Purchase::when(!empty($request->party_id), function ($query) use ($request) {
            return $query->where('party_id',  $request->party_id);
        })
            ->when(!empty($request->invoice_no), function ($query) use ($request) {
                return $query->where('invoice_no', $request->invoice_no);
            })
            ->when(!empty($request->invoice_date), function ($query) use ($request) {
                return $query->where('invoice_date', $request->invoice_date);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function party()
    {
        return $this->belongsTo(Party::class);
    }
}
