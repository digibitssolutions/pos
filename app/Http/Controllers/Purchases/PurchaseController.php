<?php

namespace App\Http\Controllers\Purchases;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\PurchaseService;
use App\Http\Controllers\Purchases\Requests\CreatePurchaseRequest;
use App\Http\Controllers\Purchases\Requests\UpdatePurchaseRequest;

class PurchaseController extends Controller
{

    private $purchaseService_;

    public function __construct(PurchaseService $partyService)
    {
        $this->purchaseService_ = $partyService;
    }

    public function index(Request $request)
    {
        try {
            return $this->purchaseService_->purchasesList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->purchaseService_->createPurchase();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreatePurchaseRequest $request)
    {
        try {
            return $this->purchaseService_->storePurchase($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($id)
    {
        try {
            return $this->purchaseService_->showPruchase($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit($id)
    {
        try {
            return $this->purchaseService_->editPurchase($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdatePurchaseRequest $request, $id)
    {
        try {
            return $this->purchaseService_->updatePurchase($request, $id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function purchaseTransaction($id)
    {
        try {
            return $this->purchaseService_->purchaseTransaction($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function storePurchaseTransaction(Request $request)
    {
        try {
            return $this->purchaseService_->storePurchaseTransaction($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function destroy($id)
    // {
    //     try {
    //         return $this->partyService_->deleteClientAccount($id);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         return $this->serverSQLError($ex);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }
}
