<?php

namespace App\Http\Controllers\Purchases\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePurchaseRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'party_id' => ['required'],
            'invoice_no' => ['required'],
            'total_amount' => ['required'],
            'net_amount' => ['required'],
        ];
    }
}
