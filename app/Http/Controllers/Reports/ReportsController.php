<?php

namespace App\Http\Controllers\Reports;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Services\ReportService;
use App\Http\Services\InvoiceService;
use App\Http\Services\SupplyReportService;
use App\Http\Services\ClientsReportsService;
use App\Http\Services\ProductsReportsService;
use App\Http\Services\ShortStockReportsService;

use function App\shopId;

class ReportsController extends Controller
{

    private $reportService_;
    private $productReportService_;
    private $shortStockReportService_;
    private $clientsReportService_;
    private $supplyReportService_;

    public function __construct(
        ReportService $productService,
        ProductsReportsService $productReportService,
        ShortStockReportsService $shortStockReportService,
        ClientsReportsService $clientsReportService,
        InvoiceService $invoiceService,
        SupplyReportService $supplyReportService
    ) {
        $this->reportService_ = $productService;
        $this->productReportService_ = $productReportService;
        $this->shortStockReportService_ = $shortStockReportService;
        $this->clientsReportService_ = $clientsReportService;
        $this->invoiceService_ = $invoiceService;
        $this->supplyReportService_ = $supplyReportService;

    }

    public function index()
    {
        try {
            $products = DB::table('products')->where('shop_id', shopId())->count();
            $clients = DB::table('clients')->where('shop_id', shopId())->count();
            $shortStocks = DB::table('short_stocks')->where('shop_id', shopId())->count();
            $todayInvoices = DB::table('invoices')->where('shop_id', shopId())->whereDate('created_at', Carbon::today())->count();
            return view('admin.reports.reports', compact('products', 'clients', 'shortStocks', 'todayInvoices'));
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function productsReport(Request $request)
    {
        try {
            return $this->reportService_->productsReport($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function getInvoices(Request $request)
    {
        try {
            return $this->invoiceService_->invoiceList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
    
    public function supplyReport(Request $request)
    {
        try {
            return $this->supplyReportService_->supplyReportGenerate($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function productsReportGenerate(Request $request)
    {
        try {
            return $this->productReportService_->productsReport($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function productsAudit()
    {
        return $this->productReportService_->productsAudit();
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function productsAuditReportGenerate(Request $request)
    {
        return $this->productReportService_->productsAuditReportGenerate($request);
        try {
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function clientsReport(Request $request)
    {
        try {
            return $this->reportService_->clientsBalanceReport($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function clientsReportGenerate(Request $request)
    {
        try {
            return $this->clientsReportService_->clientsReport($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }


    public function shortStockReport(Request $request)
    {
        try {
            return $this->shortStockReportService_->shortStockReport($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function shortReportGenerate(Request $request)
    {
        try {
            return $this->shortStockReportService_->shortReportGenerate($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
