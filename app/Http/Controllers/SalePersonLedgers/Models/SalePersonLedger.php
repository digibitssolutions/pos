<?php

namespace App\Http\Controllers\SalePersonLedgers\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SalePersonLedger extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id', 'sale_person_id', 'client_id', 'date', 'paid', 'payable', 'payment_type', 'description'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

}
