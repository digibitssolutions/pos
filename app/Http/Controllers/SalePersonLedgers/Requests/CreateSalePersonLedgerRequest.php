<?php

namespace App\Http\Controllers\SalePersonLedgers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSalePersonLedgerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sale_person_id' => ['required', 'exists:sale_persons,id'],
            'client_id' => ['required', 'exists:clients,id'],
            'payment_type' => ['required'],
            'mode' => ['required'],
            'amount' => ['required']
        ];
    }
}
