<?php

namespace App\Http\Controllers\SalePersonLedgers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\SalesPerson\Models\SalePerson;
use App\Http\Controllers\SalePersonLedgers\Models\SalePersonLedger;
use App\Http\Controllers\SalePersonLedgers\Requests\CreateSalePersonLedgerRequest;

use function App\shopId;

class SalePersonLedgerController extends Controller
{
    private $thisModel_;

    public function __construct(SalePersonLedger $model)
    {
        $this->thisModel_ = $model;
    }

    public function index(Request $request)
    {
        $ledgers = $this->thisModel_->where('shop_id', shopId())
            ->with('client.user:id,first_name,last_name')
            ->when(!empty($request->sale_person_id), function ($query) use ($request) {
                return $query->where('sale_person_id', $request->sale_person_id);
            })
            ->get();

        $salePersons = SalePerson::where('shop_id', shopId())
            ->with('user:id,first_name,last_name')->get();

        $clients = Client::where('shop_id', shopId())
            ->with('user:id,first_name,last_name')->get();

        return view('admin.sale-person-ledgers.sale-person-ledger-listing', compact('ledgers', 'salePersons', 'clients'));
    }

    public function store(CreateSalePersonLedgerRequest $request)
    {
        $data = $request->all();

        $salePersonLedger = $this->thisModel_->fill($data);
        $salePersonLedger->shop_id = shopId();
        if ($data['mode'] == 'paid') {
            $salePersonLedger->paid = $data['amount'];
        } else {
            $salePersonLedger->payable = $data['amount'];
        }
        if ($salePersonLedger->save()) {
            return response(['message' => 'Transaction Added successfully']);
        }
        return response(['error-message' => 'Something went wrong!']);
    }
}
