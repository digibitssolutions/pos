<?php

namespace App\Http\Controllers\SalesPerson\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalePerson extends Model
{

    use SoftDeletes;

    protected $table = 'sale_persons';

    protected $fillable = [
        'shop_id',
        'user_id'
    ];


    public function salesPersonFilter($request)
    {
        return SalePerson::when(!empty($request->first_name), function ($query) use ($request) {
            return $query->where('first_name', 'like', '%' . $request->first_name . '%');
        })
            ->when(!empty($request->last_name), function ($query) use ($request) {
                return $query->where('last_name', 'like', '%' . $request->last_name . '%');
            })
            ->when(!empty($request->mobile), function ($query) use ($request) {
                return $query->where('mobile', $request->mobile);
            })
            ->when(!empty($request->email), function ($query) use ($request) {
                return $query->where('email', 'like', '%' . $request->email . '%');
            })
            ->when(!empty($request->city), function ($query) use ($request) {
                return $query->where('city', 'like', '%' . $request->city . '%');
            })
            ->when(!empty($request->state), function ($query) use ($request) {
                return $query->where('state', 'like', '%' . $request->state . '%');
            })
            ->when(!empty($request->zip), function ($query) use ($request) {
                return $query->where('zip', 'like', '%' . $request->zip . '%');
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function scopeWithUser($query)
    {
        return $query->with(['user' => function ($query) {
            return $query->with('userDetail');
        }]);
    }
}
