<?php

namespace App\Http\Controllers\SalesPerson\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSalesPersonRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:20'],
            'last_name' => ['required', 'string', 'max:20'],
            'email' => ['required', 'unique:users', 'email'],
        ];
    }
}
