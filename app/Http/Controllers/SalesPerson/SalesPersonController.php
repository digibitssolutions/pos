<?php

namespace App\Http\Controllers\SalesPerson;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\SalesPersonService;
use App\Http\Controllers\SalesPerson\Requests\CreateSalesPersonRequest;
use App\Http\Controllers\SalesPerson\Requests\UpdateSalesPersonRequest;

use function App\shopId;

class SalesPersonController extends Controller
{

    private $salesPersonService_;

    public function __construct(SalesPersonService $salesPersonService)
    {
        $this->salesPersonService_ = $salesPersonService;
    }

    public function index(Request $request)
    {
        try {
            return $this->salesPersonService_->salesPersonList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function create()
    // {
    //     return view('admin.sales-person.add-sales-person');
    // }

    public function store(CreateSalesPersonRequest $request)
    {
        try {
            return $this->salesPersonService_->createSalesPerson($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($id)
    {
        try {
            return $this->salesPersonService_->singleSalesPersonRecord($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function edit($id)
    // {
    //     $salesPerson = SalePerson::where('shop_id', shopId())->where('id', $id)->first();
    //     return view('admin.sales-person.edit-sales-person', compact('salesPerson'));
    // }

    public function update(UpdateSalesPersonRequest $request)
    {
        try {
            return $this->salesPersonService_->updateSalesPersonAccount($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    // public function destroy($id)
    // {
    //     try {
    //         return $this->salesPersonService_->deleteSalesPersonAccount($id);
    //     } catch (\Illuminate\Database\QueryException $ex) {
    //         return $this->serverSQLError($ex);
    //     } catch (Exception $ex) {
    //         return $this->serverError($ex);
    //     }
    // }
}
