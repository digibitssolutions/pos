<?php

namespace App\Http\Controllers\Shops\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Groups\Models\Group;
use App\Http\Controllers\Generics\Models\VatCode;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\StockIntake\Models\StockIntake;
use App\Http\Controllers\MeasurementUnits\Models\MeasurementUnit;

class Shop extends Model
{

    protected $table = 'shops';

    protected $fillable = [
        'id',
        'user_id',
        'shop_code',
        'shop_name',
    ];


    public function shopFilter($request)
    {
       return Shop::when(!empty($request->shop_name), function ($query) use ($request) {
            return $query->where('shop_name', 'like', '%' . $request->shop_name. '%');
        })
        ->when(!empty($request->shop_code), function ($query) use ($request) {
            return $query->where('shop_code',  $request->shop_code);
        })
        ->when(!empty($request->from), function ($query) use ($request) {
            return  $query->whereDate('created_at', '>=', $request->from);
        })
        ->when(!empty($request->to), function ($query) use ($request) {
            return  $query->whereDate('created_at', '<=', $request->to);
        }); 
    }
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function groups(){
        return $this->hasMany(Group::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function measurementUnits(){
        return $this->hasMany(MeasurementUnit::class);
    }

    public function vatCodes(){
        return $this->hasMany(VatCode::class);
    }

    public function stockIntake(){
        return $this->hasMany(StockIntake::class);
    }

    public function invoices(){
        return $this->hasMany(Invoice::class);
    }

}
