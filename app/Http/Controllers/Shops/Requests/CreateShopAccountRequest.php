<?php
namespace App\Http\Controllers\Shops\Requests;

use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateShopAccountRequest extends FormRequest
{

   public function authorize()
   {
       return true;
   }


   public function rules()
   {
       return [
            'shop_code' => ['unique:shops'],
            'shop_name' => ['nullable', 'string'],
            'phone' => ['nullable','string','max:20'],
            'image' => ['nullable','string','max:20'],
       		'email' => ['required', 'unique:users,email'],
       		'password' => ['required',
                Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->symbols()
                ->uncompromised()
            ],
            'password_confirmation' => ['required', 'min:8'],
       		'postal_code' => ['nullable', 'integer'],
       		
       ];
   }

   protected function failedValidation(Validator $validator)
   {
       throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
   }
}
