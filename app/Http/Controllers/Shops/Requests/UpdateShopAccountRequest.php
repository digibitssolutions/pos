<?php
namespace App\Http\Controllers\Shops\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateShopAccountRequest extends FormRequest
{

   public function authorize()
   {
       return true;
   }


   public function rules()
   {
       return [

            'id'=>['required','exists:users,id'],
            'shop_name' => ['nullable', 'string'],
            'phone' => ['nullable','string','max:20'],
            'image' => ['nullable','string','max:20'],
            'email' => ['required', 'unique:users,email,'.$this->id],
            'password' => ['nullable', 'min:6'],
       		'postal_code' => ['nullable', 'integer'],

       ];
   }


}

?>
