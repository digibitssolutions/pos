<?php

namespace App\Http\Controllers\Shops;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Members\Requests\ForgotPasswordRequest;
use App\Http\Controllers\Members\Requests\MemberChangePasswordRequest;
use App\Http\Controllers\Members\Requests\ResendTokenRequest;
use App\Http\Controllers\Members\Requests\ResetPasswordRequest;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\Shops\Requests\AccountVerificationRequest;
use App\Http\Controllers\Shops\Requests\CreateShopAccountRequest as CreateRequest;
use App\Http\Controllers\Shops\Requests\UpdateShopAccountRequest  as UpdateRequest;
use App\Http\Services\ShopService;
use App\User;
use Illuminate\Support\Facades\Validator;


class ShopsController extends Controller
{

    private $shopService_;

    public function __construct(ShopService $shopService)
    {
        //$this->middleware('auth:api');
        $this->shopService_ = $shopService;
    }

    public function index(Request $request)
    {
        try {
            return $this->shopService_->shopsList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateRequest $request)
    {
        try {
            return $this->shopService_->createShopAccount($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($shopId)
    {
        try {
            return $this->shopService_->shopSingleRecord($shopId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateRequest $request)
    {
        try {
            return $this->shopService_->updateShopAccount($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->shopService_->deleteShopAccount($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function verify(AccountVerificationRequest $request)
    {
        try {
            return $this->memberService_->verifyAccount($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        try {
            return $this->memberService_->forgotPassword($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            return $this->memberService_->resetPassword($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function resendToken(ResendTokenRequest $request)
    {
        try {
            return $this->memberService_->resendToken($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function changePassword(MemberChangePasswordRequest $request)
    {
        try {
            return $this->memberService_->changePassword($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
