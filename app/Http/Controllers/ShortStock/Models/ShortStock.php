<?php

namespace App\Http\Controllers\ShortStock\Models;

use App\Http\Controllers\Products\Models\Product;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class ShortStock extends BaseModel
{

    protected $table = 'short_stocks';

    protected $fillable = [
        'shop_id',
        'product_id',
        'old_stock',
        'required_stock'
    ];

    public function shortStockFilters($request)
    {
        return ShortStock::when(!empty($request->product_name), function ($query) use ($request) {
            return $query->whereRelation('product', 'product_name', 'like', '%' . $request->product_name . '%');
        })
            ->when(!empty($request->master_stock), function ($query) use ($request) {
                return $query->where('old_stock', '=', $request->master_stock);
            })
            ->when(!empty($request->required_stock), function ($query) use ($request) {
                return $query->where('required_stock', '=', $request->required_stock);
            });
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
