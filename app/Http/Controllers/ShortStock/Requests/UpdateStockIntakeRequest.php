<?php

namespace App\Http\Controllers\StockIntake\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStockIntakeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product_id' => ['required', 'exists:products,id'],
            'party_id' => ['required', 'exists:parties,id'],
            'stock' => ['required'],
        ];
    }
}
