<?php

namespace App\Http\Controllers\ShortStock;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\ShortStock\Models\ShortStock;
use App\Http\Controllers\StockIntake\Models\MasterStock;

use function App\shopId;

class ShortStockController extends Controller
{
    const RECORD_CREATED = 'Short stock added successfully.';
    const RECORD_NOT_CREATED = 'Short stock not added.';

    private $ThisModel_;

    public function __construct(ShortStock $model)
    {
        $this->ThisModel_ = $model;
    }

    public function index()
    {
        if (Auth::user()->type == EnumsList::SHOP) {
            $shortStocks = MasterStock::where('shop_id', shopId())->where('master_stock', '<=', '10')->get();
            return view('admin.short-stock.add-short-stock', compact('shortStocks'));
        }
    }

    public static function shortStockNotify()
    {
        return MasterStock::where('shop_id', shopId())->where('master_stock', '<=', '10')->count();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $shortProducts = $request->required_stock;
        $shortStocks = [];
        foreach ($shortProducts as $key => $shortProduct) {
            $shortStocks[$key] = [
                'shop_id' => shopId(),
                'product_id' => $request->product_id[$key],
                'old_stock' => $request->old_stock[$key],
                'required_stock' => $request->required_stock[$key],
                'created_at' => $this->ThisModel_->freshTimestamp(),
                'updated_at' => $this->ThisModel_->freshTimestamp(),
            ];
            if (!isset($request->required_stock[$key])) {
                unset($shortStocks[$key]);
            }
        }
        $isRecordAdded = $this->ThisModel_->insert($shortStocks);
        if (!$isRecordAdded) {
            return back()->with('error-message', self::RECORD_NOT_CREATED);
        }
        return back()->with('message', self::RECORD_CREATED);
    }

    public function shortStockListing(Request $request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $shortStocks = $this->ThisModel_->shortStockFilters($request)->where('shop_id', shopId())->get();
            return view('admin.short-stock.short-listing', compact('shortStocks'));
        }
    }
}
