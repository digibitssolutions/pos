<?php

namespace App\Http\Controllers\StockIntake\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\Products\Models\Product;

class MasterStock extends Model
{

    protected $table = 'master_stock';

    protected $fillable = [
        'shop_id',
        'product_id',
        'master_stock',
    ];

    // public function stockIntakeFilter($request){
    //    return StockIntake::when(!empty($request->from), function ($query) use ($request) {
    //         return  $query->whereDate('created_at', '>=', $request->from);
    //     })
    //     ->when(!empty($request->to), function ($query) use ($request) {
    //         return  $query->whereDate('created_at', '<=', $request->to);
    //     });
    // }

    // public function shop(){
    //     return $this->belongsTo(Shop::class, 'shop_id');
    // }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
