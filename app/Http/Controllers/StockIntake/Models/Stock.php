<?php

namespace App\Http\Controllers\StockIntake\Models;

use App\Http\Controllers\MediaDrive\Models\MediaDrive;
use App\Http\Controllers\Shops\Models\Shop;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Parties\Models\Party;
use App\Models\BaseModel;

class Stock extends BaseModel
{

    use SoftDeletes;

    protected $table = 'stock';

    protected $fillable = [
        'shop_id',
        'party_id',
        'bill_no',
        'bill_date',
        'shipping_date',
        'payment_type',
        'subject',
        'notes',
        'terms_conditions',
        'shipping_charges',
        'total_amount',
        'received_amount',
        'remaining_amount',
    ];

    public function stockFilters($request)
    {
        return Stock::when(isset($request->bill_no), function ($query) use ($request) {
            return $query->where('bill_no', $request->bill_no);
        })
        ->when(isset($request->party_id), function ($query) use ($request) {
                return $query->where('party_id', $request->party_id);
            })
            ->when(isset($request->bill_date), function ($query) use ($request) {
                return $query->whereDate('bill_date', $request->bill_date);
            })
            ->when(isset($request->payment_type), function ($query) use ($request) {
                return $query->where('payment_type', $request->payment_type);
            })
            ->when(isset($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(isset($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function party()
    {
        return $this->belongsTo(Party::class, 'party_id');
    }

    // public function scopeWithClient($query)
    // {
    //     return $query->with(['client.user' => function ($query) {
    //         return $query->with('userDetail');
    //     }]);
    // }

    public function intakeStock()
    {
        return $this->hasMany(StockIntake::class);
    }

    public function media()
    {
        return $this->hasOne(MediaDrive::class, 'relation_id');
    }
}
