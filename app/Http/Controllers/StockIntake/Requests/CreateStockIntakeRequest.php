<?php

namespace App\Http\Controllers\StockIntake\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStockIntakeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product_id' => ['required', 'exists:products,id'],
            'party_id' => ['required', 'exists:parties,id'],
            'stock' => ['required'],
            'price' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'product_id.required' => 'Product feild is required.',
            'party_id.required' => 'Party feild is required.',
            'party_id.exists' => 'Party account does not exists.',
            'stock.required' => 'Stock feild is required.',
            'price.required' => 'Product price is required.'
        ];
    }
}
