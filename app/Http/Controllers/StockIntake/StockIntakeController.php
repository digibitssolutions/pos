<?php

namespace App\Http\Controllers\StockIntake;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\StockIntakeService;
use App\Http\Controllers\StockIntake\Requests\CreateStockIntakeRequest;
use App\Http\Controllers\StockIntake\Requests\UpdateStockIntakeRequest;

class StockIntakeController extends Controller
{

    private $stockIntakeService_;

    public function __construct(StockIntakeService $stockIntakeService)
    {
        $this->stockIntakeService_ = $stockIntakeService;
    }

    public function index(Request $request)
    {
        try {
            return $this->stockIntakeService_->stockIntakeList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->stockIntakeService_->createStockIntake();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateStockIntakeRequest $request)
    {
        try {
            return $this->stockIntakeService_->storeStockIntake($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            throw $ex;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function show($stockIntakeId)
    {
        try {
            return $this->stockIntakeService_->singleStockIntake($stockIntakeId);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateStockIntakeRequest $request)
    {
        try {
            return $this->stockIntakeService_->updateStockIntake($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->stockIntakeService_->deleteStockIntake($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function singleStockIntakeProduct($id)
    {
        try {
            return $this->stockIntakeService_->singleProduct($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
