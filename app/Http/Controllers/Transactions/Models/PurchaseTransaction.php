<?php

namespace App\Http\Controllers\Transactions\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseTransaction extends Model
{
    protected $fillable = [
        'shop_id',
        'party_id',
        'transaction_type_id',
        'purchase_id',
        'transaction_id',
        'credit',
        'debit',
        'remaining_credit',
        'note',
        'payment_via',
        'payment_via_mobile',
        'payment_via_cnic',
    ];
}
