<?php

namespace App\Http\Controllers\Transactions\Models;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Invoices\Models\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id',
        'client_id',
        'transaction_type_id',
        'invoice_id',
        'transaction_id',
        'credit',
        'debit',
        'remaining_credit',
        'note',
        'payment_via',
        'payment_via_mobile',
        'payment_via_cnic',
        'transaction_date',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
