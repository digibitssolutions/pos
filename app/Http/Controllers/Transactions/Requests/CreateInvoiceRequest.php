<?php

namespace App\Http\Controllers\Invoices\Requests;

use Illuminate\Validation\Rule;
use App\Http\Services\BaseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateInvoiceRequest extends FormRequest
{
    private $baseService_;

    public function __construct(BaseService $baseService)
    {
        $this->baseService_ = $baseService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sales_person' => ['nullable'],
            'subject' => ['nullable'],
            'invoice_no' => [Rule::unique('invoices')->where(function ($query) {
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'order_no' => [Rule::unique('invoices')->where(function ($query) {
                return $query->where('shop_id', $this->baseService_->shopId());
            })],
            'invoice_date' => ['required', 'date'],
            'delivery_date' => ['required', 'date'],
            'total_amount' => ['required'],
            'item_details' => ['required'],
            'quantity' => ['required'],
        ];
    }

    // protected function failedValidation(Validator $validator)
    // {
    //     throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
    // }
}
