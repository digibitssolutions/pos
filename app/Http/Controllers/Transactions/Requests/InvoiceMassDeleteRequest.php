<?php

namespace App\Http\Controllers\Invoices\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceMassDeleteRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "id" => ['required', 'exists:invoices,id', 'array'],
        ];
    }
}
