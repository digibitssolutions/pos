<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Services\TransactionService;
use Exception;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionService_;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService_ = $transactionService;
    }

    public function index()
    {
        try {
            return $this->transactionService_->invoiceTransactionListing();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create($id)
    {
        try {
            return $this->transactionService_->invoiceTransaction($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(Request $request)
    {
        try {
            return $this->transactionService_->storeInvoiceTransaction($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function show($id)
    {
        try {
            return $this->transactionService_->showInvoiceTransactions($id);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
