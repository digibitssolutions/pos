<?php

namespace App\Http\Controllers\UserDetails\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    use HasFactory;

    protected $table = 'users_details';

    protected $fillable = [
        'user_id',
        'created_by',
        'phone',
        'mobile',
        'designation',
        'department',
        'website',
        'address',
        'city',
        'zip',
        'tehsil',
        'district',
        'state',
        'fax',
        'facebook',
        'twitter',
    ];
}
