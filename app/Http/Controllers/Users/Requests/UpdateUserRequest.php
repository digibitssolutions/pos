<?php
namespace App\Http\Controllers\Users\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateUserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => ['required', 'exists:users,id'],
            'name' => ['required'],
            'email' => ['required', Rule::unique('users')->ignore($this->id)],
            'password' => ['nullable', 'min:6'],
            'email_verified' => ['boolean'],
            'status' => [Rule::in(['Inactive', 'Active'])],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422));
    }
}

?>
