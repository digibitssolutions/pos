<?php

namespace App\Http\Controllers\Users;

use Exception;
use Illuminate\Http\Request;
use App\Http\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\Requests\UpdateUserRequest;
use App\Http\Controllers\Users\Requests\CreateAdminRequest;
use App\Http\Controllers\Users\Requests\CreateUserRequest;

class UsersController extends Controller
{

    private $userService_;

    public function __construct(UserService $userService)
    {
        $this->userService_ = $userService;
    }

    public function index(Request $request)
    {
        try {
            return $this->userService_->usersList($request);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function create()
    {
        try {
            return $this->userService_->addUser();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function store(CreateUserRequest $request)
    {
        try {
            return $this->userService_->storeUser($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function edit()
    {
        try {
            return $this->userService_->editUser();
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function update(UpdateUserRequest $request)
    {
        try {
            return $this->userService_->updateUser($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->userService_->deleteUser($id);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }

    public function addAdmin(CreateAdminRequest $request)
    {
        try {
            return $this->userService_->addAdmin($request);
        } catch (\Illuminate\Database\QueryException $ex) {
            return $this->serverSQLError($ex);
        } catch (Exception $ex) {
            return $this->serverError($ex);
        }
    }
}
