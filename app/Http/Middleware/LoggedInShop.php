<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use function App\checkShop;

class LoggedInShop
{

    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        // return Auth()->user()->type;
        // return checkShop();
    }
}
