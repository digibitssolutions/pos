<?php

namespace App\Http\Resources\Brands;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandsResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop_id' => is_null($this->shop_id) ? '--' : $this->shop_id,
            'group_id' => is_null($this->group_id) ? '--' : $this->group_id,
            'brand_name' => $this->brand_name,
            'code' => $this->code,
            'short_description' => is_null($this->short_description) ? '--' : $this->short_description,
            'shop' => [
                'shop_name' => is_null($this->shop_id) ? '--' : $this->shop->shop_name ,
                'shop_code' => is_null($this->shop_id) ? '--' : $this->shop->shop_code
            ],
            'group' => [
                'group_name' => is_null($this->group_id) ? '--' : $this->group->group_name ,
                'code' => is_null($this->group_id) ? '--' : $this->group->code
            ],
            'created_at' => $this->created_at,
        ];
    }
}
