<?php

namespace App\Http\Resources\FieldOptionType;

use Illuminate\Http\Resources\Json\JsonResource;

class FieldOptionTypeResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type_description' => $this->type_description,
            'comment' => is_null($this->comment) ? '--' : $this->comment,
        ];
    }
}
