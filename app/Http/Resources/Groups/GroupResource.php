<?php

namespace App\Http\Resources\Groups;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'group_name' => $this->group_name,
            'code' => $this->code,
            'short_description' => is_null($this->short_description) ? '--' : $this->short_description,
            'shop' => [
                'shop_name' => is_null($this->shop_id) ? '--' : $this->shop->shop_name ,
                'shop_code' => is_null($this->shop_id) ? '--' : $this->shop->shop_code
            ],
            'created_at' => $this->created_at,
        ];
    }
}
