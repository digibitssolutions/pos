<?php

namespace App\Http\Resources\History;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoryResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop_id' => is_null($this->shop_id) ? '--' : $this->shop_id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'status' => $this->status,
            'ip' => is_null($this->ip) ? '--' : $this->ip,
            'login_time' => is_null($this->login_time) ? '--' : $this->login_time,
            'logout_time' => is_null($this->logout_time) ? '--' : $this->logout_time,
        ];
    }
}
