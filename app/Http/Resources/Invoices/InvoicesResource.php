<?php

namespace App\Http\Resources\Invoices;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoicesResource extends JsonResource
{

    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'shop_id' => is_null($this->shop_id) ? '--' : $this->shop_id,
            'customer' => is_null($this->customer) ? '--' : $this->customer,
            'sales_person' => $this->sales_person,
            'invoice_no' => $this->invoice_no,
            'order_no' => is_null($this->order_no) ? '--' : $this->order_no,
            'invoice_date' => $this->invoice_date,
            'delivery_date' => $this->delivery_date,
            'payment_type' => $this->payment_type,
            'subject' => $this->subject,
            'customer_notes' => is_null($this->customer_notes) ? '--' : $this->customer_notes,
            'terms_conditions' => is_null($this->terms_conditions) ? '--' : $this->terms_conditions,
            'total_amount' => $this->total_amount,
            'shipping_charges' => is_null($this->shipping_charges) ? '--' : $this->shipping_charges,
            'created_at'  => $this->created_at,
            'invoice_products' => $this->invoiceProducts,
        ];
    }
}
