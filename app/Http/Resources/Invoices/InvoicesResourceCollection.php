<?php

namespace App\Http\Resources\Invoices;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoicesResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop_id' => is_null($this->shop_id) ? '--' : $this->shop_id,
            'invoice_no' => $this->invoice_no,
            'order_no' => is_null($this->order_no) ? '--' : $this->order_no,
            'payment_type' => $this->payment_type,
            'total_amount' => $this->total_amount,
            'created_at'  => $this->created_at
        ];
    }
}
