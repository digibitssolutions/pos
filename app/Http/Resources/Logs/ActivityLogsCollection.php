<?php

namespace App\Http\Resources\Logs;

use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Modules\Models\Module;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityLogsCollection extends JsonResource
{
    // const CREATED = 'created ';
    // const DISPLAY = 'viewed ';
    // const UPDATED = 'updated ';
    // const DELETED = 'deleted ';

    // public function toArray($request)
    // {
    //     $loggedUser = auth()->user()->first_name . ' ' . auth()->user()->last_name;
    //     if ($this->old_data != null) {
    //         $oldUser = json_decode($this->old_data);
    //         $oldUserAccount = $oldUser->account_name;
    //         $moduleName = $this->getModuleName($this->module_id);
    //         switch ($this->action) {
    //             case LogAction::Display:
    //                 $client = ActivityLogsCollection::DISPLAY . $moduleName . ' Record Ref' . '(#id:' . $oldUser->id . ') @' .$oldUserAccount;
    //                 break;
    //             case LogAction::Created:
    //                 $client = ActivityLogsCollection::CREATED . $moduleName . ' Record Ref' . '(#id:' . $oldUser->id . ') @' .$oldUserAccount;
    //                 break;
    //             case LogAction::Updated:
    //                 $client = ActivityLogsCollection::UPDATED . $moduleName . ' Record Ref' . '(#id:' . $oldUser->id . ') @' .$oldUserAccount;
    //                 break;
    //             case LogAction::Deleted:
    //                 $client = ActivityLogsCollection::DELETED . $moduleName . ' Record Ref' . '(#id:' . $oldUser->id . ') @' .$oldUserAccount;
    //                 break;
    //             default:
    //                 $client = '';
    //         }
    //     } else {
    //         $client = '';
    //     }
    //     return [
    //         'id' => $this->id,
    //         'type' => $this->type,
    //         'created_at' => $this->created_at,
    //         'message' => $loggedUser . ', ' . $client
    //     ];
    // }

    // private function getModuleName(int $moduleId_)
    // {
    //     $module = Module::where('id', $moduleId_)->first();
    //     if ($module) {
    //         return $module->name;
    //     }
    //     return $moduleId_;
    // }
}
