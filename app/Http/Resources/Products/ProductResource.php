<?php

namespace App\Http\Resources\Products;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'code' => $this->code,
            'short_description' => is_null($this->short_description) ? '--' : $this->short_description,
            'shop' => [
                'shop_name' => $this->shop->shop_name,
                'shop_code' => $this->shop->shop_code
            ],
            'group' => [
                'group_name' => $this->group->group_name,
                'code' => $this->group->code
            ],
            'category' => [
                'category_name' => $this->category->category_name,
                'code' => $this->category->code
            ],
            'measurement_unit' => [
                'name' => is_null($this->measurement_unit) ? '--' :  $this->measurementUnit->name
            ],
            'vat_code' => [
                'name' => is_null($this->vatCode) ? '--' : $this->vatCode->name,
                'param_1' => is_null($this->vatCode) ? '--' : $this->vatCode->param_1,
            ],
            'unit_price' => $this->unit_price,
            'purchase_price' => is_null($this->purchase_price) ? '--' : $this->purchase_price,
            'sale_price' => is_null($this->sale_price) ? '--' : $this->sale_price,
            'discount' => is_null($this->discount) ? '--' : $this->discount,
            'special_price' => is_null($this->special_price) ? '--' : $this->special_price,
            'expiry_date' => is_null($this->expiry_date) ? '--' : $this->expiry_date,
            'varient' => is_null($this->varient) ? '--' : $this->varient,
            'created_at' => $this->created_at,
        ];
    }
}
