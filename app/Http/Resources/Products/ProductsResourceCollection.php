<?php

namespace App\Http\Resources\Products;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'code' => $this->code,
            'unit_price' => $this->unit_price,
            'category' => [
                'name' => is_null($this->category_id) ? '--' : $this->category->category_name,
            ],
            'measurement_unit' => [
                'name' => is_null($this->measurement_unit) ? '--' :  $this->measurementUnit->name
            ],
            'created_at' => $this->created_at,
        ];
    }
}
