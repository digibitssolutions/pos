<?php

namespace App\Http\Resources\StockIntake;

use Illuminate\Http\Resources\Json\JsonResource;

class StockIntakeResourceCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop_id' => is_null($this->shop_id) ? '--' : $this->shop_id,
            'product_id' => $this->product_id,
            'stock' => $this->stock,
            'created_at'  => $this->created_at
        ];
    }
}
