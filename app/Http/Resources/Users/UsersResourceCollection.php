<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResourceCollection extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "type" => $this->type,
            "email_verified" => $this->email_verified,
            "status" => $this->status,
            "created_at" => $this->created_at,
        ];
    }
}
