<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Enums\EnumsList;
use Symfony\Component\HttpFoundation\Response;

class BaseService
{

    public function shopId()
    {
        $id = null;
        $type =  Auth::user()->type;
        if ($type == 'Admin') {
            $id = null;
        } else {
            $id = Auth::user()->shop->id;
        }
        return $id;
    }

    public function checkShopOrSystem()
    {
        if ($this->type() == EnumsList::SHOP)
            return $this->shopId();
        else
            return NULL;
    }

    public function type()
    {
        return Auth::user()->type;
    }

    public static function userId()
    {
        return Auth::user()->id;
    }

    public function created(array $data, int $code = Response::HTTP_OK)
    {
        return response()->json($data, $code);
    }

    public function noRecord(array $data, int $code = Response::HTTP_NOT_FOUND)
    {
        return response()->json($data, $code);
    }

    public function deleted(array $data, int $code = Response::HTTP_OK)
    {
        return response()->json($data, $code);
    }
}
