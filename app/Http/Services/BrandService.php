<?php
namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use App\Http\Resources\Brands\BrandResource;
use App\Http\Controllers\Brands\Models\Brand;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Brands\BrandsResourceCollection;

class BrandService extends BaseService{

    const BRAND_CREATED = 'Brand created successfully.';
    const BRAND_DELETED = 'Brand record deleted successfully.';
    const RECORD_FAILD = 'Brand account not created.';
    const RECORD_NOT_FOUND = 'Brands record not found.';
    const SINGLE_RECORD_NOT_FOUND = 'Brand record not found.';
    const BRAND_ACCOUNT_UPDATED = 'Brand record updated successfully.';
    const COLLECTION_NAME = 'brands';

    private $model_;

    public function __construct(Brand $model) {
        $this->model_ = $model;
    }

    public function brandsList($request){
        if($this->type() == EnumsList::SHOP){
            $brands = $this->model_->brandsFilter($request)->where('shop_id', $this->shopId())->paginate(EnumsList::Pagination);
        }else{
            $brands = $this->model_->brandsFilter($request)->paginate(EnumsList::Pagination);
        }
        if ($brands->isNotEmpty()) {
            return BrandsResourceCollection::collection($brands);
        }
        return response()->json(['message' => BrandService::RECORD_NOT_FOUND],Response::HTTP_NOT_FOUND);
    }

    public function createBrand($request){
        $data = $request->all();
        if($this->type() == EnumsList::SHOP){
            $data['shop_id'] = $this->shopId();
        }
        $isBrandCreated = Brand::create($data);
        if($isBrandCreated){
            return response()->json(['message' => BrandService::BRAND_CREATED],Response::HTTP_CREATED);
        }
    }

    public function singleBrand($id){
        if($this->type() == EnumsList::SHOP){
            $brand = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        }else {
            $brand = $this->model_::find($id);
        }
        if(!empty($brand)){
            return new BrandResource($brand);
        }
        return response()->json(['message' => BrandService::SINGLE_RECORD_NOT_FOUND],Response::HTTP_OK);
    }

    public function updateBrand($request){
        $data = $request->all();
        if($this->type() == EnumsList::SHOP){
            $brand = $this->model_::where('shop_id', $this->shopId())->where('id', $data['id'])->first();
        }else{
            $brand = $this->model_::find($data['id']);
        }
        if($brand){
            $isBrandUpdated = $brand->update($data);
            if($isBrandUpdated){
                return response()->json(['message' => BrandService::BRAND_ACCOUNT_UPDATED],Response::HTTP_OK);
            }
        }
        return response()->json(['message' => BrandService::RECORD_FAILD]);
    }

    public function deleteBrand($id){
        if($this->type() == EnumsList::SHOP){
            $brand = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        }else{
            $brand = $this->model_::find($id);
        }
        if($brand){
            $brand->delete();
            return response()->json(['message' => BrandService::BRAND_DELETED],Response::HTTP_OK);
        }
        return response()->json(['message' => BrandService::RECORD_FAILD]);
    }

    public function toDigestQuery(){
        return $this->model_->select(['id','brand_name']);
    }

    public function todigest()
    {
        if($this->type() == EnumsList::SHOP){
            $brands = $this->toDigestQuery()->where('shop_id', $this->shopId())->get();
            return  response()->json([BrandService::COLLECTION_NAME => $brands],Response::HTTP_OK);
        }else{
            $brands = $this->toDigestQuery()->get();
            return  response()->json([BrandService::COLLECTION_NAME => $brands],Response::HTTP_OK);
        }
    }
} 

?>
