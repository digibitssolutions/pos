<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Cache;

class CacheService extends BaseService
{

    const PRODUCT_KEY = 'Product.';

    public function setProduct($productObject)
    {
        if ($productObject == null) {
            return false;
        }
        return Cache::forever(CacheService::PRODUCT_KEY . $productObject->id, $productObject);
    }

    public function getProduct(int $productId)
    {
        $product = Cache::get(CacheService::PRODUCT_KEY . $productId);
        return ($product != null) ? $product : $this->getProductServiceFromRegistry()->singleProduct($productId);
    }

    public function getProducts(array $productIds)
    {
        $extendedProfiles = [];
        foreach ($productIds as $_ => $productId) {
            $extendedProfiles[] = $this->getProduct($productId);
        }
        return $extendedProfiles;
    }

    private function getProductServiceFromRegistry()
    {
        return app()->make(ProductService::class);
    }
}
