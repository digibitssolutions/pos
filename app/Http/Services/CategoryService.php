<?php

namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Controllers\Categories\Models\Category;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Resources\Category\CategoryResourceCollection;

use function App\addToLog;

class CategoryService extends BaseService
{

    const CATEGORY_CREATED = 'Category created successfully.';
    const CATEGORY_DELETED = 'Category record deleted successfully.';
    const RECORD_FAILD = 'Category record not created.';
    const RECORD_NOT_FOUND = 'Categories record not found.';
    const SINGLE_RECORD_NOT_FOUND = 'Category record not found.';
    const CATEGORY_ACCOUNT_UPDATED = 'Category record updated successfully.';
    const COLLECTION_NAME = 'categories';

    private $model_;

    public function __construct(Category $model)
    {
        $this->model_ = $model;
    }

    public function categoriesList($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $categories = $this->model_->categoriesFilter($request)->where('shop_id', $this->shopId())->paginate(EnumsList::Pagination);
        } else {
            $categories = $this->model_->categoriesFilter($request)->paginate(EnumsList::Pagination);
        }
        if ($categories->isNotEmpty()) {
            return CategoryResourceCollection::collection($categories);
        }
        return $this->noRecord(['message' => CategoryService::RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    }

    public function createCategory($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $data['shop_id'] = $this->shopId();
        }
        $isCategoryCreated = $this->model_->create($data);
        if ($isCategoryCreated) {
            addToLog(LogTypeEnum::Info, null, null, $isCategoryCreated, LogAction::Created, ModuleEnum::Categories);
            return $this->created(['message' => CategoryService::CATEGORY_CREATED], Response::HTTP_CREATED);
        }
    }

    public function singleCategory($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $category = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $category = $this->model_->find($id);
        }
        if (!empty($category)) {
            return new CategoryResource($category);
        }
        return $this->noRecord(['message' => CategoryService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_OK);
    }

    public function updateCategory($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $category = $this->model_::where('shop_id', $this->shopId())->where('id', $data['id'])->first();
        } else {
            $category = $this->model_->find($data['id']);
        }
        if ($category) {
            $oldData = $category->getOriginal();
            $isCategoryUpdated = $category->update($data);
            if ($isCategoryUpdated) {
                addToLog(LogTypeEnum::Info, null, $oldData, $category, LogAction::Updated, ModuleEnum::Categories);
                return $this->created(['message' => CategoryService::CATEGORY_ACCOUNT_UPDATED], Response::HTTP_OK);
            }
        }
        return $this->noRecord(['message' => CategoryService::RECORD_FAILD], Response::HTTP_OK);
    }

    public function deleteCategory($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $category = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $category = $this->model_->find($id);
        }
        if ($category) {
            addToLog(LogTypeEnum::Info, null, $category, null, LogAction::Deleted, ModuleEnum::Categories);
            $category->delete();
            return $this->deleted(['message' => CategoryService::CATEGORY_DELETED], Response::HTTP_OK);
        }
        return $this->noRecord(['message' => CategoryService::RECORD_FAILD], Response::HTTP_OK);
    }

    public function toDigestQuery()
    {
        return $this->model_->select(['id', 'category_name']);
    }

    public function todigest()
    {
        if ($this->type() == EnumsList::SHOP) {
            $categories = $this->toDigestQuery()->where('shop_id', $this->shopId())->get();
            return $this->created([CategoryService::COLLECTION_NAME => $categories], Response::HTTP_OK);
        } else {
            $categories = $this->toDigestQuery()->get();
            return $this->created([CategoryService::COLLECTION_NAME => $categories], Response::HTTP_OK);
        }
    }
}
