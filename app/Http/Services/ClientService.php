<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Transactions\Models\Transaction;
use App\Http\Controllers\UserDetails\Models\UserDetail;
use App\User;
use Illuminate\Support\Facades\DB;

use function App\addToLog;
use function App\checkShop;
use function App\shopId;
use function App\transactionNumber;

class ClientService extends BaseService
{

    const ACCOUNT_CREATED = 'Client account created successfully.';
    const ACCOUNT_DELETED = 'Client account deleted successfully.';
    const RECORD_FAILD = 'Something went wrong.';
    const RECORD_NOT_FOUND = 'Clients record not found.';
    const SINGLE_RECORD_NOT_FOUND = 'Client record not found.';
    const CLIENT_ACCOUNT_UPDATED = 'Client account updated successfully.';
    const CLIENT_UPDATE_FAILED = 'Failed to update client.';
    const PASSWORD_UPDATED = 'Password updated successfully';
    const COLLECTION_NAME = 'clients';

    private $clientModel_;
    private $userDetailsModel_;

    public function __construct(Client $clientModel, UserDetail $userDetailsModel)
    {
        $this->clientModel_ = $clientModel;
        $this->userDetailsModel_ = $userDetailsModel;
    }

    public function clientsList($request)
    {
        $clients = $this->clientModel_->clientsFilter($request)
            ->where('shop_id', $this->shopId())
            ->with('user:id,first_name,last_name,status')
            ->orderBy('created_at', 'desc')
            ->get();
        return view('admin.clients.clients-list', compact('clients'));
    }

    public function createClientAccount($request)
    {
        $data = $request->all();
        DB::beginTransaction();
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->middle_name = $data['middle_name'];
        $user->email = $data['email'];
        $user->type = 'Shop';
        $user->password = Hash::make($data['password']);
        $user->decrypted_password = $data['password'];
        $user->status = 'Active';
        $user->save();

        $client = new Client();
        $client->user_id = $user->id;
        $client->shop_id = $this->shopId();
        $client->type = $data['type'];
        $client->save();

        $userDetail = $this->userDetailsModel_->fill($data);
        $userDetail->user_id = $user->id;
        $userDetail->created_by = $this->userId();
        $account = $userDetail->save();
        DB::commit();
        DB::rollBack();
        if ($account) {
            addToLog(LogTypeEnum::Info, null, null, $user, LogAction::Created, ModuleEnum::Clients);
            return response(['message' => ClientService::ACCOUNT_CREATED]);
        }
        return response(['error-message' => ClientService::RECORD_FAILD]);
    }

    public function singleClientRecord($id)
    {

        return $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)
            ->withUser()
            ->first();
    }

    public function client(int $id)
    {
        $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)
            ->withUser()
            ->first();
        return view('admin.clients.show-client', compact('client'));
    }

    public function updateClientAccount($request)
    {
        $data = $request->all();
        $data2 = $request->except('type');

        $client = $this->clientModel_->where('shop_id', shopId())->where('id', $request->id)->first();
        $client->update([
            'type' => $data['type']
        ]);

        $user = $client->user;
        $user->fill($data2);
        $oldData = $user->getOriginal();
        $udpatedUser = $user->save();

        $userDetail = $this->userDetailsModel_->where('user_id', $user->id)->first();
        $userDetail->fill($data);
        $updatedDetail = $userDetail->save();

        if ($udpatedUser && $updatedDetail) {
            addToLog(LogTypeEnum::Info, null, $oldData, $user, LogAction::Updated, ModuleEnum::Clients);
            return response(['message' => ClientService::CLIENT_ACCOUNT_UPDATED]);
        }
        return response(['error-message' => ClientService::CLIENT_UPDATE_FAILED]);
    }


    public function clientTransaction($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
            $invoices = Invoice::where('shop_id', $this->shopId())->where('client_id', $client->id)->get(['id']);
        }
        return view('admin.clients.add-transaction', compact('client', 'invoices'));
    }

    public function storeClientTransaction($request)
    {
        $data = $request->all();

        $client = Client::where('shop_id', $this->shopId())->where('id', $request->client_id)->first();
        $invoice = Invoice::where('shop_id', $this->shopId())->where('id', $request->invoice_id)->first();
        DB::beginTransaction();

        // CLIENT TOTAL REMAINING BALANCE START
        if ($data['transaction_amount'] <= $client->total_remaining_balance) {
            $client->total_remaining_balance = $client->total_remaining_balance - $request->transaction_amount;
            $client->save();
            if ($invoice) {
                $invoice->received_amount += $data['transaction_amount'];
                $invoice->remaining_amount -= $data['transaction_amount'];
                $invoice->save();
            }
        } else {
            return back()->with(['error-message' => TransactionService::TRANSACTION_AMOUNT_ERROR]);
        }
        // CLIENT TOTAL REMAINING BALANCE END

        // TRANSACTION START
        $transaction = new Transaction();
        $transaction->shop_id = $this->shopId();
        $transaction->client_id = $data['client_id'];
        $transaction->transaction_type_id = $data['transaction_type_id'];
        $transaction->transaction_id = transactionNumber($client->id);
        $transaction->credit = $data['transaction_amount']; //Hard Coded
        $transaction->remaining_credit = $client->total_remaining_balance;
        $transaction->note = $data['note'];
        $transaction->payment_via = $data['payment_via'];
        $transaction->payment_via_mobile = $data['payment_via_mobile'];
        $transaction->payment_via_cnic = $data['payment_via_cnic'];
        $transaction->transaction_date = $data['transaction_date'];
        $isTransactionSaved = $transaction->save();
        DB::commit();
        DB::rollback();
        // TRANSACTION END
        if ($isTransactionSaved) {
            return back()->with(['message' => TransactionService::TRANSACTION_CREATED]);
        }
        return back()->with(['error-message' => TransactionService::RECORD_FAILD]);
    }

    // public function deleteClientAccount($id)
    // {
    //     if ($this->type() == EnumsList::SHOP) {
    //         $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $client = $this->clientModel_->find($id);
    //     }
    //     if ($client) {
    //         $client->delete();
    //         return $this->deleted(['message' => ClientsService::ACCOUNT_DELETED], Response::HTTP_OK);
    //     }
    //     return $this->noRecord(['message' => ClientsService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    // }

    public function clientAccountStatement($id)
    {
        $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
        $invoices = Invoice::where('shop_id', $this->shopId())->where('client_id', $id)->get();
        $transactions = Transaction::with('invoice')->where('shop_id', $this->shopId())->where('client_id', $id)->get();
        return view('admin.clients.account-statement', compact('transactions', 'invoices'));
    }

    public function totalRemainingBalance($request)
    {
        return;
    }
}
