<?php

namespace App\Http\Services;

use App\Http\Builders\ClientsReportsQueryBuilder;
use App\Http\Controllers\Clients\Models\Client;

class ClientsReportsService extends BaseService
{

    private $reportsService_;

    public function __construct(
        ReportService $reportsService
    ) {
        $this->reportsService_ = $reportsService;
    }

    public function clientsReport($request)
    {
        $downloadFileName = 'clients-report';
        $dailySalesReportQueryBuilder = new ClientsReportsQueryBuilder(new Client(), $request);
        $reportQuery = $dailySalesReportQueryBuilder->build();
        $columns = [
            'client_name',
            'phone',
            'city',
            'total_remaining_balance',
        ];
        $title = 'Clients Balance Report';
        $meta = [];
        $type = $request->get('type', 'json');
        return $this->reportsService_->generateResponse($type, $reportQuery, $title, $meta, $columns, $downloadFileName);
    }
}
