<?php

namespace App\Http\Services;

use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Resources\Invoices\InvoicesResource;
use App\Http\Resources\Invoices\InvoicesResourceCollection;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct as ModelsInvoiceProduct;
use App\Http\Controllers\Expenses\Models\Expense;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\StockIntake\Models\MasterStock;
use Error;
use Exception;
use Illuminate\Support\Facades\Hash;

use function App\addToLog;

class ExpenseService extends BaseService
{

    const RECORD_CREATED = 'Expense created successfully.';
    const RECORD_DELETED = 'Record deleted successfully.';
    const RECORD_FAILD = 'Something went wrong.';
    const RECORD_NOT_FOUND = 'Record not found.';
    const CLIENT_ACCOUNT_UPDATED = 'Client account updated successfully.';
    const CLIENT_UPDATE_FAILED = 'Failed to update client.';

    private $expenseModel_;

    public function __construct(Expense $expenseModel)
    {
        $this->expenseModel_ = $expenseModel;
    }

    public function expensesList($request)
    {
        $expenses = $this->expenseModel_::where('shop_id', $this->shopId())->get();
        return view('admin.expenses.index', compact('expenses'));
    }

    public function createExpense()
    {
        return view('admin.expenses.add-expense');
    }

    public function storeExpense($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $data['shop_id'] = $this->shopId();
            $this->expenseModel_->create($data);
            return back()->with(['message' => ExpenseService::RECORD_CREATED]);
        }
        return back()->with(['message' => ExpenseService::RECORD_FAILD]);
    }

    public function deleteExpense($id)
    {
        return $expense = $this->expenseModel_::where([['shop_id', $this->shopId()], ['id', $id]])->first();
        if ($expense) {
            $expense->delete();
            return back()->with(['message' => ExpenseService::RECORD_DELETED]);
        }
        return back()->with(['message' => ExpenseService::RECORD_NOT_FOUND]);
    }
    // public function singleClientRecord($id)
    // {

    //     if ($this->type() == EnumsList::SHOP) {
    //         $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $client = $this->clientModel_->find($id);
    //     }
    //     if (!empty($client)) {
    //         return new ClientResource($client);
    //     }
    //     return $this->noRecord(['message' => ClientsService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_OK);
    // }

    // public function updateClientAccount($request)
    // {
    //     $data = $request->all();
    //     $data['user_id'] = $this->userId();
    //     if ($request->password == null) {
    //         $data['password'] = Hash::make($request->password);
    //     }
    //     $client = $this->clientModel_->find($request->id)->update($data);
    //     if ($client) {
    //         return $this->updated(['message' => ClientsService::CLIENT_ACCOUNT_UPDATED]);
    //     }
    //     return $this->failed(['message' => ClientsService::CLIENT_UPDATE_FAILED]);
    // }

    // public function deleteClientAccount($id)
    // {
    //     if ($this->type() == EnumsList::SHOP) {
    //         $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $client = $this->clientModel_->find($id);
    //     }
    //     if ($client) {
    //         $client->delete();
    //         return $this->deleted(['message' => ClientsService::ACCOUNT_DELETED], Response::HTTP_OK);
    //     }
    //     return $this->noRecord(['message' => ClientsService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    // }

    // public function toDigest()
    // {
    //     $clients = $this->clientModel_->select(DB::raw('id, CONCAT(first_name, " ",last_name) as name'))->get();
    //     return  response()->json([ClientsService::COLLECTION_NAME => $clients], Response::HTTP_OK);
    // }
}
