<?php

namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\Enums\LogTypeEnum;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\FieldOptionType\FieldOptionTypeResourceCollection;
use App\Http\Controllers\FieldOptionType\Models\FieldOptionType as ModelsFieldOptionType;

use function App\addToLog;

class FieldOptionTypeService extends BaseService
{

    const Record_Faild = 'Field Option Type record not created.';
    const RECORD_NOT_FOUND = 'Field type option record not found.';
    const Field_Option_Type_Created = 'Field option type created successfully.';
    const Field_Option_Type_DELETED = 'Field option type deleted successfully.';
    const Field_Type_Updated = 'Field Option Type record updated successfully.';

    private $model_;

    public function __construct(ModelsFieldOptionType $model)
    {
        $this->model_ = $model;
    }

    public function fieldOptionTypeList($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $fieldOptionTypes = $this->model_->where('shop_id', $this->shopId())->orderby('created_at', 'desc')->paginate(EnumsList::Pagination);
        } else {
            $fieldOptionTypes = $this->model_->orderby('created_at', 'desc')->paginate(EnumsList::Pagination);
        }
        if ($fieldOptionTypes->isNotEmpty()) {
            return FieldOptionTypeResourceCollection::collection($fieldOptionTypes);
        }
        return $this->noRecord(['message' => FieldOptionTypeService::RECORD_NOT_FOUND], Response::HTTP_OK);
    }

    public function createFieldOptionType($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $data['shop_id'] = $this->shopId();
        }
        $isFeildTypeCreated = $this->model_->create($data);
        addToLog(LogTypeEnum::Info, null, null, $isFeildTypeCreated, LogAction::Created, ModuleEnum::FieldOptionType);
        if ($isFeildTypeCreated) {
            return $this->created(['message' => FieldOptionTypeService::Field_Option_Type_Created], Response::HTTP_OK);
        }
    }

    public function updateFieldTypeOption($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $fieldOptionType = $this->model_::where('shop_id', $this->shopId())->where('id', $data['id'])->first();
        } else {
            $fieldOptionType = $this->model_->find($data['id']);
        }
        if ($fieldOptionType) {
            $oldData = $fieldOptionType->getOriginal();
            $isTypeUpdated = $fieldOptionType->update($data);
            if ($isTypeUpdated) {
                addToLog(LogTypeEnum::Info, null, $oldData, $fieldOptionType, LogAction::Updated, ModuleEnum::FieldOptionType);
                return $this->created(['message' => FieldOptionTypeService::Field_Type_Updated], Response::HTTP_OK);
            }
        }
        return $this->noRecord(['message' => FieldOptionTypeService::Record_Faild], Response::HTTP_OK);
    }

    public function deleteFieldOptionType($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $fieldOptionType = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $fieldOptionType = $this->model_->find($id);
        }
        if ($fieldOptionType) {
            addToLog(LogTypeEnum::Info, null, $fieldOptionType, null, LogAction::Deleted, ModuleEnum::FieldOptionType);
            $fieldOptionType->delete();
            return $this->deleted(['message' => FieldOptionTypeService::Field_Option_Type_DELETED], Response::HTTP_OK);
        }
        return $this->noRecord(['message' => FieldOptionTypeService::Record_Faild], Response::HTTP_OK);
    }
}
