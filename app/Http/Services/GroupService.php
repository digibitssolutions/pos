<?php

namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Resources\Groups\GroupResource;
use App\Http\Controllers\Groups\Models\Group;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Groups\GroupsResourceCollection;

use function App\addToLog;

class GroupService extends BaseService
{

    const COLLECTION_NAME = 'groups';
    const RECORD_NOT_FOUND = 'Groups record not found.';
    const GROUP_CREATED = 'Group created successfully.';
    const SINGLE_RECORD_NOT_FOUND = 'Group record not found.';
    const GROUP_DELETED = 'Group record deleted successfully.';
    const GROUP_RECORD_FAILD = 'Record failed some thing went wrong.';
    const GROUP_ACCOUNT_UPDATED = 'Group record updated successfully.';

    private $model_;

    public function __construct(Group $model)
    {
        $this->model_ = $model;
    }

    public function groupsList($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $groups = $this->model_->groupsFilter($request)->where('shop_id', $this->shopId())->with('shop')->paginate(EnumsList::Pagination);
        } else {
            $groups = $this->model_->groupsFilter($request)->paginate(EnumsList::Pagination);
        }
        if ($groups->isNotEmpty()) {
            return GroupsResourceCollection::collection($groups);
        }
        return response()->json(['message' => GroupService::RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    }

    public function createGroup($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $data['shop_id'] = $this->shopId();
        }
        $isGroupCreated = Group::create($data);
        if ($isGroupCreated) {
            addToLog(LogTypeEnum::Info, null, null, $isGroupCreated, LogAction::Created, ModuleEnum::Groups);
            return response()->json(['message' => GroupService::GROUP_CREATED], Response::HTTP_CREATED);
        }
    }

    public function singleGroup($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $group = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $group = $this->model_::find($id);
        }
        if (!empty($group)) {
            return new GroupResource($group);
        }
        return response()->json(['message' => GroupService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_OK);
    }

    public function updateGroup($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $group = $this->model_::where('shop_id', $this->shopId())->where('id', $data['id'])->first();
        } else {
            $group = $this->model_::find($data['id']);
        }
        if ($group) {
            $oldData = $group->getOriginal();
            $isGroupUpdated = $group->update($data);
            if ($isGroupUpdated) {
                addToLog(LogTypeEnum::Info, null, $oldData, $group, LogAction::Updated, ModuleEnum::Groups);
                return response()->json(['message' => GroupService::GROUP_ACCOUNT_UPDATED], Response::HTTP_OK);
            }
        }
        return response()->json(['message' => GroupService::GROUP_RECORD_FAILD]);
    }

    public function deleteGroup($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $group = $this->model_::where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $group = $this->model_::find($id);
        }
        if ($group) {
            addToLog(LogTypeEnum::Info, null, $group, null, LogAction::Deleted, ModuleEnum::Groups);
            $group->delete();
            return response()->json(['message' => GroupService::GROUP_DELETED], Response::HTTP_OK);
        }
        return response()->json(['message' => GroupService::GROUP_RECORD_FAILD]);
    }

    public function toDigestQuery()
    {
        return $this->model_->select(['id', 'group_name']);
    }

    public function todigest()
    {
        if ($this->type() == EnumsList::SHOP) {
            $groups = $this->toDigestQuery()->where('shop_id', $this->shopId())->get();
            return  response()->json([GroupService::COLLECTION_NAME => $groups], Response::HTTP_OK);
        } else {
            $groups = $this->toDigestQuery()->get();
            return  response()->json([GroupService::COLLECTION_NAME => $groups], Response::HTTP_OK);
        }
    }
}
