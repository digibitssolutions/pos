<?php

namespace App\Http\Services;

use App\Http\Controllers\ClientLedgers\Models\ClientLedger;
use App\Http\Controllers\Clients\Models\Client;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct as ModelsInvoiceProduct;
use App\Http\Controllers\Invoices\Models\SaleReport;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\SalesPerson\Models\SalePerson;
use App\Http\Controllers\StockIntake\Models\MasterStock;
use App\Http\Controllers\Transactions\Models\Transaction;

use function App\addToLog;
use function App\shopId;
use function App\transactionNumber;

class InvoiceService extends BaseService
{

    const RECORD_FAILD = 'Record not created.';
    const RECORD_CREATED = 'Record created successfully.';
    const RECORD_NOT_FOUND = 'Invoices record not found.';
    const INVOICE_CREATED = 'Invoice created successfully.';
    const SINGLE_RECORD_NOT_FOUND = 'Invoice record not found.';
    const INVOICE_UPDATED = 'Invoice record updated successfully.';
    const INVOICE_DELETED = 'Invoice record deleted successfully.';
    const SOMETHING_WRONG = 'Data not saved, somethings went wrong.';
    const TRANSACTION_AMOUNT_ERROR = 'Transaction amount either less or zero.';
    const STATUS_UPDATED = 'Invoice status updated successfully.';

    private $invoiceModel_;
    private $saleReportModel_;

    public function __construct(Invoice $model, SaleReport $saleReport)
    {
        $this->invoiceModel_ = $model;
        $this->saleReportModel_ = $saleReport;
    }

    public function invoicesList($request)
    {
        $clients = Client::where('shop_id', shopId())
            ->with('user:id,first_name,last_name')
            ->get();
        $invoices = $this->invoiceModel_->invoicesFilter($request)
            ->with('salePerson.user:id,first_name,last_name')
            ->with('client.user:id,first_name,last_name')
            ->orderByDesc('created_at')->paginate(20);

        return view('admin.invoices.listing', compact('invoices', 'clients'));
    }


    public function invoiceList($request)
    {
        $clients = Client::where('shop_id', shopId())->get();
        $invoices = $this->invoiceModel_->invoicesFilter($request)
            ->WithSalePerson()->where('shop_id', $this->shopId())
            ->orderBy('created_at', 'desc')->get();
        return view('admin.reports.supply-report', compact('invoices', 'clients'));
    }

    public function createInvoice()
    {
        $invoice = $this->invoiceNo();
        $products = DB::table('products')->where('shop_id', $this->shopId())->get(['id', 'product_name', 'sale_price']);
        $customers = Client::where('shop_id', $this->shopId())
            ->with('user:id,first_name,last_name')
            ->get();
        $salePersons = SalePerson::where('shop_id', $this->shopId())
            ->with('user:id,first_name,last_name')
            ->get();
        return view('admin.invoices.add-invoice', compact('products', 'invoice', 'customers', 'salePersons'));
    }

    public function storeInvoice($request)
    {
        $invoiceData = [
            'shop_id' => $this->shopId(),
            'client_id' => $request->client_id,
            'sale_person_id' => $request->sale_person_id,
            'invoice_no' => $this->invoiceNo(),
            'order_no' => $this->orderNo(),
            'invoice_date' => $request->invoice_date,
            'delivery_date' => $request->delivery_date,
            'payment_type' => $request->payment_type,
            'subject' => $request->subject,
            'customer_notes' => $request->customer_notes,
            'terms_conditions' => $request->terms_conditions,
            'total_amount' => $request->total_amount,
            'shipping_charges' => $request->shipping_charges,
            'received_amount' => 0,
            'remaining_amount' => $request->total_amount,
        ];

        DB::beginTransaction();
        $invoice = $this->invoiceModel_::create($invoiceData);
        $invoice['account_name'] = $invoice->client->user->first_name . ' ' . $invoice->client->user->last_name;
        addToLog(LogTypeEnum::Info, null, $invoice, $invoice, LogAction::Created, ModuleEnum::INVOICES);
        $invoiceProducts = [];
        $codes = $request['item_details'];
        $totalSum = 0;
        foreach ($codes as $key => $code) {
            $invoiceProducts[$key] = [
                'invoice_id' => $invoice->id,
                'product_id' => $request->product_id[$key],
                'item_details' => $request->item_details[$key],
                'quantity' => $request->quantity[$key],
                'price' => $request->price[$key],
                'amount' => $request->quantity[$key] * $request->price[$key],
            ];
            $masterStock = MasterStock::where('product_id', $request->product_id[$key])->first();
            if (!($request->quantity[$key] <= $masterStock->master_stock)) {
                return back()->with(['error-message' => 'Low Stock']);
            }
            if ($masterStock && $masterStock->master_stock >= $request->quantity[$key]) {
                $masterStock->master_stock = $masterStock->master_stock - $request->quantity[$key];
                $masterStock->save();
            }
            // SALES REPORT START
            $product = Product::find($request->product_id[$key]);
            $difference = $request->price[$key] - $product->purchase_price;
            $total = $difference * $request->quantity[$key];
            $totalSum = $totalSum + $total;
        }

        $this->saveSalesReport($invoice, $totalSum, $request);
        // SALES REPORT END

        // INVOICE PRODUCTS INSERTION START
        ModelsInvoiceProduct::insert($invoiceProducts);
        // INVOICE PRODUCTS INSERTION END

        // Ledger Start
        $ledger = new ClientLedger();
        $ledger->shop_id = $this->shopId();
        $ledger->client_id = $request->client_id;
        $ledger->invoice_no = $invoice->invoice_no;
        $ledger->invoice_date = $invoice->invoice_date;
        $ledger->payable = $invoice->total_amount;
        $ledger->payment_type = $invoice->payment_type;
        $ledger->status = 'purchased';
        $ledger->description = 'invoice #' . $invoice->invoice_no;
        $ledger->save();
        // Ledger End

        DB::commit();
        DB::rollBack();
        return back()->with(['message' => InvoiceService::INVOICE_CREATED]);
    }

    public function transactionNo($invoiceNo)
    {
        return $transactionNubmer = $this->shopId() . '-' . $invoiceNo . '-' . rand(0, 999999);
    }

    public function saveSalesReport($invoice, $totalSum, $request)
    {
        $sale = new SaleReport();
        $sale->shop_id = $this->shopId();
        $sale->invoice_id = $invoice->id;
        $sale->client_id = $request->client_id;
        if ($totalSum > 0) {
            $sale->profit = $totalSum;
        } elseif ($totalSum < 0) {
            $sale->loss = $totalSum;
        }
        $sale->save();
    }

    public function saveInvoicePDF(int $id)
    {
        $invoice = $this->invoiceModel_::where('shop_id', $this->shopId())->where('id', $id)
            ->first();
        $invoiceItems = $invoice->invoiceProducts()->count();
        $itemPieces = $invoice->invoiceProducts()->sum('quantity');
        $clientsBalance = $this->calculatePrevBalance($invoice->client_id, $invoice->id);
        $data = [
            'customer_name' => $invoice->client->user->first_name . ' ' . $invoice->client->user->last_name,
            'mobile' => $invoice->client->user->userDetail->mobile,
            'city' => $invoice->client->user->userDetail->city,
            'invoice_no' => $invoice['invoice_no'],
            'invoice_date' => $invoice['invoice_date'],
            'total_amount' => $invoice['total_amount'],
            'sales_person' => $invoice->salePerson ? $invoice->salePerson->user->first_name . ' ' . $invoice->salePerson->user->last_name : ' ',
            'previousBalance' => $clientsBalance['previousBalance'],
            'netAmount' => $clientsBalance['netAmount'],
            'invoiceItems' => $invoiceItems,
            'itemPieces' => $itemPieces
        ];
        $products = ModelsInvoiceProduct::where('invoice_id', $invoice->id)->get();
        $pdf = \PDF::loadView('admin.invoice', ['data' => $data, 'products' => $products]);
        return $pdf->download($data['customer_name'] . '.pdf');
    }

    public function clientNetBlance(int $client = null, int $invoiceId = null)
    {
        $clientData = [
            'shop_id' => $this->shopId(),
            'id' => $client
        ];
        $invoiceData = [
            'shop_id' => $this->shopId(),
            'client_id' => $client,
            'id' => $invoiceId
        ];
        $clientsBalance = Client::where($clientData)->select('total_remaining_balance')->first();
        $invoiceAmount = $this->invoiceModel_->where($invoiceData)->select('total_amount')->first();
        $previousBalance = $clientsBalance->total_remaining_balance -= $invoiceAmount->total_amount;
        $netAmount = $previousBalance + $invoiceAmount->total_amount;
        return ['previousBalance' => $previousBalance, 'netAmount' => $netAmount];
    }

    public function changeStatus($request)
    {
        $query = $this->invoiceModel_::where('shop_id', $this->shopId())->where('id', $request->invoice_number);
        if ($request->status == 'Processing') {
            $query->update(['invoice_status' => 'Processing']);
        } elseif ($request->status == 'Paid') {
            $query->update(['invoice_status' => 'Paid']);
        }
        return back()->with(['message' => InvoiceService::STATUS_UPDATED]);
    }

    public function invoiceNo()
    {
        $newInvoiceNumber = null;
        $invoiceNumber    = $this->invoiceModel_->where('shop_id', $this->shopId())->select(['invoice_no'])->latest("id")->withTrashed()->first();
        if (!$invoiceNumber) {
            $newInvoiceNumber = 1;
        } else {
            $newInvoiceNumber = $invoiceNumber->invoice_no;
            $newInvoiceNumber = $newInvoiceNumber + 1;
        }
        return $newInvoiceNumber;
    }

    public function orderNo()
    {
        $newOrderNumber = null;
        $orderNumber    = $this->invoiceModel_::select(['order_no'])->latest("id")->withTrashed()->first();
        if (!$orderNumber) {
            $newOrderNumber = 1;
        } else {
            $newOrderNumber = $orderNumber->order_no;
            $newOrderNumber = $newOrderNumber + 1;
        }
        return $newOrderNumber;
    }

    public function singleInvoice($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $invoice = $this->invoiceModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
            if ($invoice) {
                $invoiceProducts = ModelsInvoiceProduct::where('invoice_id', $invoice->id)->get();
                return view('admin.invoices.show-invoice', compact('invoice', 'invoiceProducts'));
            }
            return back()->with(['error-message' => 'Record not found.']);
        }
    }

    public function editInvoice($id)
    {
        $invoice = $this->invoiceModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
        if (!$invoice) {
            return back()->with(['error-message' => 'Record not found.']);
        }
        $invoiceProducts = ModelsInvoiceProduct::with('product')->where('invoice_id', $invoice->id)->get();
        $products = Product::where('shop_id', $this->shopId())->get();
        $clients = Client::where('shop_id', $this->shopId())->get(['id', 'first_name', 'last_name']);
        $salePersons = SalePerson::where('shop_id', $this->shopId())->get(['id', 'first_name', 'last_name']);
        return view('admin.invoices.update-invoice', compact('invoice', 'invoiceProducts', 'clients', 'products', 'salePersons'));
    }

    public function updateInvoice($request)
    {
        // return $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $invoice = $this->invoiceModel_->with('invoiceProducts')->where('shop_id', $this->shopId())->where('id', $request->id)->first();
        }
        if ($invoice) {
            $invoiceData = [
                'customer' => $request->customer,
                'sale_person_id' => $request->sale_person_id,
                'invoice_date' => $request->invoice_date,
                'delivery_date' => $request->delivery_date,
                'payment_type' => $request->payment_type,
                'subject' => $request->subject,
                'total_amount' => $request->total_amount,
                'remaining_amount' => $request->total_amount,
            ];
            DB::beginTransaction();
            $oldData = $invoice->getOriginal();

            // Client Balance
            $client = Client::where('shop_id', $this->shopId())->where('id', $request->client_id)->first();
            if ($invoice->total_amount < $request->total_amount) {
                $clientInvoice = $request->total_amount - $invoice->total_amount;
                $client->total_remaining_balance += $clientInvoice;
            } else {
                $clientInvoice = $invoice->total_amount - $request->total_amount;
                $client->total_remaining_balance -= $clientInvoice;
            }
            $client->save();

            // Sale report
            if ($invoice->total_amount > $request->total_amount) {
                $difference = $invoice->total_amount - $request->total_amount;
            } else {
                $difference = $request->total_amount - $invoice->total_amount;
            }
            $this->saveSalesReport($invoice, $difference, $request);

            // Invoice
            $invoice->update($invoiceData);

            // Logs
            addToLog(LogTypeEnum::Info, null, $oldData, $invoice, LogAction::Updated, ModuleEnum::Invoices);

            // Invoice Products
            $products = $request['product_id'];
            // $invoiceProduct = [];
            foreach ($products as $key => $product) {

                // return $invoice->invoiceProducts[$key]->quantity;
                // if ($invoiceProduct->quantity < $request->quantity[$key]) {
                // }
                $invoiceProduct =  ModelsInvoiceProduct::where('product_id', $product)->updateOrCreate([
                    'invoice_id' => $invoice->id,
                    'product_id' => $request->product_id[$key],
                    'item_details' => $request->item_details[$key],
                    'quantity' => $request->quantity[$key],
                    'price' => $request->price[$key],
                    'amount' => $request->amount[$key],
                ]);
                // Master Stock
                $masterStock = MasterStock::where('product_id', $request->product_id)->first();
                if (!($request->quantity <= $masterStock->master_stock)) {
                    return back()->with(['error-message' => 'Low Stock']);
                }
                // if (!($invoiceProduct->quantity >= $request->quantity[$key])) {
                if ($invoiceProduct->product_id == $request->product_id) {
                    $masterStock->master_stock = $masterStock->master_stock - $request->quantity;
                    $masterStock->save();
                }
            }


            DB::commit();
            DB::rollBack();
            return back()->with(['message' => 'Record updated successfully.']);
        }
        return back()->with(['error-message' => 'Record not found.']);
    }

    public function deleteInvoice($id)
    {
        $invoice = $this->invoiceModel_->where('shop_id', $this->shopId())->where('id', $id)->first();

        if ($invoice) {
            addToLog(LogTypeEnum::Info, null, $invoice, null, LogAction::Deleted, ModuleEnum::Invoices);
            $invoiceProducts = ModelsInvoiceProduct::where('invoice_id', $invoice->id)->get();

            DB::beginTransaction();
            foreach ($invoiceProducts as $key => $invoiceProduct) {
                $masterStock = MasterStock::where('product_id', $invoiceProduct->product_id)->first();;
                $masterStock['master_stock'] = $masterStock['master_stock'] + $invoiceProduct->quantity;
                $masterStock->save();
            }
            $client = Client::where('shop_id', $this->shopId())->where('id', $invoice->client_id)->first();
            $client->update(['total_remaining_balance' => $client->total_remaining_balance - $invoice->total_amount]);
            $invoice->delete();
            $invoice->invoiceProducts()->delete();
            $invoice->saleReports()->delete();
            DB::commit();
            DB::rollBack();
            return back()->with(['message' => InvoiceService::INVOICE_DELETED]);
        }
        return back()->with(['error-message' => InvoiceService::RECORD_NOT_FOUND]);
    }

    public function invoiceReturn()
    {
        $invoiceNo = $this->invoiceNo();
        $clients = Client::where('shop_id', $this->shopId())
            ->with('user:id,first_name,last_name')
            ->get();
        $salePersons = SalePerson::where('shop_id', shopId())->with('user')->get();
        $products = DB::table('products')->where('shop_id', $this->shopId())->get(['id', 'product_name', 'sale_price']);
        return view('admin.invoices.invoice-return', compact('clients', 'salePersons', 'products', 'invoiceNo'));
    }

    public function storeInvoiceReturn($request)
    {
        $data = $request->all();
        $client = Client::where('shop_id', $this->shopId())->where('id', $data['client_id'])->first();
        $invoiceData = [
            'shop_id' => $this->shopId(),
            'client_id' => $client->id,
            'sale_person_id' => $data['sale_person'] ? $data['sale_person'] : '',
            'invoice_no' => $this->invoiceNo(),
            'order_no' => $this->orderNo(),
            'subject' => $request->subject,
            'total_amount' => $request->total_amount,
            'invoice_status' => 'Paid',
            'invoice_date' => $request->invoice_date
        ];
        $msg = (bool) false;
        DB::beginTransaction();
        $invoice = $this->invoiceModel_::create($invoiceData);
        addToLog(LogTypeEnum::Info, null, null, $invoice, LogAction::Created, ModuleEnum::INVOICES);
        $invoiceProducts = [];
        $codes = $request['item_details'];
        foreach ($codes as $key => $code) {
            $invoiceProducts[$key] = [
                'invoice_id' => $invoice->id,
                'product_id' => $request->product_id[$key],
                'item_details' => $request->item_details[$key],
                'quantity' => $request->quantity[$key],
                'price' => $request->price[$key],
                'amount' => $request->quantity[$key] * $request->price[$key],
            ];
            $masterStock = MasterStock::where('product_id', $request->product_id[$key])->first();
            if ($masterStock) {
                $masterStock->master_stock += $request->quantity[$key];
                $masterStock->save();
            } else {
                $newStock = new MasterStock();
                $newStock->shop_id = $this->shopId();
                $newStock->product_id = $request->product_id[$key];
                $newStock->master_stock = $request->quantity[$key];
                $newStock->save();
            }
        }
        ModelsInvoiceProduct::insert($invoiceProducts);
        // Ledger Start
        $ledger = new ClientLedger();
        $ledger->shop_id = $this->shopId();
        $ledger->client_id = $client->id;
        $ledger->invoice_no = $invoice->id;
        $ledger->paid = $invoice->total_amount;
        $ledger->invoice_date = $invoice->invoice_date;
        $ledger->status = 'paid';
        $ledger->description = 'invoice return #' . $invoice->id;
        $ledger->save();
        // Ledger End
        $invoice->save();
        $msg = true;
        DB::commit();
        DB::rollback();
        if ($msg == true) {
            return back()->with(['message' => InvoiceService::RECORD_CREATED]);
        }
        return back()->with(['error-message' => InvoiceService::RECORD_FAILD]);
    }

    public function singleProduct($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $product = Product::with('masterStock')->where('shop_id', $this->shopId())->where('id', $id)->first();
            return ['product' => $product];
        }
    }

    public function salesList($request)
    {
        $clients = Client::where('shop_id', $this->shopId())
            ->with('user:id,first_name,last_name')
            ->get();
        $salesReport = $this->saleReportModel_->saleReportFilters($request)->where('shop_id', $this->shopId())
            ->orderByDesc('created_at')
            ->get();
        return view('admin.sales.sales-listing', compact('salesReport', 'clients'));
    }

    public function deleteProduct($id)
    {
        $invoiceProduct = ModelsInvoiceProduct::find($id);
        $productStock = MasterStock::where('shop_id', $this->shopId())->where('product_id', $invoiceProduct->product_id)->first();
        $productStock->master_stock = $productStock->master_stock + $invoiceProduct->quantity;
        if ($productStock->save() && $invoiceProduct->delete()) {
            return (['message' => 'Product removed successfully.']);
        }
    }

    public function printInvoice($id)
    {
        $invoice = $this->invoiceModel_::where('shop_id', $this->shopId())->where('id', $id)
            ->first();
        $invoiceItems = $invoice->invoiceProducts()->count();
        $itemPieces = $invoice->invoiceProducts()->sum('quantity');
        $clientsBalance = $this->calculatePrevBalance($invoice->client_id, $invoice->id);

        $data = [
            'customer_name' => $invoice->client->user->first_name . ' ' . $invoice->client->user->last_name,
            'mobile' => $invoice->client->user->userDetail->mobile,
            'city' => $invoice->client->user->userDetail->city,
            'invoice_no' => $invoice['invoice_no'],
            'invoice_date' => $invoice['invoice_date'],
            'total_amount' => $invoice['total_amount'],
            'sales_person' => $invoice->salePerson ? $invoice->salePerson->user->first_name . ' ' . $invoice->salePerson->user->last_name : ' ',
            'previousBalance' => $clientsBalance['previousBalance'],
            'netAmount' => $clientsBalance['netAmount'],
            'invoiceItems' => $invoiceItems,
            'itemPieces' => $itemPieces
        ];
        $products = ModelsInvoiceProduct::where('invoice_id', $invoice->id)->get();
        return view('admin.invoice', compact('data', 'products'))->render();
    }

    public function calculatePrevBalance(int $id, int $inovoiceId)
    {
        $ledger = ClientLedger::where('shop_id', shopId())->where('client_id', $id)->get();
        $invoice = Invoice::where('shop_id', shopId())->where('id', $inovoiceId)
            ->select(['id', 'client_id', 'total_amount'])
            ->first();
        if ($ledger && $invoice) {
            $total = $ledger->sum('payable');
            $received = $ledger->sum('paid');
            $payable = $total - $received;
            $prevBalance = $payable - $invoice->total_amount;
            $netAmount = $prevBalance + $invoice->total_amount;
            return ['previousBalance' => $prevBalance, 'netAmount' => $netAmount];
        }
        return back()->with(['message' => 'Record not found!']);
    }

    public function getLastProductPrice($cId, $pId)
    {
        $product = ModelsInvoiceProduct::where('product_id', $pId)
            ->whereHas('invoice.client', function ($query) use ($cId) {
                return $query->where('id', $cId);
            })
            ->orderByDesc('created_at')
            ->select(['price'])
            ->first();

        if ($product)
            return (['data' => $product->price]);
        else
            return response(['data' => '--']);
    }
}
