<?php
namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\History\HistoryResourceCollection;
use App\Http\Controllers\LoginHistory\Models\LoginHistory;

class LoginLogoutHistoryService extends BaseService {

        const RECORD_NOT_FOUND = "History record not found.";
        const RECORD_FAILD = 'History not deleted.';
        const HISTORY_DELETED = 'History record deleted successfully.';

        private $model_;

        public function __construct(LoginHistory $loginHistory)
        {
                $this->model_ = $loginHistory;
        }

        public function saveLoginHistory($data) {
                $history = new LoginHistory();
                $history->shop_id = $data->type == 'Admin' ? null : $this->shopId();
                $history->type = $data->type;
                $history->user_id = $data->id;
                $history->status = 'Logged-In';
                $history->ip = request()->ip();
                $history->login_time = now();
                $history->save();
        }

        public function saveLogoutHistory($data) {
                $history = new LoginHistory();
                $history->shop_id = $data->type == 'Admin' ? null : $this->shopId();
                $history->type = $data->type;
                $history->user_id = $data->id;
                $history->status = 'Logged-Out';
                $history->ip = request()->ip();
                $history->logout_time = now();
                $history->save();
        }

        public function historyList($request){
                $histories = $this->model_->paginate(EnumsList::Pagination);
                if($histories->isNotEmpty()){
                        return HistoryResourceCollection::collection($histories);
                }
                return response()->json(['message' => LoginLogoutHistoryService::RECORD_NOT_FOUND],Response::HTTP_NOT_FOUND);
        }

        public function deleteAllHistory(){
                $history = LoginHistory::truncate();
                if($history){
                        return response()->json(['message' => LoginLogoutHistoryService::HISTORY_DELETED],Response::HTTP_OK);
                }
                return response()->json(['message' => LoginLogoutHistoryService::RECORD_FAILD]);
        }

        public function deleteSelected($request){
                $history = $this->model_->loginHistoryFilter($request)->delete();
                if($history){
                        return response()->json(['message' => LoginLogoutHistoryService::HISTORY_DELETED],Response::HTTP_OK);
                }
                return response()->json(['message' => LoginLogoutHistoryService::RECORD_FAILD]);
        }

}    