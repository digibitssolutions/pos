<?php

namespace App\Http\Services;

use Exception;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\MediaDrive\Models\MediaDrive;

use function App\shopId;

class MediaDriveService extends BaseService
{
    const MEDIA_ADDED = 'File added successfully!';

    private $drivePathOriginal = "storage/uploads/drive/";

    public function __construct(MediaDrive $model)
    {
        $this->model_ = $model;
    }

    public function storeFile($request)
    {
        if (!$request->file)
            throw new Exception('File not found!');

        if (!$request->has('relation_id'))
            throw new Exception('Relation not found!');

        $originalFileName = $request->file->getClientOriginalName();
        $extension = strtolower($request->file->getClientOriginalExtension());
        $filename = $request->file->hashName();
        $upload_success = $request->file->move(public_path($this->drivePathOriginal), $filename);
        if ($upload_success) {
            $billNo = $request->bill_no;
            $relationId = $request->relation_id;
            $this->createMedia($billNo, $filename, $originalFileName, $extension, $relationId, $request->module_id);
            return ['message' => self::MEDIA_ADDED];
        }
    }

    public function createMedia($billNo, $filename, $originalFileName, $extension, $relationId, $moduleId)
    {
        $file = new $this->model_;
        $file->bill_no = $billNo;
        $file->shop_id = shopId();
        $file->module_id = $moduleId;
        $file->file_name = $filename;
        $file->media_title = $originalFileName;
        $file->extension = $extension;
        $file->media_type = File::mimeType($this->drivePathOriginal . $filename);
        $file->relation_id = $relationId;
        $file->save();
    }

    public function fetchMediaByEntity($moduleId, $relationId, $mediaId)
    {
        $record = $this->model_->where('shop_id', 1)
            ->where('module_id', $moduleId)
            ->where('relation_id', $relationId)->find($mediaId);
        if ($record) {
            $record->url = URL::to($this->drivePathOriginal . $record['file_name']);
            return $record;
        }
        return ['message' => 'record not found'];
    }

    public function fetchMediaByEntities($moduleId, $relationId)
    {
        $record = $this->model_->where('shop_id', 1)
            ->where('module_id', $moduleId)
            ->where('relation_id', $relationId)->get();
        if ($record) {
            $medias = [];
            foreach ($record as $key => $value) {
                $medias[$key] = URL::to($this->drivePathOriginal . $value['file_name']);
            }
            return $medias;
        }
    }

    public function deleteMediaEntity($moduleId, $relationId, $mediaId)
    {
        $record = $this->model_->where('shop_id', 1)
            ->where('module_id', $moduleId)
            ->where('relation_id', $relationId)->find($mediaId);
        if ($record) {
            $fileExists = file_exists($this->drivePathOriginal . $record->file_name);
            if ($fileExists)
                unlink($this->drivePathOriginal . $record->file_name);
            $record->delete();
            return ['message' => 'Record deleted!'];
        }
        return ['message' => 'Record not found!'];
    }
}
