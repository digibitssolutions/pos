<?php

namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\Parties\Models\Party;
use App\Http\Controllers\UserDetails\Models\UserDetail;

use function App\addToLog;
use function App\shopId;

class PartyService extends BaseService
{

    const ACCOUNT_CREATED = 'Party account created successfully.';
    const RECORD_FAILD = 'Something went wrong.';
    const ACCOUNT_UPDATED = 'Account updated successfully.';
    const UPDATE_FAILED = 'Failed to update record.';

    private $partyModel_;
    private $userDetailsModel_;

    public function __construct(Party $partyModel, UserDetail $userDetailsModel)
    {
        $this->partyModel_ = $partyModel;
        $this->userDetailsModel_ = $userDetailsModel;
    }

    public function partiesList($request)
    {
        $parties = DB::table('parties as p')->join('users as u', 'u.id', 'p.user_id')
            ->rightJoin('users_details as ud', 'ud.user_id', '=', 'u.id')
            ->select(['p.id', 'p.party_name', 'ud.city', 'ud.mobile'])
            ->where('u.type', 'Party')
            ->where('p.shop_id', $this->shopId())
            ->orderBy('p.created_at', 'desc')
            ->when(!empty($request->party_name), function ($query) use ($request) {
                return $query->where('p.party_name', 'like', '%' . $request->party_name . '%');
            })
            ->when(!empty($request->mobile), function ($query) use ($request) {
                return $query->where('ud.mobile', $request->mobile);
            })
            ->when(!empty($request->email), function ($query) use ($request) {
                return $query->where('u.email', $request->email);
            })
            ->when(!empty($request->city), function ($query) use ($request) {
                return $query->where('ud.city', $request->city);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('p.created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('p.created_at', '<=', $request->to);
            })
            ->get();
        return view('admin.parties.parties-listing', compact('parties'));
    }

    // public function createPartyAccount()
    // {
    //     return view('admin.parties.add-party');
    // }

    public function storePartyAccount($request)
    {
        $data = $request->all();
        DB::beginTransaction();
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->middle_name = $data['middle_name'];
        $user->email = $data['email'];
        $user->type = 'Party';
        $user->password = Hash::make($data['password']);
        $user->decrypted_password = $data['password'];
        $user->status = 'Active';
        $user->save();

        $party = new Party();
        $party->user_id = $user->id;
        $party->shop_id = $this->shopId();
        $party->party_name = $user->first_name . ' ' . $user->last_name;
        $party->save();

        $userDetail = $this->userDetailsModel_->fill($data);
        $userDetail->user_id = $user->id;
        $userDetail->created_by = $this->userId();
        $account = $userDetail->save();
        DB::commit();
        DB::rollBack();

        if ($account) {
            addToLog(LogTypeEnum::Info, null, null, $user, LogAction::Created, ModuleEnum::Party);
            return response(['message' => PartyService::ACCOUNT_CREATED]);
        }
        return response(['error-message' => PartyService::RECORD_FAILD]);
    }

    public function showPartyAccount($id)
    {
        return $party = DB::table('parties as p')->leftJoin('users as u', 'p.user_id', '=', 'u.id')
            ->rightJoin('users_details as ud', 'ud.user_id', '=', 'u.id')
            ->where('p.shop_id', $this->shopId())
            ->where('p.id', $id)
            ->where('u.type', 'Party')
            ->select(['ud.*', 'p.id', 'p.party_name', 'u.first_name', 'u.last_name', 'u.email', 'u.type'])
            ->first();
    }

    // public function editPartyAccount($id)
    // {

    //     if ($this->type() == EnumsList::SHOP) {
    //         $party = $this->partyModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $party = $this->partyModel_->find($id);
    //     }
    //     return view('admin.parties.edit-party', compact('party'));
    // }

    public function updatePartyAccount($request)
    {
        $data = $request->all();

        $party = $this->partyModel_->where('shop_id', shopId())->where('id', $request->id)->first();
        $party->update([
            'party_name' => $data['party_name']
        ]);

        $user = $party->user;
        $user->fill($data);
        $oldData = $user->getOriginal();
        $udpatedUser = $user->save();

        $userDetail = $this->userDetailsModel_->where('user_id', $user->id)->first();
        $userDetail->fill($data);
        $updatedDetail = $userDetail->save();

        if ($party && $udpatedUser && $updatedDetail) {
            addToLog(LogTypeEnum::Info, null, $oldData, $user, LogAction::Updated, ModuleEnum::Party);
            return response(['message' => PartyService::ACCOUNT_UPDATED]);
        }
        return response(['error-message' => PartyService::UPDATE_FAILED]);
    }

    // public function deleteClientAccount($id)
    // {
    //     if ($this->type() == EnumsList::SHOP) {
    //         $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $client = $this->clientModel_->find($id);
    //     }
    //     if ($client) {
    //         $client->delete();
    //         return $this->deleted(['message' => ClientsService::ACCOUNT_DELETED], Response::HTTP_OK);
    //     }
    //     return $this->noRecord(['message' => ClientsService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    // }
}
