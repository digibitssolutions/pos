<?php

namespace App\Http\Services;

use App\Traits\FilesUploadTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Groups\Models\Group;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\Categories\Models\Category;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\Generics\Models\MeasurementUnit;

use function App\addToLog;
use function PHPUnit\Framework\fileExists;

class ProductService extends BaseService
{

    use FilesUploadTrait;

    const COLLECTION_NAME = 'products';
    const RECORD_FAILD = 'Product not created.';
    const PRODUCT_IMAGES_FOLDER = 'ProductsImages';
    const RECORD_NOT_FOUND = 'Products record not found.';
    const PRODUCT_CREATED = 'Product created successfully.';
    const PRODUCT_DELETED = 'Product deleted successfully.';
    const PRODUCT_UPDATED = 'Product updated successfully.';
    const SINGLE_RECORD_NOT_FOUND = 'Product record not found.';

    private $productModel_;

    public function __construct(Product $model)
    {
        $this->productModel_ = $model;
    }

    public function productsList($request)
    {
        $products = DB::table('products as p')
        ->leftJoin('master_stock as ms', 'p.id', '=', 'ms.product_id')
        ->select(['p.id', 'p.product_name', 'p.purchase_price', 'p.sale_price', 'ms.master_stock', 'p.code', 'p.varient', 'p.expiry_date'])
            ->where('p.shop_id', $this->shopId())
            ->when(!empty($request->product_name), function ($query) use ($request) {
                return $query->where('product_name', 'like', '%' . $request->product_name . '%');
            })
            ->when(!empty($request->code), function ($query) use ($request) {
                return $query->where('code', $request->code);
            })
            ->when(!empty($request->unit_price), function ($query) use ($request) {
                return $query->where('unit_price', $request->unit_price);
            })
            ->when(!empty($request->category_id), function ($query) use ($request) {
                return $query->where('category_id', $request->category_id);
            })
            ->when(!empty($request->measurement_unit), function ($query) use ($request) {
                return $query->where('measurement_unit', $request->measurement_unit);
            })
            ->when(!empty($request->from_expiry_date), function ($query) use ($request) {
                return $query->whereDate('expiry_date', '>=', $request->from_expiry_date);
            })
            ->when(!empty($request->to_expiry_date), function ($query) use ($request) {
                return $query->whereDate('expiry_date', '<=', $request->to_expiry_date);
            })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            })
            ->orderByDesc('p.created_at')
            ->get();

        $measurementUnits = MeasurementUnit::where('shop_id', $this->shopId())->get(['id', 'name']);
        $categories = Category::where('shop_id', $this->shopId())->get(['id', 'category_name']);
        return view('admin.products.products-listing', compact('products', 'measurementUnits', 'categories'));
    }

    public function fetchPagination($request)
    {
        if ($request->ajax()) {
            $products = Product::paginate(10);
            return view('admin.products.paginate', compact('products'))->render();
        }
    }

    public function createProduct()
    {
        $groups = Group::where('shop_id', $this->shopId())->get(['id', 'group_name']);
        $categories = Category::where('shop_id', $this->shopId())->get(['id', 'category_name']);
        $measurementUnits = MeasurementUnit::where('shop_id', $this->shopId())->get(['id', 'name']);
        return view('admin.products.add-product', compact('groups', 'categories', 'measurementUnits'));
    }

    public function storeProduct($request)
    {
        $data = $request->all();
        $data['shop_id'] = $this->shopId();
        // $data['image'] = $this->mediaStore($request, ProductService::PRODUCT_IMAGES_FOLDER);
        $isProductCreated = $this->productModel_->create($data);
        if ($isProductCreated) {
            addToLog(LogTypeEnum::Info, null, null, $isProductCreated, LogAction::Created, ModuleEnum::Products);
            return back()->with(['message' => ProductService::PRODUCT_CREATED]);
        }
        return back()->with(['message' => ProductService::PRODUCT_CREATED]);
    }

    public function singleProduct($id)
    {
        $product = $this->productModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
        if ($product) {
            return view('admin.products.show-product', compact('product'));
        }
        return back()->with(['error-message' => 'Record not found.']);
    }

    public function editProduct($id)
    {
        $product = $this->productModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
        $groups = Group::get(['id', 'group_name']);
        $categories = Category::get(['id', 'category_name']);
        $measurementUnits = MeasurementUnit::get(['id', 'name']);
        return response([
            'product' => $product,
            'groups' => $groups,
            'categories' => $categories,
            'measurementUnits' => $measurementUnits
        ]);
        // return view('admin.products.edit-product', compact('product', 'groups', 'categories', 'measurementUnits'));
    }

    public function updateProduct($request)
    {
        $data = $request->all();
        $product = $this->productModel_::where('shop_id', $this->shopId())->where('id', $request->id)->first();
        $old = $product->getOriginal();
        if ($product && $product->update($data)) {
            addToLog(LogTypeEnum::Info, null, $old, $product, LogAction::Updated, ModuleEnum::Products);
            return back()->with(['message' => ProductService::PRODUCT_UPDATED]);
        }
        return back()->with(['error-message' => ProductService::RECORD_FAILD]);
    }

    public function deleteProduct($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $product = $this->productModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
        }
        if ($product && $product->delete()) {
            return back()->with(['message' => ProductService::PRODUCT_DELETED]);
        }
        return back()->with(['message' => ProductService::SINGLE_RECORD_NOT_FOUND]);
    }

    public function toDigestQuery()
    {
        return $this->productModel_->select(['id', 'product_name']);
    }

    public function todigest()
    {
        if ($this->type() == EnumsList::SHOP) {
            $products = $this->toDigestQuery()->where('shop_id', $this->shopId())->get();
            return  response()->json([ProductService::COLLECTION_NAME => $products], Response::HTTP_OK);
        } else {
            $products = $this->toDigestQuery()->get();
            return  response()->json([ProductService::COLLECTION_NAME => $products], Response::HTTP_OK);
        }
    }

    public function deleteImage($product)
    {
        $productImage = public_path(ProductService::PRODUCT_IMAGES_FOLDER . "/" . $product->image);
        if (fileExists($productImage)) {
            unlink($productImage);
        }
    }

    public function getCurrentProductName($code)
    {
        $data = $this->productModel_::where('shop_id', $this->shopId())->where('code', $code)->select(['id', 'product_name'])->first();
        if ($data) {
            return $data;
        } else {
            return $this->noRecord(['message' => ProductService::RECORD_FAILD], Response::HTTP_NOT_FOUND);
        }
    }

    // Ajax Called Here
    public function updateProductById($id, $price, $salePrice)
    {
        $product = $this->productModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
        $pPrice = $product->purchase_price;
        $sPrice = $product->sale_price;

        if ($pPrice > $price || $pPrice < $price || $sPrice > $salePrice || $sPrice < $salePrice) {
            $product->update([
                'unit_price' => isset($price) ? $price : $product->unit_price,
                'purchase_price' => isset($price) ? $price : $product->purchase_price,
                'sale_price' => isset($salePrice) ? $salePrice : $product->sale_price
            ]);
            return response(['message' => ProductService::PRODUCT_UPDATED]);
        } else {
            return response(['message' => ''], 422);
        }
        return response(['message' => ProductService::RECORD_NOT_FOUND]);
    }
}
