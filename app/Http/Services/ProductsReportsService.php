<?php

namespace App\Http\Services;

use App\Http\Builders\ProductsAuditQueryBuilder;
use App\Http\Builders\ProductsReportsQueryBuilder;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Products\Models\Product;

use function App\shopId;

class ProductsReportsService extends BaseService
{

    private $productModel_;
    private $reportsService_;

    public function __construct(
        Product $productModel,
        ReportService $reportsService
    ) {
        $this->productModel_ = $productModel;
        $this->reportsService_ = $reportsService;
    }

    public function productsReport($request)
    {
        $downloadFileName = 'products-report';
        $dailySalesReportQueryBuilder = new ProductsReportsQueryBuilder(new Product(), $request);
        $reportQuery = $dailySalesReportQueryBuilder->build();
        $columns = [
            'shop_id',
            'product_name',
            'sale_price',
            'expiry_date',
            'created_at',
        ];
        $title = 'Products Report';
        $meta = [];
        $type = $request->get('type', 'json');
        return $this->reportsService_->generateResponse($type, $reportQuery, $title, $meta, $columns, $downloadFileName);
    }


    public function productsAudit()
    {
        if ($this->type() == EnumsList::SHOP) {
            $products = $this->productModel_::where('shop_id', shopId())->with('masterStock')->get();
            return view('admin.reports.products-audit-report', compact('products'));
        }
    }

    public function productsAuditReportGenerate($request)
    {

        $downloadFileName = 'products-audit-report';
        $ProductsAuditQueryBuilder = new ProductsAuditQueryBuilder(new Product(), $request);
        $reportQuery = $ProductsAuditQueryBuilder->build();
        $columns = [
            'product_name',
            'quantity',
            'purchase_price',
            'amount',
        ];
        $title = 'Products Audit Report';
        $meta = [];
        $type = $request->get('type', 'json');
        return $this->reportsService_->generateResponse($type, $reportQuery, $title, $meta, $columns, $downloadFileName);
    }
}
