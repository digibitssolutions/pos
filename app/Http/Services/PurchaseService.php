<?php

namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Parties\Models\Party;
use App\Http\Controllers\Purchases\Models\Purchase;
use App\Http\Controllers\Transactions\Models\PurchaseTransaction;

use function App\transactionNumber;

class PurchaseService extends BaseService
{

    const PURCHASE_CREATED = 'Purchase created successfully.';
    const RECORD_FAILD = 'Something went wrong.';
    const PURCHASE_UPDATED = 'Purchase updated successfully.';
    const UPDATE_FAILED = 'Failed to update record.';
    const TRANSACTION_AMOUNT_ERROR = 'Transaction amount either less or zero.';

    private $purchaseModel_;

    public function __construct(Purchase $purchaseModel)
    {
        $this->purchaseModel_ = $purchaseModel;
    }

    public function purchasesList($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $parties = Party::where('shop_id', $this->shopId())->get(['id', 'party_name']);
            $purchases = $this->purchaseModel_->purchasesFilter($request)->where('shop_id', $this->shopId())->paginate(EnumsList::Pagination);
        } else {
            $purchases = $this->purchaseModel_->purchasesFilter($request)->paginate(EnumsList::Pagination);
        }
        return view('admin.purchases.purchase-listing', compact('parties', 'purchases'));
    }

    public function createPurchase()
    {
        $parties = Party::where('shop_id', $this->shopId())->get(['id', 'party_name']);
        return view('admin.purchases.add-purchase', compact('parties'));
    }

    public function storePurchase($request)
    {
        $data = $request->all();
        $purchase = $this->purchaseModel_->create($data);
        if ($purchase) {
            return back()->with(['message' => PurchaseService::PURCHASE_CREATED]);
        }
        return back()->with(['error-message' => PurchaseService::RECORD_FAILD]);
    }

    public function showPruchase($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $purchase = $this->purchaseModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $purchase = $this->purchaseModel_->find($id);
        }
        return view('admin.purchases.show-purchase', compact('purchase'));
    }

    public function editPurchase($id)
    {

        if ($this->type() == EnumsList::SHOP) {
            $parties = Party::where('shop_id', $this->shopId())->get(['id', 'party_name']);
            $purchase = $this->purchaseModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $purchase = $this->purchaseModel_->find($id);
        }
        return view('admin.purchases.edit-purchase', compact('purchase', 'parties'));
    }

    public function updatePurchase($request, $id)
    {
        $data = $request->all();
        $party = $this->purchaseModel_->find($id)->update($data);
        if ($party) {
            return back()->with(['message' => PurchaseService::PURCHASE_UPDATED]);
        }
        return back()->with(['error-message' => PurchaseService::UPDATE_FAILED]);
    }

    public function purchaseTransaction($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $purchase = $this->purchaseModel_::where('shop_id', $this->shopId())->where('id', $id)->first();
        }
        return view('admin.purchases.add-purchase-transaction', compact('purchase'));
    }


    public function storePurchaseTransaction($request)
    {
        $data = $request->all();

        $purchase = Purchase::where('shop_id', $this->shopId())->where('id', $request->purchase_id)->first();
        if ($data['transaction_amount'] <= $purchase->net_amount) {
            $purchase->net_amount = $purchase->net_amount - $data['transaction_amount'];
        } else {
            return back()->with(['error-message' => PurchaseService::TRANSACTION_AMOUNT_ERROR]);
        }

        // Purchase Transaction Start
        $purchaseTransaction = new PurchaseTransaction();
        $purchaseTransaction->shop_id = $this->shopId();
        $purchaseTransaction->party_id = $data['party_id'];
        $purchaseTransaction->purchase_id = $data['purchase_id'];
        $purchaseTransaction->transaction_type_id = $data['transaction_type_id'];
        $purchaseTransaction->transaction_id = transactionNumber($data['invoice_no']);
        if ($purchase->payment_type == 'cash') {
            $purchaseTransaction->credit = $data['transaction_amount'];
        } else {
            $purchaseTransaction->debit = $data['transaction_amount'];
        }
        $purchaseTransaction->remaining_credit = 0;
        $purchaseTransaction->note = $data['note'];
        $purchaseTransaction->payment_via = $data['payment_via'];
        $purchaseTransaction->payment_via_mobile = $data['payment_via_mobile'];
        $purchaseTransaction->payment_via_cnic = $data['payment_via_cnic'];
        $isTransactionSaved = $purchaseTransaction->save();
        $purchase->save();
        // Purchase Transaction End

        if ($isTransactionSaved) {
            return back()->with(['message' => PurchaseService::PURCHASE_CREATED]);
        }
        return back()->with(['error-message' => PurchaseService::RECORD_FAILD]);
    }

    // public function deleteClientAccount($id)
    // {
    //     if ($this->type() == EnumsList::SHOP) {
    //         $client = $this->clientModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
    //     } else {
    //         $client = $this->clientModel_->find($id);
    //     }
    //     if ($client) {
    //         $client->delete();
    //         return $this->deleted(['message' => ClientsService::ACCOUNT_DELETED], Response::HTTP_OK);
    //     }
    //     return $this->noRecord(['message' => ClientsService::SINGLE_RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    // }

}
