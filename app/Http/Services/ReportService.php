<?php

namespace App\Http\Services;

use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\Categories\Models\Category;
use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Generics\Models\MeasurementUnit;
use Illuminate\Support\Facades\DB;
use Jimmyjs\ReportGenerator\Facades\PdfReportFacade as PdfReport;
use Jimmyjs\ReportGenerator\Facades\ExcelReportFacade as ExcelReport;
use Jimmyjs\ReportGenerator\Facades\CSVReportFacade as CSVReport;

class ReportService extends BaseService
{

    const RECORD_FAILD = 'User account not created.';
    const ADMIN = 'User account created successfully.';
    const RECORD_NOT_FOUND = 'Users account not found.';
    const USER_DELETED = 'User account deleted successfully.';
    const USER_ACCOUNT_UPDATED = 'User account updated successfully.';
    const USER_ACCOUNT_CREATED = 'User account created successfully.';

    private $productModel_;
    private $clientModel_;

    public function __construct(Product $productModel, Client $clientModel)
    {
        $this->productModel_ = $productModel;
        $this->clientModel_ = $clientModel;
    }

    public function productsReport($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $products = DB::table('products as p')->where('shop_id', $this->shopId())->get();
        }
        $measurementUnits = MeasurementUnit::where('shop_id', $this->shopId())->get(['id', 'name']);
        $categories = Category::where('shop_id', $this->shopId())->get(['id', 'category_name']);
        return view('admin.reports.products-reports', compact('products', 'measurementUnits', 'categories'));
    }

    public function clientsBalanceReport($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $clients = $this->clientModel_->clientsFilter($request)
                ->withUser()
                ->where('shop_id', $this->shopId())->get();
        }
        return view('admin.reports.clients-balance-reports', compact('clients'));
    }

    public function generateResponse($type, $queryBuilder, $title, $meta, $columns, $downloadFileName, $options = [])
    {
        ob_end_clean();
        ob_start();
        switch ($this->getReportExportType($type)) {
            case EnumsList::Json:
                return $queryBuilder->get()->toJson();
                break;
            case EnumsList::Pdf:
                $report = PdfReport::of($title, $meta, $queryBuilder, $columns);
                break;
            case EnumsList::Excel:
                $report = ExcelReport::of($title, $meta, $queryBuilder, $columns);
                break;
            case EnumsList::Csv:
                $report = CSVReport::of($title, $meta, $queryBuilder, $columns);
                break;
        }

        $this->addReportoptions($type, $report, $options);
        if (isset($options['do_not_send_stream']) && $options['do_not_send_stream'] === true) {
            return $report;
        }
        return $report->download($downloadFileName);
    }

    private function getReportExportType(string $type)
    {
        $reportExportType = EnumsList::Json;
        switch ($type) {
            case "pdf":
                $reportExportType = EnumsList::Pdf;
                break;
            case "excel":
                $reportExportType = EnumsList::Excel;
                break;
            case "csv":
                $reportExportType = EnumsList::Csv;
                break;
        }
        return $reportExportType;
    }

    private function addReportoptions($type, &$report, $options)
    {
        if (isset($options['orientation'])) {
            $report = $report->setOrientation($options['orientation']);
        }

        if (isset($options['editColumn'])) {
            $report = $report->editColumns($options['editColumn']['columns'], $options['editColumn']['edit']);
        }

        if (isset($options['css'])) {
            $report = $report->setCss($options['css']);
        }
    }
}
