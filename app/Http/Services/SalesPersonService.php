<?php

namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use App\Http\Controllers\SalesPerson\Models\SalePerson;
use App\Http\Controllers\UserDetails\Models\UserDetail;

use function App\addToLog;

class SalesPersonService extends BaseService
{

    const ACCOUNT_CREATED = 'Sales Person account created successfully.';
    const ACCOUNT_DELETED = 'Sales Person account deleted successfully.';
    const RECORD_FAILD = 'Something went wrong.';
    const SINGLE_RECORD_NOT_FOUND = 'Record not found.';
    const SP_ACCOUNT_UPDATED = 'Sale Person updated successfully.';
    const SP_UPDATE_FAILED = 'Failed to update Sale Person.';

    private $salesPersonModel_;
    private $userDetailsModel_;

    public function __construct(SalePerson $salesPersonModel, UserDetail $userDetailsModel)
    {
        $this->salesPersonModel_ = $salesPersonModel;
        $this->userDetailsModel_ = $userDetailsModel;
    }

    public function salesPersonList($request)
    {
        $salesPersons = DB::table('sale_persons as sp')->join('users as u', 'u.id', 'sp.user_id')
            ->rightJoin('users_details as ud', 'ud.user_id', '=', 'u.id')
            ->select(['sp.id', 'u.first_name', 'u.last_name', 'ud.phone', 'ud.city'])
            ->where('u.type', 'SalePerson')
            ->where('sp.shop_id', $this->shopId())
            ->orderBy('sp.created_at', 'desc')
            ->get();
        return view('admin.sales-person.sale-person-listing', compact('salesPersons'));
    }

    public function createSalesPerson($request)
    {
        $data = $request->all();
        DB::beginTransaction();
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->middle_name = $data['middle_name'];
        $user->email = $data['email'];
        $user->type = 'SalePerson';
        $user->password = Hash::make($data['password']);
        $user->decrypted_password = $data['password'];
        $user->status = 'Active';
        $user->save();

        $salePerson = new SalePerson();
        $salePerson->user_id = $user->id;
        $salePerson->shop_id = $this->shopId();
        $salePerson->save();

        $userDetail = $this->userDetailsModel_->fill($data);
        $userDetail->user_id = $user->id;
        $userDetail->created_by = $this->userId();
        $account = $userDetail->save();
        DB::commit();
        DB::rollBack();

        if ($account) {
            addToLog(LogTypeEnum::Info, null, null, $user, LogAction::Created, ModuleEnum::SalePerson);
            return response(['message' => SalesPersonService::ACCOUNT_CREATED]);
        }
        return response(['error-message' => SalesPersonService::RECORD_FAILD]);
    }

    public function singleSalesPersonRecord($id)
    {
        return $salePerson = DB::table('sale_persons as sp')->leftJoin('users as u', 'sp.user_id', '=', 'u.id')
            ->rightJoin('users_details as ud', 'ud.user_id', '=', 'u.id')
            ->where('sp.shop_id', $this->shopId())
            ->where('sp.id', $id)
            ->where('u.type', 'SalePerson')
            ->select(['ud.*', 'sp.id', 'u.first_name', 'u.last_name', 'u.email', 'u.type'])
            ->first();
    }

    public function updateSalesPersonAccount($request)
    {
        $data = $request->all();
        $salesPerson = $this->salesPersonModel_->where('shop_id', $this->shopId())->where('id', $request->id)->first();

        $user = $salesPerson->user;
        $user->fill($data);
        $oldData = $user->getOriginal();
        $udpatedUser = $user->save();

        $userDetail = $this->userDetailsModel_->where('user_id', $user->id)->first();
        $userDetail->fill($data);
        $updatedDetail = $userDetail->save();

        if ($salesPerson && $udpatedUser && $updatedDetail) {
            addToLog(LogTypeEnum::Info, null, $oldData, $user, LogAction::Updated, ModuleEnum::SalePerson);
            return response(['message' => SalesPersonService::SP_ACCOUNT_UPDATED]);
        }
        return response(['error-message' => SalesPersonService::SP_UPDATE_FAILED]);
    }

    public function deleteSalesPersonAccount($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $salesPerson = $this->salesPersonModel_->where('shop_id', $this->shopId())->where('id', $id)->first();
        } else {
            $salesPerson = $this->salesPersonModel_->find($id);
        }
        if ($salesPerson && $salesPerson->delete()) {
            return back()->with(['message' =>  SalesPersonService::ACCOUNT_DELETED]);
        }
        return back()->with(['message' =>  SalesPersonService::SINGLE_RECORD_NOT_FOUND]);
    }
}
