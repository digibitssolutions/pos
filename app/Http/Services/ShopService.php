<?php
namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Shops\Models\Shop;
use Symfony\Component\HttpFoundation\Response;

class ShopService extends BaseService {

    const RECORD_FAILD = 'Shop account not created.';
    const RECORD_NOT_FOUND = 'Shops record not found.';
    const SINGLE_RECORD_NOT_FOUND = 'Shop record not found.';
    const ACCOUNT_CREATED = 'Shop account created successfully.';
    const ACCOUNT_DELETED = 'Shop account deleted successfully.';
    const SHOP_ACCOUNT_UPDATED = 'Shop account updated successfully.';

    private $model_;

    public function __construct(Shop $model) {
        $this->model_ = $model;
    }

    public function shopsList($request) {
        $shops = $this->model_->shopFilter($request)->select(['id', 'shop_code', 'shop_name', 'city', 'user_id', 'mobile','created_at'])
                 ->with('user')->paginate(10);
        if ($shops->isNotEmpty()) {
            return $this->created(['shops_list'=> $shops],Response::HTTP_OK);
        }
        return $this->noRecord(['message'=> ShopService::RECORD_NOT_FOUND, 'status' => 'false'],Response::HTTP_NOT_FOUND);
    }

    public function shopSingleRecord($id) {
        $shop = $this->model_->with('user')->find($id);
        if (!empty($shop)) {
            return response()->json(['shop_data'=> $shop]);
        }
        return $this->created(['message'=> ShopService::SINGLE_RECORD_NOT_FOUND],Response::HTTP_OK);
    }

    public function deleteShopAccount($id){
        $shop = $this->model_->find($id);
        if($shop){
            $shop->delete();
            User::find($shop->user_id)->delete();
            return $this->created(['message'=> ShopService::ACCOUNT_DELETED, 'status' => 'true'],Response::HTTP_OK);
        }else{
            return $this->noRecord(['message'=> ShopService::SINGLE_RECORD_NOT_FOUND, 'status' => 'false'],Response::HTTP_NOT_FOUND);
        }
    }

    public function createShopAccount($request) {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->status = 'Inactive';
        $user->type = 'Shop';
        $isUserAccountCreated = $user->save();
        if($isUserAccountCreated){
            $data = $request->all();
            $data['shop_code'] = rand(100,9999).$user->id.'-'.$user->id.rand(100,9999);
            $data['user_id'] = $user->id;
            $isshopCreated = Shop::create($data);

            if($isshopCreated) {
                return $this->created(['message'=> ShopService::ACCOUNT_CREATED],Response::HTTP_CREATED);
            }
        }
        return $this->noRecord(['message'=> ShopService::RECORD_FAILD]);
    }

    public function updateShopAccount($request) {
        $user = User::find($request->id);
        $user->email = $request->email;
        $user->status = $request->status;
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }
        $isUserAccountUpdated = $user->save();
        if($isUserAccountUpdated){
            $shop = Shop::where('user_id', $user->id)->first();
            $shop->shop_name = $request->shop_name;
            $shop->phone = $request->phone;
            $shop->mobile = $request->mobile;
            $shop->image = $request->image;
            $shop->address = $request->address;
            $shop->city = $request->city;
            $shop->tehsil = $request->tehsil;
            $shop->district = $request->district;
            $shop->fax = $request->fax;
            $shop->postal_code = $request->postal_code;
            $isshopUpdated = $shop->save();
            if($isshopUpdated) {
                return $this->created(['message'=> ShopService::SHOP_ACCOUNT_UPDATED],Response::HTTP_OK);
            }
        }
        return $this->noRecord(['message'=> ShopService::RECORD_FAILD]);
    }
}

?>
