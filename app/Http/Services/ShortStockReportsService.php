<?php

namespace App\Http\Services;

use App\Http\Builders\ShortStockReportsQueryBuilder;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\ShortStock\Models\ShortStock;

class ShortStockReportsService extends BaseService
{

    private $reportsService_;
    private $ThisModel_;

    public function __construct(
        ShortStock $thisModel,
        ReportService $reportsService
    ) {
        $this->ThisModel_ = $thisModel;
        $this->reportsService_ = $reportsService;
    }

    public function shortStockReport($request)
    {
        if ($this->type() == EnumsList::SHOP) {
            $shortStocks = $this->ThisModel_->shortStockFilters($request)->where('shop_id', $this->shopId())->get();
            return view('admin.reports.short-stock-reports', compact('shortStocks'));
        }
    }

    public function shortReportGenerate($request)
    {
        $downloadFileName = 'short-stocks-report';
        $dailySalesReportQueryBuilder = new ShortStockReportsQueryBuilder(new ShortStock(), $request);
        $reportQuery = $dailySalesReportQueryBuilder->build();
        $columns = [
            'product_name',
            'current_stock',
            'required_stock'
        ];
        $title = 'Short Stocks Report';
        $meta = [];
        $type = $request->get('type', 'json');
        return $this->reportsService_->generateResponse($type, $reportQuery, $title, $meta, $columns, $downloadFileName);
    }
}
