<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Enums\LogAction;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Enums\ModuleEnum;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Parties\Models\Party;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\StockIntake\Models\StockIntake;
use App\Http\Controllers\PurchaseLedgers\Models\PurchaseLedger;
use App\Http\Controllers\StockIntake\Models\Stock as ModelsStock;
use App\Http\Controllers\StockIntake\Models\MasterStock as ModelsMasterStock;

use function App\addToLog;
use function App\shopId;

class StockIntakeService extends BaseService
{

    const COLLECTION_NAME = 'stock_intake';
    const RECORD_NOT_FOUND = 'Stock record not found.';
    const RECORD_FAILD = 'StockIntake record not created.';
    const MASTERSTOCK_CREATED = 'MasterStock created successfully';
    const STOCKINTAKE_CREATED = 'StockIntake created successfully.';
    const SINGLE_RECORD_NOT_FOUND = 'StockIntake record not found.';
    const STOCKINTAKE_DELETED = 'StockIntake record deleted successfully.';
    const STOCKINTAKE_UPDATED = 'StockIntake record updated successfully.';

    private $stockIntakeModel_;
    private $stockModel_;
    private $mediaService_;

    public function __construct(StockIntake $model, ModelsStock $stockModel, MediaDriveService $mediaService)
    {
        $this->stockIntakeModel_ = $model;
        $this->stockModel_ = $stockModel;
        $this->mediaService_ = $mediaService;
    }

    public function stockIntakeList($request)
    {
        $parties = Party::where('shop_id', shopId())->get();
        $totalStock = $this->stockModel_->stockFilters($request)->where('shop_id', shopId())
            ->orderByDesc('created_at')->paginate(20);
        return view('admin.stock-intake.list-stock', compact('totalStock', 'parties'));
    }

    public function createStockIntake()
    {
        if ($this->type() == EnumsList::SHOP) {
            $parties = Party::where('shop_id', $this->shopId())->get();
            $products = DB::table('products')->where('shop_id', $this->shopId())->get(['id', 'product_name']);
        }
        return view('admin.stock-intake.add-stock', compact('products', 'parties'));
    }

    public function storeStockIntake($request)
    {
        $codes = $request['product_id'];
        DB::beginTransaction();
        $stockIntake = [];
        $stock = $this->saveStock($request);
        if ($request->file) {
            $request->merge([
                'relation_id' => $stock->id,
                'module_id' => ModuleEnum::STOCK
            ]);
            $this->mediaService_->storeFile($request);
        }
        foreach ($codes as $key => $value) {
            $stockIntake[$key] = [
                'shop_id' => $this->shopId(),
                'party_id' => $request['party_id'],
                'stock_id' => $stock->id,
                'product_id' => $request->product_id[$key],
                'price' => $request->price[$key],
                'stock' =>  $request->stock[$key],
                'created_at' => $this->stockIntakeModel_->freshTimestamp(),
                'updated_at' => $this->stockIntakeModel_->freshTimestamp(),
            ];
            $checkMasterStock = ModelsMasterStock::where('product_id', $request->product_id[$key])->first();
            if ($checkMasterStock) {
                ModelsMasterStock::where('product_id', $request->product_id[$key])->update(['master_stock' => $request->stock[$key] + $checkMasterStock->master_stock]);
            } else {
                $masterStock = new ModelsMasterStock();
                $masterStock->shop_id = $this->shopId();
                $masterStock->product_id = $request->product_id[$key];
                $masterStock->master_stock = $request->stock[$key];
                $masterStock->save();
            }
        }
        $isInserted = $this->stockIntakeModel_::insert($stockIntake);
        addToLog(LogTypeEnum::Info, NULL, $stock, $isInserted, LogAction::Created, ModuleEnum::STOCK);

        // Ledger Start
        $ledger = new PurchaseLedger();
        $ledger->shop_id = $this->shopId();
        $ledger->party_id = $request->party_id;
        $ledger->bill_no = $request->bill_no;
        $ledger->bill_date = $request->bill_date;
        $ledger->payable = $request->payable;
        $ledger->payment_type = $request->payment_type;
        $ledger->description = 'invoice #' . $request->bill_no;
        $ledger->save();
        // Ledger End

        $message = StockIntakeService::STOCKINTAKE_CREATED;
        DB::commit();
        DB::rollback();
        if ($message) {
            return back()->with(['message' => StockIntakeService::STOCKINTAKE_CREATED]);
        }
        return back()->with(['message' => StockIntakeService::RECORD_FAILD]);
    }

    public function singleStockIntake(int $id)
    {
        $stock = ModelsStock::where('shop_id', $this->shopId())->where('id', $id)
            ->with(['intakeStock', 'media'])->first();
        $media = ' ';
        if ($stock->media != null)
            $media = $this->mediaService_->fetchMediaByEntity(ModuleEnum::STOCK, $stock->id, $stock->media['id']);
        return view('admin.stock-intake.show-stock', compact('stock', 'media'));
    }

    public function updateStockIntake($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::SHOP) {
            $stockInake = $this->stockIntakeModel_::where('shop_id', $this->shopId())->where('id', $data['id'])
                ->where('product_id', $data['product_id'])->first();
        } else {
            $stockInake = $this->stockIntakeModel_->where('id', $data['id'])->where('product_id', $data['product_id'])->first();
        }
        if ($stockInake) {
            $masterStock = ModelsMasterStock::where('product_id', $stockInake->product_id)->first();
            if ($stockInake['stock'] > $data['stock']) {
                $quantity = $stockInake->stock - $data['stock'];
                $result = $masterStock->master_stock - $quantity;
            } else {
                $quantity = $data['stock'] - $stockInake->stock;
                $result = $masterStock->master_stock + $quantity;
            }
            $stockInake->update(['stock' => $data['stock']]);
            $masterStock->update(['master_stock' => $result]);
            return $this->created(['message' => StockIntakeService::STOCKINTAKE_UPDATED], Response::HTTP_OK);
        }
        return $this->noRecord(['message' => StockIntakeService::RECORD_FAILD], Response::HTTP_OK);
    }

    // public function deleteStockIntake($id)
    // {
    //     if ($this->type() == EnumsList::SHOP) {
    //         $stockIntake = $this->stockIntakeModel_::where('shop_id', $this->shopId())->where('product_id', $id)->get();
    //     } else {
    //         $stockIntake = $this->stockIntakeModel_->where('product_id', $id)->frist();
    //     }
    //     if ($stockIntake) {
    //         $masterStock = ModelsMasterStock::where('product_id', $id)->first();
    //         foreach ($stockIntake as $key => $value) {
    //             $currentStock[] = $value->stock;
    //             $value->delete();
    //         }
    //         $totalStock = array_sum($currentStock);
    //         $result = $masterStock->master_stock - $totalStock;
    //         $masterStock->update(['master_stock' => $result]);
    //         return back()->with(['message' => StockIntakeService::STOCKINTAKE_DELETED]);
    //     }
    //     return back()->with(['message' => StockIntakeService::RECORD_FAILD]);
    // }

    public function deleteStockIntake($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $stockIntake = $this->stockIntakeModel_::where('shop_id', $this->shopId())->where('product_id', $id)->first();
        } else {
            $stockIntake = $this->stockIntakeModel_->where('product_id', $id)->frist();
        }
        if ($stockIntake) {
            $masterStock = ModelsMasterStock::where('product_id', $id)->first();
            $result = $masterStock->master_stock - $stockIntake->stock;
            $masterStock->update(['master_stock' => $result]);
            $stockIntake->delete();
            return back()->with(['message' => StockIntakeService::STOCKINTAKE_DELETED]);
        }
        return back()->with(['message' => StockIntakeService::RECORD_FAILD]);
    }

    public function singleProduct($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $product = Product::with('masterStock')->where('shop_id', $this->shopId())->where('id', $id)->first();
            return $product;
        };
    }

    private function saveStock($request)
    {
        $data = $request->only(['party_id', 'bill_no', 'bill_date', 'payment_type', 'received_amount', 'payable']);
        $remainingAmount = $data['payable'] - $data['received_amount'];
        $record = array_merge($data, [
            'shop_id' => shopId(),
            'remaining_amount' => $remainingAmount,
            'total_amount' => $data['payable']
        ]);
        unset($record['payable']);
        return $this->stockModel_->create($record);
    }
}
