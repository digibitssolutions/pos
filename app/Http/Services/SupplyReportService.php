<?php

namespace App\Http\Services;

use App\Http\Builders\SupplyReportQueryBuilder;
use App\Http\Controllers\Invoices\Models\Invoice;

class SupplyReportService extends BaseService
{

    private $reportsService_;

    public function __construct(
        ReportService $reportsService

    ) {
        $this->reportsService_ = $reportsService;
    }

    public function supplyReportGenerate($request)
    {
        $downloadFileName = 'supply-report';
        $supplyReportBuilder = new SupplyReportQueryBuilder(new Invoice(), $request);
        $reportQuery = $supplyReportBuilder->build();
        $columns = [
            'customer',
            'invoice_no',
            'invoice_date',
            'amount',
            'prev_balance',
            'received',
        ];
        $title = 'Supply Report';
        $meta = [];
        $type = $request->get('type', 'json');
        return $this->reportsService_->generateResponse($type, $reportQuery, $title, $meta, $columns, $downloadFileName);
    }
}
