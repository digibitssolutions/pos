<?php

namespace App\Http\Services;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Transactions\Models\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionService extends BaseService
{

    const RECORD_FAILD = 'Transaction record not created.';
    const TRANSACTION_CREATED = 'Transaction created successfully.';
    const TRANSACTION_AMOUNT_ERROR = 'Transaction amount either less or zero.';
    const TRANSACTIONS_NOT_FOUND = 'Transactions not found.';

    private $transactionModel_;

    public function __construct(Transaction $model)
    {
        $this->transactionModel_ = $model;
    }

    public function invoiceTransactionListing()
    {
        if ($this->type() == EnumsList::SHOP) {
            $invoices = Invoice::where('shop_id', $this->shopId())->get();
            $transactions = $this->transactionModel_::where('shop_id', $this->shopId())->get();
        }
        return view('admin.transactions.index', compact('transactions', 'invoices'));
    }

    public function showInvoiceTransactions($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $client = $this->transactionModel_->with('client')->where('shop_id', $this->shopId())->where('invoice_id', $id)->first();
            $transactions = $this->transactionModel_->with('invoice')->where('shop_id', $this->shopId())->where('invoice_id', $id)->get();
            if ($transactions->isNotEmpty()) {
                return view('admin.transactions.show-transaction', compact('transactions', 'client'));
            }
            return back()->with(['error-message' => TransactionService::TRANSACTIONS_NOT_FOUND]);
        }
    }

    public function invoiceTransaction($id)
    {
        if ($this->type() == EnumsList::SHOP) {
            $invoice = Invoice::where('shop_id', $this->shopId())->where('id', $id)->first();
        }
        return view('admin.transactions.add-invoice-transaction', compact('invoice'));
    }

    public function storeInvoiceTransaction($request)
    {
        $data = $request->all();

        DB::beginTransaction();
        // INVOICES START
        $invoice = Invoice::where('shop_id', $this->shopId())->where('id', $request->invoice_id)->first();

        if ($data['transaction_amount'] <= $invoice->remaining_amount) {
            $invoice->received_amount = $invoice->received_amount + $data['transaction_amount'];
            $invoice->remaining_amount = $invoice->remaining_amount - $data['transaction_amount'];
        } else {
            return back()->with(['error-message' => TransactionService::TRANSACTION_AMOUNT_ERROR]);
        }
        if ($invoice->remaining_amount == 0 || null) {
            $invoice->invoice_status = 'Paid';
        } else {
            $invoice->invoice_status = 'Processing';
        }

        $invoice->save();
        // INVOICES END
        // CLIENT TOTAL REMAINING BALANCE START
        $client = Client::where('id', $invoice->client_id)->first();
        if ($data['transaction_amount'] <= $client->total_remaining_balance) {
            $client->total_remaining_balance = $client->total_remaining_balance - $request->transaction_amount;
            $client->save();
        } else {
            return back()->with(['error-message' => TransactionService::TRANSACTION_AMOUNT_ERROR]);
        }
        // CLIENT TOTAL REMAINING BALANCE END
        // TRANSACTION START
        $transaction = new Transaction();
        $transaction->shop_id = $this->shopId();
        $transaction->client_id = $data['client_id'];
        $transaction->transaction_type_id = $data['transaction_type_id'];
        $transaction->invoice_id = $data['invoice_id'];
        $transaction->transaction_id = $this->transactionNumber($data['invoice_id']);
        if ($invoice->payment_type == 'cash') {
            $transaction->credit = $data['transaction_amount'];
        } else {
            $transaction->debit = $data['transaction_amount'];
        }
        $transaction->remaining_credit = 0;
        $transaction->note = $data['note'];
        $transaction->payment_via = $data['payment_via'];
        $transaction->payment_via_mobile = $data['payment_via_mobile'];
        $transaction->payment_via_cnic = $data['payment_via_cnic'];
        $isTransactionSaved = $transaction->save();
        DB::commit();
        DB::rollback();
        // TRANSACTION END
        if ($isTransactionSaved) {
            return back()->with(['message' => TransactionService::TRANSACTION_CREATED]);
        }
        return back()->with(['error-message' => TransactionService::RECORD_FAILD]);
    }

    public function transactionNumber($invoiceNo)
    {
        return $transactionNubmer = $this->shopId() . '-' . $invoiceNo . '-' . rand();
    }
}
