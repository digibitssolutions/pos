<?php

namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Enums\EnumsList;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Users\UsersResourceCollection;

use function App\userId;

class UserService extends BaseService
{

    const RECORD_FAILD = 'User account not created.';
    const ADMIN = 'User account created successfully.';
    const RECORD_NOT_FOUND = 'Users account not found.';
    const USER_DELETED = 'User account deleted successfully.';
    const USER_ACCOUNT_UPDATED = 'User account updated successfully.';
    const USER_ACCOUNT_CREATED = 'User account created successfully.';

    private $model_;

    public function __construct(User $model)
    {
        $this->model_ = $model;
    }

    public function usersList($request)
    {
        $users = $this->model_->usersFilter($request)->where('type', EnumsList::ADMIN)->paginate(EnumsList::Pagination);
        if ($users->isNotEmpty()) {
            return view('admin.users.users-list', compact('users'));
            return UsersResourceCollection::collection($users);
        }
        return response()->json(['message' => UserService::RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    }

    public function addUser()
    {
        return view('admin.users.add-user');
        // $users = $this->model_->usersFilter($request)->where('type', EnumsList::ADMIN)->paginate(EnumsList::Pagination);
        // if ($users->isNotEmpty()) {
        //     return view('admin.users.users-list', compact('users'));
        //     return UsersResourceCollection::collection($users);
        // }
        // return response()->json(['message' => UserService::RECORD_NOT_FOUND], Response::HTTP_NOT_FOUND);
    }

    public function storeUser($request)
    {
        $data = $request->all();
        if (!empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }
        $data['type'] = EnumsList::SHOP;
        // $user = $this->model_->create($data);
        if ($this->model_->create($data)) {
            return back()->with(['message' => UserService::USER_ACCOUNT_CREATED]);
        }
        return redirect(route('users-list'))->with(['message' => UserService::RECORD_FAILD]);
    }

    public function editUser()
    {
        $id = userId();
        return $user = $this->model_->where('type', EnumsList::ADMIN)->where('id', $id)->first();
    }

    public function updateUser($request)
    {
        return $data = $request->all();
        if (!empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }
        $user = $this->model_->where('type', EnumsList::ADMIN)->where('id', $data['id'])->update($data);
        if ($user) {
            return response()->json(['message' => UserService::USER_ACCOUNT_UPDATED], Response::HTTP_OK);
        }
        return response()->json(['message' => UserService::RECORD_FAILD]);
    }

    public function deleteUser($id)
    {
        $user = $this->model_->where('type', EnumsList::ADMIN)->where('id', $id)->first();
        if ($user) {
            $user->delete();
            return response()->json(['message' => UserService::USER_DELETED], Response::HTTP_OK);
        }
        return response()->json(['message' => UserService::RECORD_NOT_FOUND]);
    }

    public function addAdmin($request)
    {
        $data = $request->all();
        if ($this->type() == EnumsList::ADMIN) {
            $data['password'] = Hash::make($request->password);
            $data['type'] = 'Admin';
            $data['status'] = 'Active';
            $data['email_verified'] = 1;
            $isAdminCreated = $this->model_->create($data);
            if ($isAdminCreated) {
                return response()->json(['message' => UserService::ADMIN], Response::HTTP_OK);
            }
        }
        return response()->json(['message' => UserService::RECORD_FAILD]);
    }
}
