<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // $this->mapApiRoutes();

        $this->mapWebRoutes();

        // PORTAL

        // History
        $this->mapWebLoginLogoutRoutes();

        // Shops Account
        $this->mapWebShopAccountRoutes();

        // Groups
        $this->mapWebGroupsRoutes();

        // Brands
        $this->mapWebBrandsRoutes();

        // Categories
        $this->mapWebCategoriesRoutes();

        // Products
        $this->mapWebProductsRoutes();

        // Users
        $this->mapWebUsersRoutes();

        // Clients
        $this->mapWebClientsRoutes();

        // Generics
        $this->mapWebGenericsRoutes();

        // StockIntake
        $this->mapWebStockIntakeRoutes();

        // Invoices
        $this->mapWebInvoicesRoutes();

        // Invoices
        $this->mapWebFieldOptionsRoutes();

        // Invoice Transactions
        $this->mapWebInvoiceTransactionsRoutes();

        // Expenses
        $this->mapWebExpensesRoutes();

        // Parties
        $this->mapWebPartiesRoutes();

        // Purchases
        $this->mapWebPurchasesRoutes();

        // Sales Person
        $this->mapWebSalesPersonRoutes();

        // Reports
        $this->mapWebReportsRoutes();

        // Short Stock
        $this->mapWebShortStockRoutes();

        // Purchase Ledger
        $this->mapWebPurchaseLedgerRoutes();

        // Client Ledger
        $this->mapWebClientLedgerRoutes();

        // Sale Person Ledger
        $this->mapWebSalePersonLedgerRoutes();

        // Media Drive
        $this->mapWebMediaDriveRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    // protected function mapApiRoutes()
    // {
    //     Route::prefix('api')
    //         ->middleware('api')
    //         ->namespace($this->namespace)
    //         ->group(base_path('routes/api.php'));
    // }

    protected function mapWebShopAccountRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Shops')
            ->group(base_path('routes/route-files/shops.php'));
    }

    protected function mapWebGroupsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Groups')
            ->group(base_path('routes/route-files/groups.php'));
    }

    protected function mapWebBrandsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Brands')
            ->group(base_path('routes/route-files/brands.php'));
    }

    protected function mapWebCategoriesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Categories')
            ->group(base_path('routes/route-files/categories.php'));
    }

    protected function mapWebProductsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Products')
            ->group(base_path('routes/route-files/products.php'));
    }

    protected function mapWebLoginLogoutRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\LoginHistory')
            ->group(base_path('routes/route-files/login-history.php'));
    }

    protected function mapWebClientsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Clients')
            ->group(base_path('routes/route-files/clients.php'));
    }

    protected function mapWebUsersRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Users')
            ->group(base_path('routes/route-files/users.php'));
    }

    protected function mapWebGenericsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Generics')
            ->group(base_path('routes/route-files/generics.php'));
    }

    protected function mapWebStockIntakeRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\StockIntake')
            ->group(base_path('routes/route-files/stock-intake.php'));
    }

    protected function mapWebInvoicesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Invoices')
            ->group(base_path('routes/route-files/invoices.php'));
    }

    protected function mapWebFieldOptionsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\FieldOptionType')
            ->group(base_path('routes/route-files/field-options.php'));
    }

    protected function mapWebInvoiceTransactionsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Transactions')
            ->group(base_path('routes/route-files/transactions.php'));
    }

    protected function mapWebExpensesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Expenses')
            ->group(base_path('routes/route-files/expenses.php'));
    }

    protected function mapWebPartiesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Parties')
            ->group(base_path('routes/route-files/parties.php'));
    }

    protected function mapWebPurchasesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Purchases')
            ->group(base_path('routes/route-files/purchases.php'));
    }

    protected function mapWebSalesPersonRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\SalesPerson')
            ->group(base_path('routes/route-files/sales-person.php'));
    }

    protected function mapWebReportsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Reports')
            ->group(base_path('routes/route-files/reports.php'));
    }

    protected function mapWebShortStockRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\ShortStock')
            ->group(base_path('routes/route-files/short-stock.php'));
    }

    protected function mapWebPurchaseLedgerRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\PurchaseLedgers')
            ->group(base_path('routes/route-files/purchase-ledger.php'));
    }

    protected function mapWebClientLedgerRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\ClientLedgers')
            ->group(base_path('routes/route-files/client-ledger.php'));
    }

    protected function mapWebSalePersonLedgerRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\SalePersonLedgers')
            ->group(base_path('routes/route-files/sale-person-ledger.php'));
    }

    protected function mapWebMediaDriveRoutes()
    {
        Route::middleware('api')
            ->namespace($this->namespace . '\MediaDrive')
            ->group(base_path('routes/route-files/media-routes.php'));
    }
}
