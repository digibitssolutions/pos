<?php

namespace App\Traits;

trait FilesUploadTrait
{
    public function mediaStore($request, $mediaFolderName){
        $extension = $request->image->extension();
        $filename = time()."_.".$extension;
        $request->image->move(public_path($mediaFolderName),$filename);
        return $filename;
    }
}

?>