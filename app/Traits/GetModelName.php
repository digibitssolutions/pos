<?php

namespace App\Traits;

trait GetModelName
{
    public function getModelName($type) {
        
        $tableName = '';
        switch($type){
            case "measurement_units":
                $tableName = 'measurement_unit';
            break;
            case "usage_units":
                $tableName = 'usage_unit';
            break;
            
            case "vat_codes":
                $tableName = 'vat code';
            break;
        }

        return $tableName;
    }   
}
?>