<?php

namespace App;


use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Parties\Models\Party;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\UserDetails\Models\UserDetail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name','email', 'password', 'decrypted_password', 'type', 'status', 'code', 'email_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function isActive()
    {
        return $this->status == 'Active';
    }

    public function isEmailVerified()
    {
        return $this->email_verified;
    }

    public function shop()
    {
        return $this->hasOne(Shop::class);
    }

    public function clients()
    {
        return $this->hasOne(Client::class, 'user_id');
    }

    public function parties()
    {
        return $this->hasOne(Party::class, 'user_id');
    }

    public function userDetail()
    {
        return $this->hasOne(UserDetail::class);
    }

    public function usersFilter($request)
    {
        return User::when(!empty($request->email), function ($query) use ($request) {
            return $query->where('email', 'like', '%' . $request->email . '%');
        })
            ->when(!empty($request->from), function ($query) use ($request) {
                return  $query->whereDate('created_at', '>=', $request->from);
            })
            ->when(!empty($request->to), function ($query) use ($request) {
                return  $query->whereDate('created_at', '<=', $request->to);
            });
    }
}
