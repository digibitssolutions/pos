<?php

namespace App;

use App\Http\Controllers\Clients\Models\Client;
use App\Http\Controllers\Enums\EnumsList;
use App\Http\Services\BaseService;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Enums\LogTypeEnum;
use App\Http\Controllers\Modules\Models\Module;
use App\Http\Controllers\LogActivities\Models\LogActivity;
use App\Http\Controllers\Parties\Models\Party;
use Illuminate\Support\Facades\{
    Auth
};

function getModuleName($module_id)
{
    $module = Module::find($module_id, ['name']);
    if ($module) {
        return $module->name;
    }
    return '--';
}

function addToLog($type, $exceptionMessage = NULL, $oldData = NULL, $newData = NULL, $action = NULL, $module_id = NULL)
{
    $log = [];
    $log['type'] = $type;
    $log['relation_id'] = isset($oldData) ? $oldData['id'] : null;
    $log['old_data'] = json_encode($oldData);
    $log['new_data'] = json_encode($newData);
    $log['action'] = $action;
    $log['message'] = $type == LogTypeEnum::Error ? $exceptionMessage : auth()->user()->first_name . auth()->user()->last_name . ' ' . $action . ' ' . getModuleName($module_id) . ' Record';
    $log['url'] = Request::fullUrl();
    $log['method'] = Request::method();
    $log['ip'] = Request::ip();
    $log['module_id'] = $module_id;
    $log['created_by_type'] = $type == LogTypeEnum::Error ? 'System' : 'User';
    $log['created_by'] = $type == LogTypeEnum::Error ? '0' : BaseService::userId();
    LogActivity::create($log);
}

function shopId()
{
    $id = null;
    $type =  Auth::user()->type;
    if ($type == 'Admin') {
        $id = null;
    } else {
        $id = Auth::user()->shop->id;
    }
    return $id;
}

function userId()
{
    $id = Auth::user()->shop->id;
    return $id;
}

function transactionNumber($invoiceNo)
{
    return $transactionNubmer = shopId() . '-' . $invoiceNo . '-' . rand();
}

function checkShop()
{
    if (type() == EnumsList::SHOP)
        return 'Shop';
    else
        return NULL;
}

function type()
{
    return Auth::user()->type;
}

function loggedUserName()
{
    return auth()->user()->first_name .' '. auth()->user()->last_name;
}

function clientName(int $client_id)
{
    $client = Client::where('shop_id', shopId())->find($client_id);
    return $client ? $client->user->first_name. ' '. $client->user->last_name : null;
}

function partyName(int $partyId)
{
    $party = Party::where('shop_id', shopId())->find($partyId);
    return $party ? $party->user->first_name. ' '. $party->user->last_name : null;
}
