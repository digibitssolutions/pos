<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 40);
            $table->string('middle_name', 40)->nullable();
            $table->string('last_name', 40);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('decrypted_password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->enum('type', ['Admin', 'Shop', 'Party', 'SalePerson'])->default('Shop');
            $table->smallInteger('email_verified')->default(false);
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
            $table->string('code')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
