<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoginHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_history', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->string('type');
            $table->integer('user_id');
            $table->ipAddress('ip')->nullable();
            $table->string('device')->nullable();
            $table->enum('status',['Logged-In','Logged-Out','Expired'])->nullable();
            $table->dateTime('login_time')->nullable();
            $table->dateTime('logout_time')->nullable();           
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_history');
    }
}
