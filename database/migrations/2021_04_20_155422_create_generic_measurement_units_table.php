<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenericMeasurementUnitsTable extends Migration
{

    public function up()
    {
        Schema::create('generic_measurement_units', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->string('name'); // 1kg,2ks,3tr,
            $table->string('param_1')->nullable(); // 
            $table->string('param_2')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('generic_measurement_units');
    }
}
