<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenericVatCodesTable extends Migration
{

    public function up()
    {
        Schema::create('generic_vat_codes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->string('name');
            $table->enum('param_1', ['Included', 'Excluded'])->nullable();
            $table->string('param_2')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('generic_vat_codes');
    }
}
