<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable()->references('id')->on('users');
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->foreignId('group_id')->nullable()->references('id')->on('groups');
            $table->foreignId('category_id')->nullable()->references('id')->on('categories');
            $table->foreignId('measurement_unit')->nullable()->references('id')->on('generic_measurement_units');
            $table->string('product_name');
            $table->string('code')->nullable();
            $table->text('short_description')->nullable();
            $table->string('image')->nullable();
            $table->string('varient')->nullable();
            $table->decimal('unit_price');
            $table->decimal('purchase_price')->nullable();
            $table->decimal('sale_price')->nullable();
            $table->decimal('discount')->nullable();
            $table->decimal('special_price')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('tax')->nullable();
            $table->enum('tax_type', ['Included', 'Excluded'])->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
