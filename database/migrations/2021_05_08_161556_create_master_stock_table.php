<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterStockTable extends Migration
{

    public function up()
    {
        Schema::create('master_stock', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->integer('master_stock');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('master_stock');
    }
}
