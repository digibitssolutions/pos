<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldOptionTypesTable extends Migration
{

    public function up()
    {
        Schema::create('field_option_types', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->string('type_description');
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('field_option_types');
    }
}
