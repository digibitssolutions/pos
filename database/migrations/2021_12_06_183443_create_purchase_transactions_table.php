<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->foreignId('party_id')->references('id')->on('parties');
            $table->foreignId('purchase_id')->nullable()->references('id')->on('purchases');
            $table->integer('transaction_type_id');
            $table->string('transaction_id', 64)->unique();
            $table->decimal('credit')->nullable();
            $table->decimal('debit')->nullable();
            $table->decimal('remaining_credit')->nullable();
            $table->string('note', 255)->nullable();
            $table->string('payment_via', 255)->nullable();
            $table->string('payment_via_mobile', 13)->nullable();
            $table->string('payment_via_cnic', 13)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_transactions');
    }
}
