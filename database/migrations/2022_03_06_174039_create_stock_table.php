<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{

    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->foreignId('party_id')->nullable()->references('id')->on('parties');
            $table->string('bill_no');
            $table->date('bill_date');
            $table->enum('payment_type', ['cash', 'credit']);
            $table->string('subject')->nullable();
            $table->text('notes')->nullable();
            $table->text('terms_conditions')->nullable();
            $table->integer('shipping_charges')->nullable();
            $table->float('total_amount');
            $table->float('received_amount')->nullable();
            $table->float('remaining_amount')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
