<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockIntakeTable extends Migration
{

    public function up()
    {
        Schema::create('stock_intake', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->foreignId('party_id')->nullable()->references('id')->on('parties');
            $table->decimal('price')->nullable();
            $table->integer('stock');
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    public function down()
    {
        Schema::dropIfExists('stock_intake');
    }
}
