<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{

    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->references('id')->on('shops');
            $table->foreignId('client_id')->nullable()->references('id')->on('clients');
            $table->foreignId('sale_person_id')->nullable()->references('id')->on('sale_persons');
            $table->string('invoice_no');
            $table->string('order_no')->nullable()->unique();
            $table->date('invoice_date');
            $table->date('delivery_date');
            $table->enum('payment_type', ['cash', 'credit']);
            $table->string('subject')->nullable();
            $table->text('customer_notes')->nullable();
            $table->text('terms_conditions')->nullable();
            $table->integer('total_amount');
            $table->integer('shipping_charges')->nullable();
            $table->enum('invoice_status', ['Pending', 'Paid', 'Processing'])->default('Pending');
            $table->decimal('received_amount')->nullable();
            $table->decimal('remaining_amount')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
