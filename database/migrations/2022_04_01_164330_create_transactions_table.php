<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->integer('transaction_type_id');
            $table->foreignId('invoice_id')->nullable()->references('id')->on('invoices');
            $table->string('transaction_id', 64)->unique();
            $table->decimal('credit')->nullable();
            $table->decimal('debit')->nullable();
            $table->decimal('remaining_credit')->nullable();
            $table->string('note', 255)->nullable();
            $table->string('payment_via', 255)->nullable();
            $table->string('payment_via_mobile', 13)->nullable();
            $table->string('payment_via_cnic', 13)->nullable();
            $table->date('transaction_date')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
