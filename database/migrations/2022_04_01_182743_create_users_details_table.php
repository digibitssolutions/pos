<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->references('id')->on('users');
            $table->foreignId('created_by')->nullable()->references('id')->on('users');
            $table->string('phone', 40)->nullable();
            $table->string('mobile', 40)->nullable();
            $table->string('designation', 40)->nullable();
            $table->string('department', 50)->nullable();
            $table->string('website', 250)->nullable();

            $table->text('address')->nullable();
            $table->string('city', 40)->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('tehsil')->nullable();
            $table->string('district')->nullable();
            $table->string('state', 40)->nullable();
            $table->string('fax')->nullable();

            //Other Details
            $table->string('facebook', 250)->nullable();
            $table->string('twitter', 250)->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_details');
    }
}
