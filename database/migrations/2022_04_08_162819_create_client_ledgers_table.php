<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_ledgers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->string('invoice_no')->nullable();
            $table->double('paid')->default(0);
            $table->double('payable')->default(0);
            $table->enum('status', ['purchased','paid']);
            $table->enum('payment_type', ['cash', 'bank']);
            $table->date('invoice_date');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_ledgers');
    }
}
