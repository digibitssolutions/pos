<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalePersonLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_person_ledgers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->references('id')->on('shops');
            $table->foreignId('sale_person_id')->references('id')->on('sale_persons');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->date('date')->nullable();
            $table->double('paid')->default(0);
            $table->double('payable')->default(0);
            $table->enum('payment_type', ['cash', 'bank']);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_person_ledgers');
    }
}
