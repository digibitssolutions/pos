<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnumToClientLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_ledgers', function () {
            \DB::statement("ALTER TABLE `client_ledgers` CHANGE `payment_type` `payment_type` ENUM('cash', 'bank', 'credit') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_ledgers', function () {
            //
        });
    }
}
