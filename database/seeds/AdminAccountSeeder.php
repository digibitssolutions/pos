<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{

    public function run()
    {
        $admin = new User();
        $admin->type = "Admin";
        $admin->email = "info@gmail.com";
        $admin->password = Hash::make(12123456);
        $admin->email_verified = true;
        $admin->status = 'Active';
        $admin->save();
    }
}
