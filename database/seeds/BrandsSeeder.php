<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use App\Http\Controllers\Brands\Models\Brand;

class BrandsSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        for ($i = 1; $i < 10; $i++) {
            $brands[] = [
                'shop_id' => $i,
                'group_id' => $i,
                'brand_name' => $faker->name(),
                'code' => $i,
                'short_description' => $faker->text($maxNbChars = 100),
                'created_at' => date("Y-m-d H:i:s"),
            ];
        }
        Brand::insert($brands);
    }
}
