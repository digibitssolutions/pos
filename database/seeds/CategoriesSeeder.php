<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use App\Http\Controllers\Categories\Models\Category;

class CategoriesSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        for ($i=1; $i < 10; $i++) { 
            $categories [] = [
                'shop_id' => $i,
                'group_id' => $i,
                // 'brand_id' => $i,
                'category_name' => $faker->name(),
                'code' => $i,
                'short_description' => $faker->text($maxNbChars = 100),
                'created_at' => date("Y-m-d H:i:s"),  
            ];
        }
            Category::insert($categories);
    }
}
