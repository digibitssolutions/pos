<?php

use Database\Seeders\DefaultValuesSeeder;
use Illuminate\Database\Seeder;
use Database\Seeders\InvoicesSeeder;
use Database\Seeders\InvoiceProductsSeeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call([
            // AdminAccountSeeder::class,
            // HamzaTradersSeeder::class,
            DefaultValuesSeeder::class,
        ]);
    }
}
