<?php

namespace Database\Seeders;

use App\Http\Controllers\Modules\File\Modules;
use App\Http\Controllers\Modules\Models\Module;
use FontLib\Table\Type\name;
use Illuminate\Database\Seeder;

use function GuzzleHttp\Promise\each;

class DefaultValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modulesList = Modules::modulesList;
        $preparedModule = [];
        foreach ($modulesList as $key => $module) {
            $preparedModule[$key] = [
                'name' => $module['name'],
                'namespace' => $module['namespace'],
                'status' => (isset($module['status'])) ? $module['status'] : 'Inactive'
            ];
        }

        Module::insert($preparedModule);
    }
}
