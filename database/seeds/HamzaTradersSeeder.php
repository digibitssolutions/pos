<?php

use App\Http\Controllers\Shops\Models\Shop;
use App\Http\Controllers\UserDetails\Models\UserDetail;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class HamzaTradersSeeder extends Seeder
{

    public function run()
    {
        $admin = new User();
        $admin->type = "Shop";
        $admin->email = "hamzatraders@gmail.com";
        $admin->first_name = "Hamza";
        $admin->last_name = "Traders";
        $admin->password = Hash::make(12123456);
        $admin->email_verified = true;
        $admin->status = 'Active';
        $admin->save();

        Shop::create([
            'user_id' => $admin->id,
            'shop_code' => '0001',
            'shop_name' => 'Hamza Traders'
        ]);

        UserDetail::create([
            'user_id' => $admin->id,
            'phone' => '0348-1864501',
            'mobile' => '0348-1864501',
            'address' => 'Gulberg Road, Lalamusa',
            'city' => 'Lalamusa',
            'zip' => '50200',
            'tehsil' => 'Kharian',
            'district' => 'Gujrat',
            'state' => 'Punjab',
        ]);
        
    }
}
