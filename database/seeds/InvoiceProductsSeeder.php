<?php

namespace Database\Seeders;

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use App\Http\Controllers\InvoiceProducts\Models\InvoiceProduct;

class InvoiceProductsSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        for ($i = 1; $i < 10; $i++) {
            $invoices[] = [
                'invoice_id' => $i,
                'item_details' => $faker->name(),
                'quantity' => $faker->randomDigit,
                'price' => $faker->randomDigit,
                'discount' => $faker->randomDigit,
                'tax' => $faker->randomDigit,
                'amount' => $faker->randomDigit,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        }
        InvoiceProduct::insert($invoices);
    }
}
