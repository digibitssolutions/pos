<?php

namespace Database\Seeders;

use App\Http\Controllers\Invoices\Models\Invoice;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class InvoicesSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        for ($i = 1; $i < 10; $i++) {
            $invoices[] = [
                'shop_id' => $i,
                'customer' => $faker->name(),
                'sales_person' => $faker->name(),
                'invoice_no' => $i,
                'order_no' => $i,
                'invoice_date' => date("Y-m-d"),
                'delivery_date' => date("Y-m-d"),
                'payment_type' => 'cash',
                'subject' => $faker->randomLetter,
                'customer_notes' => $faker->text($maxNbChars = 50),
                'terms_conditions' => $faker->text($maxNbChars = 50),
                'total_amount' => $faker->randomDigit,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        }
        Invoice::insert($invoices);
    }
}
