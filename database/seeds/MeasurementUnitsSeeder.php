<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\Generics\Models\MeasurementUnit;

class MeasurementUnitsSeeder extends Seeder
{

    public function run()
    {
        for ($i=1; $i < 10; $i++) { 
            $measurementUnits [] = [
                'shop_id' => $i,
                'name' => $i.'kg',
                'created_at' => date("Y-m-d H:i:s"),  
            ];
        }
            MeasurementUnit::insert($measurementUnits);
    }
}
