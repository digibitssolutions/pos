<?php

use App\Http\Controllers\Shops\Models\Shop;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ShopAccountSeeder extends Seeder
{
    public function run(Faker $faker)
    {
        $j = 0;
        for ($i = 3; $i <= 10; $i++) {
            $shops[] = [
                'user_id' => $i,
                'shop_code' => ++$j . '-' . $faker->numberBetween(1, 100),
                'shop_name' => $faker->name(),
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
                'image' => $faker->imageUrl(),
                'address' => $faker->streetAddress,
                'city' => $faker->city,
                'tehsil' => $faker->city,
                'district' => $faker->city,
                'fax' => $faker->phoneNumber . '+',
                'postal_code' => $faker->randomDigit,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ];
        }
        Shop::insert($shops);
    }
}
