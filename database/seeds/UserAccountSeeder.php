<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        for ($i=1; $i < 10; $i++) { 
            $users [] = [
                'name' => $faker->name(),
                'email' => 'shop'.$i.'@gmail.com',
                'password' => Hash::make(12123456),
                'type' => 'Shop',
                'status' => 'Active',
                'email_verified' => 1,
                'code' => $i,
                'created_at' => date("Y-m-d H:i:s"),  
                'updated_at' => date("Y-m-d H:i:s")
            ];
        }
            User::insert($users);
    }
}
