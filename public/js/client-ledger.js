
$(".add-client-ledger-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-client-ledger-form").serialize();

    $('.load-client-tran-btn').removeAttr('style');
    $('.add-client-ledger-btn').hide();
    
    $.ajax({
        url: '/administrator/store-client-ledger',
        type: "POST",
        data: data,
        success: function (res) {
            $('#client-ledger-modal').modal('hide');
            $('#add-client-ledger-form').trigger('reset');
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
            $('.add-client-ledger-btn').show();
            $('.load-client-tran-btn').hide();
            $('.client-ledger-table').load(' .client-ledger-table');

        },
        error: function (error) {
            $('#client-ledger-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.add-client-ledger-btn').show();
            $('.load-client-tran-btn').hide();
            var er = error.responseJSON.errors;

            $.each(er, function (key, value) {
                $('.alert-success p').html(value);
            });
        }
    });

});
