$(".edit-client-btn").on('click', function () {
    var id = $(this).data('id');

    var cId = $('.edit-client-id');
    var fname = $('.edit-client-first');
    var mname = $('.edit-client-middle');
    var lname = $('.edit-client-last');
    var email = $('.edit-client-email');
    var mob = $('.edit-client-mob');
    var phone = $('.edit-client-phone');
    var depart = $('.edit-client-depart');
    var city = $('.edit-client-city');
    var state = $('.edit-client-state');
    var desig = $('.edit-client-designation');
    var zip = $('.edit-client-zip');
    var web = $('.edit-client-web');
    var fb = $('.edit-client-fb');
    var tw = $('.edit-client-tw');
    var type = $('.edit-client-type');
    var addr = $('.edit-client-addr');

    $.ajax({
        type: "get",
        url: "/administrator/edit-client/" + id,
        success: function (res) {
            cId.val(res.id);
            fname.val(res.user.first_name);
            mname.val(res.user.middle_name);
            lname.val(res.user.last_name);
            email.val(res.user.email);
            mob.val(res.user.user_detail.mobile);
            phone.val(res.user.user_detail.phone);
            depart.val(res.user.user_detail.department);
            city.val(res.user.user_detail.city);
            state.val(res.user.user_detail.state);
            desig.val(res.user.user_detail.designation);
            zip.val(res.user.user_detail.zip);
            web.val(res.user.user_detail.website);
            fb.val(res.user.user_detail.facebook);
            tw.val(res.user.user_detail.twitter);
            type.val(res.user.user_detail.type);
            addr.val(res.user.user_detail.address);
        }
    });
});


$(".update-client-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#update-client-form").serialize();

    $.ajax({
        url: '/administrator/update-client',
        type: "POST",
        data: data,
        success: function (res) {
            $('#update-client-form').trigger('reset');
            $('#update-client-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.alert-success').html(res.message);
        },
        error: function (error) {
            $('#update-client-form').trigger('reset');
            $('#update-client-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            var er = error.responseJSON.errors;
            printErrorMsg(er);
        }
    });

});


$(".add-client-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-client-form").serialize();
    $('.load-client-btn').removeAttr('style');
    $('.add-client-btn').hide();

    $.ajax({
        url: '/administrator/store-client',
        type: "POST",
        data: data,
        success: function (res) {
            $('#add-client-modal').modal('hide');
            $('#add-client-form').trigger('reset');
            $('.error').removeAttr('style');
            $('.error ul').html(res.message);
            $('.add-client-btn').show();
            $('.load-client-btn').hide();
            $(".clients-table").load(" .clients-table");
        },
        error: function (error) {
            $('#add-client-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.add-client-btn').show();
            $('.load-client-btn').hide();
            var msg = error.responseJSON.errors;
            printErrorMsg(msg);
        }
    });

});


$(".view-client-btn").on('click', function () {
    var id = $(this).data('id');

    var cId = $('.view-client-id');
    var fname = $('.view-client-first');
    var mname = $('.view-client-middle');
    var lname = $('.view-client-last');
    var email = $('.view-client-email');
    var mob = $('.view-client-mob');
    var phone = $('.view-client-phone');
    var depart = $('.view-client-depart');
    var city = $('.view-client-city');
    var state = $('.view-client-state');
    var desig = $('.view-client-designation');
    var zip = $('.view-client-zip');
    var web = $('.view-client-web');
    var fb = $('.view-client-fb');
    var tw = $('.view-client-tw');
    var type = $('.view-client-type');
    var addr = $('.view-client-addr');

    $.ajax({
        type: "get",
        url: "/administrator/show-client/" + id,
        success: function (res) {
            console.log(res);
            fname.val(res.user.first_name);
            mname.val(res.user.middle_name);
            lname.val(res.user.last_name);
            email.val(res.user.email);
            mob.val(res.user.user_detail.mobile);
            phone.val(res.user.user_detail.phone);
            depart.val(res.user.user_detail.department);
            city.val(res.user.user_detail.city);
            state.val(res.user.user_detail.state);
            desig.val(res.user.user_detail.designation);
            zip.val(res.user.user_detail.zip);
            web.val(res.user.user_detail.website);
            fb.val(res.user.user_detail.facebook);
            tw.val(res.user.user_detail.twitter);
            type.val(res.user.type);
            addr.val(res.user.user_detail.address);
        }
    });
});

function printErrorMsg(msg) {
    $(".error").find("ul").html('');
    $(".error").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".error").find("ul").append('<li>' + value + '</li>');
    });
}

$('#add-client-form').on('blur keyup', function () {
    if ($("#add-client-form").validate().checkForm()) {
        $('.add-client-btn').prop('disabled', false);
    } else {
        $('.add-client-btn').prop('disabled', 'disabled');
    }
});
