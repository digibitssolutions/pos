$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



// < ====================== Short Stock Start ====================== >
$(".required-stock").on('input', function () {
    var shortRow = $(this).closest('tr');
    var shortStock = shortRow.find(".required-stock").val();

    if (shortStock > 0) {
        $(".required-stock-btn").removeAttr('disabled');
    } else {
        $(".required-stock-btn").attr('disabled', 'true');
    }
});
// < ====================== Short Stock Ends ====================== >

// < ====================== Invoices ====================== >
$("#invoice-product").on('change', function () {
    $(".invoice-product-quantity").on('input', function () {
        var inPQuantity = $(".invoice-product-quantity").val();
        var inPMStock = $("#invoice-product-master-stock").val();
        if (+inPQuantity <= +inPMStock) {
            $('#add-product').attr("disabled", false);
        } else {
            $('#add-product').attr("disabled", true);
        }
    });
    $('#add-product').attr("disabled", true);
});


$("#invoice-product").select2()
    .on('change', function () {
        var getID = $(this).select2('data');
        var id = getID[0]['id'];

        $.ajax({
            type: "get",
            url: "single-product" + '/' + id,
            success: function (res) {
                $('.invoice-product-name').val(res.product.product_name);
                $('.invoice-product-price').val(res.product.sale_price);
                if (res.product.master_stock && res.product.master_stock.master_stock > 0) {
                    $('#invoice-product-master-stock').val(res.product.master_stock.master_stock);
                } else {
                    $('#invoice-product-master-stock').val(0);
                }
                $('.invoice-product-amount').val(res.product.sale_price * 1);
            }
        });
    });

$(".update-invoice-product-select").select2()
    .on('change', function (e) {
        var getID = $(this).select2('data');
        var id = getID[0]['id'];
        var currentRow = $(this).closest("tr");

        $.ajax({
            type: "get",
            url: '/administrator/single-product/' + id,
            success: function (res) {
                currentRow.find('.invoice-product-name').val(res.product_name);
                currentRow.find(".invoice-product-price").val(res.sale_price);
                if (res.master_stock) {
                    $('.invoice-product-master-stock').val(res.master_stock.master_stock);
                } else {
                    $('#invoice-product-master-stock').val(0);
                }
                currentRow.find('.invoice-product-amount').val(res.sale_price * 1);
            }
        });
    });


$(".update-invoice-product").select2()
    .on('change', function (e) {
        var getID = $(this).select2('data');
        var id = getID[0]['id'];

        $.ajax({
            type: "get",
            url: '/administrator/single-product/' + id,
            success: function (res) {
                $('.invoice-product-name-1').val(res.product_name);
                $('.invoice-product-price-1').val(res.sale_price);
                if (res.master_stock) {
                    $('.invoice-product-master-stock-1').val(res.master_stock.master_stock);
                    $('.add-product-1').removeAttr('disabled');
                } else {
                    $('.invoice-product-master-stock-1').val(0);
                    $('.add-product').attr('disabled', 'disabled');
                }
                $('.invoice-product-amount-1').val(res.sale_price * 1);
            }
        });
    });

//add to cart 
var count = 1;
$('#add-product').on('click', function () {
    var product_id = $('#invoice-product').val();

    var name = $('#item_details').val();
    var price = $('#price').val();
    var qty = $('#quantity').val();

    billFunction(); // Below Function passing here 

    function billFunction() {
        var total = 0;
        $("#receipt_bill").each(function () {
            var total = price * qty;
            var subTotal = 0;
            subTotal += parseFloat(total);

            var table = '<tr><td><input type="hidden" name="product_id[]" value="' + product_id + '">' + count + '</td><td><input type="hidden" name="item_details[]" value="' + name + '">' + name + '</td><td><input type="hidden" name="price[]" value="' + price + '">' + price + '</td><td><input type="hidden" name="quantity[]" value="' + qty + '">' + qty + '</td><td><strong><input type="hidden" name="amount[]" id="total" value="' + total + '">' + total + '</strong></td><td><i class="fa fa-times" id="remove" style="cursor:pointer;"></i></td></tr>';
            // $('#new').append(table);
            $('#new').append(table);
            var total = 0;
            $('tbody tr td:nth-last-child(2)').each(function () {
                var value = parseFloat($('#total', this).val());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            $('#subTotal').text(total);
            var Subtotal = $('#subTotal').text();
            // var taxAmount = $('#taxAmount').text();

            var totalPayment = parseFloat(Subtotal);
            $('#totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 

        });
        count++;

        $("#invoice-total-amount").val($('#totalPayment').text());
    }

    $('#invoice-product').val('-- Select Product --');
    $('#item_details').val('');
    $('#price').val('');
    $('#quantity').val('1');
    $('#invoice-product-master-stock').val(0);
    $('#amount').val('0');
});

$("#new").on('click', '#remove', function () {
    $(this).closest('tr').remove();
    var subTotal = 0;
    subTotal += parseFloat(total);

    // Code for Sub Total of Vegitables 
    var total = 0;
    $('tbody tr td:nth-last-child(2)').each(function () {
        var value = parseFloat($('#total', this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $('#subTotal').text(total);

    var Subtotal = $('#subTotal').text();

    var totalPayment = parseFloat(Subtotal);
    $('#totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 

});


$('#quantity').on('input', function () {
    var price = $('#price').val();
    var qty = $('#quantity').val();
    var discount = $('#discount').val();
    var amount = qty * price;
    $('#amount').val(amount);
    if (discount > 0) {
        var amount = (price - (discount * price / 100)) * qty;
        $('#amount').val(amount);
    }
});

// < ====================== Invoices End ====================== >

// < ====================== Update Invoice Start ====================== >
var count2 = 1;
$('.add-product-1').on('click', function () {
    var product_id = $('.update-invoice-product').val();
    var name = $('.invoice-product-name-1').val();
    var price = $('.invoice-product-price-1').val();
    var qty = $('.invoice-product-quantity-1').val();
    billFunction2(); // Below Function passing here 

    function billFunction2() {
        var total = 0;
        $(".receipt_bill").each(function () {
            var total = price * qty;
            var subTotal = 0;
            subTotal += parseInt(total);

            var table = '<tr class="product"><td><input type="hidden" name="product_id[]" value="' + product_id + '"><input type="text" class="form-control" name="item_details[]" value="' + name + '" readonly></td><td><input type="text" class="form-control" value="' + name + '" readonly></td><td><input class="form-control" type="number" name="price[]" value="' + price + '" readonly></td><td><input class="form-control" type="number" name="quantity[]" value="' + qty + '" readonly></td><td><input class="form-control total" type="text" name="amount[]" value="' + total + '" readonly></td><td><i class="fa fa-times remove" style="cursor:pointer;"></i></td></tr>';
            $('.new').append(table);
            var total = 0;
            $('tbody tr td:nth-last-child(2)').each(function () {
                var value = parseInt($('.total', this).val());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            $('.subTotal').text(total);
            var Subtotal = $('.subTotal').text();
            // var taxAmount = $('#taxAmount').text();

            var totalPayment = parseFloat(Subtotal);
            $('.totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 

        });
        count2++;

        $(".invoice-total-amount").val($('.totalPayment').text());
    }
    $('.update-invoice-product').val('');
    $('#item_details').val('');
    $('#price').val('');
    $('#quantity').val('1');
    $('#invoice-product-master-stock').val(0);
    $('#amount').val('0');
});

$(window).load(function () {
    var total = 0;
    $('tbody tr td:nth-last-child(2)').each(function () {
        var value = parseInt($('.total', this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $('.subTotal').text(total);
    var Subtotal = $('.subTotal').text();
    // var taxAmount = $('#taxAmount').text();

    var totalPayment = parseFloat(Subtotal);
    $('.totalPayment').text(totalPayment.toFixed(2));
});

$('.invoice-product-quantity').on('input', function () {
    var currentRow = $(this).closest("tr");
    var price = currentRow.find(".invoice-product-price").val();
    var qty = currentRow.find(".invoice-product-quantity").val();
    var amount = qty * price;
    currentRow.find('.invoice-product-amount').val(amount);

    var total = price * qty;
    var subTotal = 0;
    subTotal += parseInt(total);

    var total = 0;
    $('tbody tr td:nth-last-child(2)').each(function () {
        var value = parseInt($('.total', this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $('.subTotal').text(total);
    var Subtotal = $('.subTotal').text();

    var totalPayment = parseFloat(Subtotal);
    $('.totalPayment').text(totalPayment.toFixed(2));
    $(".invoice-total-amount").val($('.totalPayment').text());

});

$(".remove-id").on('click', function () {
    var id = $(this).data('id');

    $(this).closest("tr").remove();
    $.ajax({
        type: "post",
        url: '/administrator/delete-product/' + id,
        data: {
            '_token': $('input[name=_token]').val()
        },
        success: function (res) {
            alert(res.message);
        }
    });

    var total = 0;
    $('tbody tr td:nth-last-child(2)').each(function () {
        var value = parseInt($('.total', this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $('.subTotal').text(total);

    var Subtotal = $('.subTotal').text();

    var totalPayment = parseFloat(Subtotal);

    $('.totalPayment').text(totalPayment.toFixed(2));
    $(".invoice-total-amount").val($('.totalPayment').text());

});
        // < ====================== Update Invoice End ====================== >
