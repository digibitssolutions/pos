// < ====================== STOCK INTAKE ====================== >

$(".select-product-name").select2()
    .on('change', function (e) {
        var getID = $(this).select2('data');
        var id = getID[0]['id'];

        $.ajax({
            type: "get",
            url: "single-stock-intake-product/" + id,
            success: function (res) {
                $('#stock-product-name').val(res.product_name);
                $('.product-purchase-price').val(res.purchase_price);
                $('.product-sale-price').val(res.sale_price);
                // $('#add-stock').removeAttr('disabled');
                if (res.master_stock) {
                    $('.product-master-stock').val(res.master_stock.master_stock);
                } else {
                    $('.product-master-stock').val(0);
                }

            }
        });

    });


$(document).on('focusout', ".product-purchase-price, .product-sale-price", function () {

    var id = $('#stock-product').val();
    var purchase = $('.product-purchase-price').val();
    var sale = $('.product-sale-price').val();
    const param = id + '/' + purchase + '/' + sale;

    $.ajax({
        type: "post",
        url: "update-product-price/" + param,
        success: function (res) {
            $('#message').css("display", "block");
            $('#message p').text(res.message);
        },
        error: function (er) {
            $('#message').css("display", "none");
        }
    });
});

// Enter Trigger
$(".product-stock").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#add-stock").click();
    }
})


// Setting Party Id in Stock Intake
$('.stock-intak-party').on('change', function () {
    var party = $('.stock-intak-party').val();
    $('.reciept-party').val(party);
});


// Setting Purchase Ledger in Stock Intake
$('#save-stock').on('click', function () {
    var billNo = $('#purchase-bill-no').val();
    var billDate = $('#purchase-bill-date').val();
    var payable = $('.stock-payment').text();
    var type = $('#purchase-payment').val();
    var paidAmount = $('#purchase-paid-amount').val();

    $('.purchase-bill-no').val(billNo);
    $('.purchase-bill-date').val(billDate);
    $('.purchase-payable').val(payable);
    $('.purchase-payment').val(type);
    $('.purchase-paid-amount').val(paidAmount);
});


var newCount = 1;
$('#add-stock').on('click', function () {
    var product_id = $('#stock-product').val();
    var name = $('#stock-product-name').val();
    var price = $('.product-purchase-price').val();
    var masterStock = $('.product-master-stock').val();
    var stock = $('.product-stock').val();

    $('#new-stock').each(function () {
        var amount = price * stock;
        var total = 0;
        var table = '<tr><td><input type="hidden" name="product_id[]" value="' + product_id + '">' + newCount + '</td><td>' + name + '</td><td>' + masterStock + '</td><td><input type="hidden" name="stock[]" value="' + stock + '">' + stock + '</td><td><input type="hidden" name="price[]" value="' + price + '">' + price + '</td><td>' + stock + '</td><td><input type="hidden" class="stock-total" value="' + amount + '">' + amount + '</td><td><i class="fa fa-times" id="stock-remove" style="cursor:pointer;"></i></td></tr>';
        $('#new-stock').append(table);

        $('tbody tr td:nth-last-child(2)').each(function () {
            var value = parseFloat($('.stock-total', this).val());
            if (!isNaN(value)) {
                total += value;
            }
        });
        $('.stock-payment').text(total);
    });

    if (stock !== '') {
        $('.product-stock').css({
            "border-color": "#d2d6de"
        });

        $('.product-purchase-price').val('');
        $('.product-sale-price').val('');
        $('.product-master-stock').val('');
        $('.product-stock').val('');
        $('#stock-product').val('-- Select Product --');
        $("#add-stock").attr("disabled", true);
        newCount++;
    } else {
        $('.product-stock').css({
            "border": "1px solid red"
        });
    }

});

// $("#new-stock").on('click', '#stock-remove', function () {
//     var row = $(this).closest("tr");
//     var price = row.find('.stock-total').val();
//     var total = $('.stock-payment').text();
//     var abc = total - price;
//     $('.stock-payment').text(abc);
//     $(this).closest('tr').remove();
// });


// $("#add-stock").on('click', function () {
//     var value = $(".reciept-party").val();

//     setTimeout(() => {
//         if (+value > 0) {
//             $("#save-stock").attr("disabled", false);
//         } else {
//             $("#save-stock").attr("disabled", true);
//         }
//     }, 200);

// });

$(".product-stock").on('input', function () {
    var stock = $(".product-stock").val();
    if (stock === '' || stock == 0) {
        $("#add-stock").attr("disabled", true);
    } else {
        $("#add-stock").attr("disabled", false);
    }
});

$(".stock-switch").on('change', function () {
    let checkAttr = document.querySelector('#save-stock').hasAttribute('disabled');
    if (checkAttr == false) {
        $("#save-stock").attr('disabled', true);
    } else {
        $("#save-stock").attr('disabled', false);
    }
});

// < ====================== Stock Intake ====================== >