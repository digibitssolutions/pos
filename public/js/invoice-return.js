$("#invoice-return-product").select2()
    .on('change', function () {
        var getID = $(this).select2('data');
        var id = getID[0]['id'];
        $.ajax({
            type: "get",
            url: "/administrator/single-product" + '/' + id,
            success: function (res) {
                $('.invoice-product-name').val(res.product.product_name);
                $('.invoice-product-price').val(res.product.sale_price);
                if (res.product.master_stock && res.product.master_stock.master_stock > 0) {
                    $('#invoice-return-master-stock').val(res.product.master_stock.master_stock);
                } else {
                    $('#invoice-return-master-stock').val(0);
                }
                $('.invoice-product-amount').val(res.product.sale_price * 1);
            }
        });
    });
//add to cart 
var count = 1;
$('#return-product').on('click', function () {
    var product_id = $('#invoice-return-product').val();
    var name = $('#item_details').val();
    var price = $('#price').val();
    var qty = $('#quantity').val();
    billFunction(); // Below Function passing here 

    function billFunction() {

        var total = 0;
        $("#receipt_bill").each(function () {
            var total = price * qty;
            var subTotal = 0;
            subTotal += total;
            var table = '<tr><td><input type="hidden" name="product_id[]" value="' + product_id + '">' + count + '</td><td><input type="hidden" name="item_details[]" value="' + name + '">' + name + '</td><td><input type="hidden" name="price[]" value="' + price + '">' + price + '</td><td><input type="hidden" name="quantity[]" value="' + qty + '">' + qty + '</td><td><strong><input type="hidden" name="amount[]" id="total" value="' + total + '">' + total + '</strong></td><td><i class="fa fa-times" id="remove" style="cursor:pointer;"></i></td></tr>'; -
            $('#new').append(table);
            var total = 0;
            $('tbody tr td:nth-last-child(2)').each(function () {
                var value = parseInt($('#total', this).val());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            $('#subTotal').text(total);
            var Subtotal = $('#subTotal').text();
            var totalPayment = parseFloat(Subtotal); -
            $('#totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 
        });
        count++;
        $("#invoice-total-amount").val($('#totalPayment').text());
    }
    $('#invoice-product').val('-- Select Product --');
    $('#item_details').val('');
    $('#price').val('');
    $('#quantity').val('1');
    $('#invoice-product-master-stock').val(0);
    $('#amount').val('0');
});

$("#new").on('click', '#remove', function () {
    $(this).closest('tr').remove();
    var subTotal = 0;
    subTotal += parseInt(total);
    var total = 0;
    $('tbody tr td:nth-last-child(2)').each(function () {
        var value = parseInt($('#total', this).val());
        if (!isNaN(value)) {
            total += value;
        }
    });
    $('#subTotal').text(total);
    var Subtotal = $('#subTotal').text();
    var totalPayment = parseFloat(Subtotal);
    $('#totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 


});

$('#quantity').on('input', function () {
    var price = $('#price').val();
    var qty = $('#quantity').val();
    var discount = $('#discount').val();
    var amount = qty * price;
    $('#amount').val(amount);
    if (discount > 0) {
        var amount = (price - (discount * price / 100)) * qty;
        $('#amount').val(amount);
    }
});

// client id
$('#customer').select2()
    .on('change', function () {
        let clientId = $(this).val();
        $('.set-client').val(clientId)
    });

// add btn enable/disable
$("#invoice-return-product").on('change', function () {
    let exists = $("#invoice-return-product").val().length;
    if (exists) {
        $('#return-product').attr("disabled", false);
    } else {
        $('#return-product').attr("disabled", true);
    }
});

// inv date set
if ($("#invoice_date").val()) {
    let date = $("#invoice_date").val();
    $(".set-invoice-date").val(date);
}

$('#invoice_date').on('change', function () {
    let date = $("#invoice_date").val();
    $(".set-invoice-date").val(date);
});

// save btn enable/disable
$(".return-switch").on('change', function () {
    let checkAttr = document.querySelector('#return-save-btn').hasAttribute('disabled');
    if (checkAttr == false) {
        $("#return-save-btn").attr('disabled', true);
    } else {
        $("#return-save-btn").attr('disabled', false);
    }
});
