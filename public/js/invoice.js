$("#invoice-product").select2()
    .on('change', function () {
        let cId = $("#customer").val();
        var getID = $(this).select2('data');
        var id = getID[0]['id'];

        if (cId && id)
            $.ajax({
                type: "get",
                url: "get-last-product" + '/' + cId + '/' + id,
                success: function (res) {
                    console.log(res);
                    $('#last-product').val(res.data);
                }
            });
    });

$(".invoice-switch").on('change', function () {
    let checkAttr = document.querySelector('#invoice-save-btn').hasAttribute('disabled');
    if (checkAttr == false) {
        $("#invoice-save-btn").attr('disabled', true);
    } else {
        $("#invoice-save-btn").attr('disabled', false);
    }
});

// Invoice Print

$('.print-invoice').on('click', function () {
    var id = $(this).data('id');
    $.ajax({
        type: "get",
        url: "print-to-invoice/" + id,
        success: function (res) {
            let inv = window.open('_blank');
            inv.document.write(res);
            inv.focus();
            inv.print();
        }
    });
});
