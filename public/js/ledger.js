
$(".add-ledger-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-ledger-form").serialize();
    $('.load-pur-tran-btn').removeAttr('style');
    $('.add-ledger-btn').hide();
    
    $.ajax({
        url: '/administrator/store-ledger',
        type: "POST",
        data: data,
        success: function (res) {
            $('#ledger-modal').modal('hide');
            $('#add-ledger-form').trigger('reset');
            $('.add-ledger-btn').show();
            $('.load-pur-tran-btn').hide();
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
            $(".purchase-ledger-table").load(" .purchase-ledger-table");

        },
        error: function (error) {
            console.log(error);
            $('#ledger-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.add-ledger-btn').show();
            $('.load-pur-tran-btn').hide();
            var er = error.responseJSON.errors;

            $.each(er, function (key, value) {
                $('.alert-success p').html(value);
            });
        }
    });

});