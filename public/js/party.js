$(".edit-party-btn").on('click', function () {
    var id = $(this).data('id');

    var pId = $('.edit-party-id');
    var fname = $('.edit-party-first');
    var mname = $('.edit-party-middle');
    var lname = $('.edit-party-last');
    var email = $('.edit-party-email');
    var name = $('.edit-party-name');
    var mob = $('.edit-party-mob');
    var phone = $('.edit-party-phone');
    var depart = $('.edit-party-depart');
    var city = $('.edit-party-city');
    var state = $('.edit-party-state');
    var desig = $('.edit-party-designation');
    var zip = $('.edit-party-zip');
    var web = $('.edit-party-web');
    var fb = $('.edit-party-fb');
    var tw = $('.edit-party-tw');
    var type = $('.edit-party-type');
    var addr = $('.edit-party-addr');

    $.ajax({
        type: "get",
        url: "/administrator/show-party/" + id,
        success: function (res) {
            console.log(res);
            pId.val(res.id);
            fname.val(res.first_name);
            mname.val(res.middle_name);
            lname.val(res.last_name);
            email.val(res.email);
            name.val(res.party_name);
            mob.val(res.mobile);
            phone.val(res.phone);
            depart.val(res.department);
            city.val(res.city);
            state.val(res.state);
            desig.val(res.designation);
            zip.val(res.zip);
            web.val(res.website);
            fb.val(res.facebook);
            tw.val(res.twitter);
            type.val(res.type);
            addr.val(res.address);
        }
    });
});


$(".update-party-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#update-party-form").serialize();

    $.ajax({
        url: '/administrator/update-party',
        type: "POST",
        data: data,
        success: function (res) {
            console.log(res);
            $('#update-party-form').trigger('reset');
            $('#update-party-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
        },
        error: function (error) {
            $('#update-party-form').trigger('reset');
            $('#update-party-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            var ab = error.responseJSON.errors;
            $.each(ab, function (key, value) {
                console.log(value);
                $('.alert-success p').html(value);
            });
        }
    });

});


$(".add-party-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-party-form").serialize();
    $('.load-party-btn').removeAttr('style');
    $('.add-party-btn').hide();

    $.ajax({
        url: '/administrator/store-party',
        type: "POST",
        data: data,
        success: function (res) {
            $('#add-party-modal').modal('hide');
            $('#add-party-form').trigger('reset');
            $('.error').removeAttr('style');
            $('.error ul').html(res.message);
            $('.add-party-btn').show();
            $('.load-party-btn').hide();
        },
        error: function (error) {
            $('#add-party-modal').modal('hide');
            $('.add-party-btn').show();
            $('.load-party-btn').hide();
            var er = error.responseJSON.errors;
            printErrorMsg(er);
        }
    });

});

$(".view-party-btn").on('click', function () {
    var id = $(this).data('id');

    var fname = $('.view-party-first');
    var mname = $('.view-party-middle');
    var lname = $('.view-party-last');
    var email = $('.view-party-email');
    var name = $('.view-party-name');
    var mob = $('.view-party-mob');
    var phone = $('.view-party-phone');
    var depart = $('.view-party-depart');
    var city = $('.view-party-city');
    var state = $('.view-party-state');
    var desig = $('.view-party-designation');
    var zip = $('.view-party-zip');
    var web = $('.view-party-web');
    var fb = $('.view-party-fb');
    var tw = $('.view-party-tw');
    var type = $('.view-party-type');
    var addr = $('.view-party-addr');

    $.ajax({
        type: "get",
        url: "/administrator/show-party/" + id,
        success: function (res) {
            console.log(res);
            fname.val(res.first_name);
            mname.val(res.middle_name);
            lname.val(res.last_name);
            email.val(res.email);
            name.val(res.party_name);
            mob.val(res.mobile);
            phone.val(res.phone);
            depart.val(res.department);
            city.val(res.city);
            state.val(res.state);
            desig.val(res.designation);
            zip.val(res.zip);
            web.val(res.website);
            fb.val(res.facebook);
            tw.val(res.twitter);
            type.val(res.type);
            addr.val(res.address);
        }
    });
});

function printErrorMsg(msg) {
    $(".error").find("ul").html('');
    $(".error").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".error").find("ul").append('<li>' + value + '</li>');
    });
}

$('#add-party-form').on('blur keyup', function () {
    if ($("#add-party-form").validate().checkForm()) {
        $('.add-party-btn').prop('disabled', false);
    } else {
        $('.add-party-btn').prop('disabled', 'disabled');
    }
});
