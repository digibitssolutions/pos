$(document).on('click', '.edit-product-btn', function () {
    var id = $(this).data('id');
    var pId = $('.edit-product-id');
    var pName = $('.edit-product-name');
    var pCode = $('.edit-product-code');
    var pVarient = $('.edit-product-color');
    var pExp = $('.edit-product-expiry');
    var pUnit = $('.edit-product-unit');
    var pPurchase = $('.edit-product-purchase');
    var pSale = $('.edit-product-sale');
    var pSpecial = $('.edit-product-special');
    var pTax = $('.edit-product-tax');
    var pDisc = $('.edit-product-discount');
    var pDesc = $('.edit-product-description');

    $.ajax({
        type: "get",
        url: "/administrator/edit-product-record/" + id,
        success: function (res) {
            pId.val(res.product.id);
            pName.val(res.product.product_name);
            pCode.val(res.product.code);
            pVarient.val(res.product.varient);
            pExp.val(res.product.expiry_date);
            pUnit.val(res.product.unit_price);
            pPurchase.val(res.product.purchase_price);
            pSale.val(res.product.sale_price);
            pSpecial.val(res.product.special_price);
            pTax.val(res.product.tax);
            pDisc.val(res.product.discount);
            pDesc.val(res.product.short_description);
        }
    });
});

$(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data(page);
});

function fetch_data(page){
    $.ajax({
        url: "/administrator/paginate?page=" + page,
        success: function(data){
            $('#table_data').html(data);
        }
    });
}