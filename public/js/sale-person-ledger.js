
$(".add-sale-ledger-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-sale-ledger-form").serialize();
    console.log(data);
    
    $.ajax({
        url: '/administrator/store-sale-person-ledger',
        type: "POST",
        data: data,
        success: function (res) {
            $('#add-sale-ledger-form').trigger('reset');
            $('#ledger-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
        },
        error: function (error) {
            $('#ledger-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            var er = error.responseJSON.errors;
            var msg = error.responseJSON.message;

            $.each(er, function (key, value) {
                $('.alert-success p').html(value);
            });
            $('.alert-success p').html(msg);
        }
    });

});
