
$(".edit-person-btn").on('click', function () {
    var id = $(this).data('id');
    
    var pId = $('.edit-person-id');
    var fname = $('.edit-person-first');
    var mname = $('.edit-person-middle');
    var lname = $('.edit-person-last');
    var email = $('.edit-person-email');
    var mob = $('.edit-person-mob');
    var phone = $('.edit-person-phone');
    var depart = $('.edit-person-depart');
    var city = $('.edit-person-city');
    var state = $('.edit-person-state');
    var desig = $('.edit-person-designation');
    var zip = $('.edit-person-zip');
    var web = $('.edit-person-web');
    var fb = $('.edit-person-fb');
    var tw = $('.edit-person-tw');
    var type = $('.edit-person-type');
    var addr = $('.edit-person-addr');

    $.ajax({
        type: "get",
        url: "/administrator/show-sales-person/" + id,
        success: function (res) {
            pId.val(res.id);
            fname.val(res.first_name);
            mname.val(res.middle_name);
            lname.val(res.last_name);
            email.val(res.email);
            mob.val(res.mobile);
            phone.val(res.phone);
            depart.val(res.department);
            city.val(res.city);
            state.val(res.state);
            desig.val(res.designation);
            zip.val(res.zip);
            web.val(res.website);
            fb.val(res.facebook);
            tw.val(res.twitter);
            type.val(res.type);
            addr.val(res.address);
        }
    });
});


$(".update-person-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#update-person-form").serialize();

    $.ajax({
        url: '/administrator/update-sales-person',
        type: "POST",
        data: data,
        success: function (res) {
            $('#update-person-form').trigger('reset');
            $('#update-person-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
        },
        error: function (error) {
            $('#update-person-form').trigger('reset');
            $('#update-person-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            var msg = error.responseJSON.message;
            var er = error.responseJSON.error;
            $('.alert-success p').html(msg, er);
            // var ab = error.responseJSON.errors;
            // $.each(ab, function (key, value) {
            //     console.log(value);
            //     $('.alert-success p').html(value);
            // });
        }
    });

});


$(".add-sale-person-btn").on('click', function (event) {
    event.preventDefault();
    let data = $("#add-sale-person-form").serialize();

    $.ajax({
        url: '/administrator/store-sales-person',
        type: "POST",
        data: data,
        success: function (res) {
            $('#add-sale-person-modal').modal('hide');
            $('#add-sale-person-form').trigger('reset');
            $('.alert-success').removeAttr('style');
            $('.alert-success p').html(res.message);
        },
        error: function (error) {
            $('#add-sale-person-modal').modal('hide');
            $('.alert-success').removeAttr('style');
            var msg = error.responseJSON.message;
            var er = error.responseJSON.error;
            $('.alert-success p').html(msg, er);
            // $.each(ab, function (key, value) {
            // });
        }
    });

});

$(".view-person-btn").on('click', function () {
    var id = $(this).data('id');
    
    var fname = $('.view-person-first');
    var mname = $('.view-person-middle');
    var lname = $('.view-person-last');
    var email = $('.view-person-email');
    var mob = $('.view-person-mob');
    var phone = $('.view-person-phone');
    var depart = $('.view-person-depart');
    var city = $('.view-person-city');
    var state = $('.view-person-state');
    var desig = $('.view-person-designation');
    var zip = $('.view-person-zip');
    var web = $('.view-person-web');
    var fb = $('.view-person-fb');
    var tw = $('.view-person-tw');
    var type = $('.view-person-type');
    var addr = $('.view-person-addr');

    $.ajax({
        type: "get",
        url: "/administrator/show-sales-person/" + id,
        success: function (res) {
            fname.val(res.first_name);
            mname.val(res.middle_name);
            lname.val(res.last_name);
            email.val(res.email);
            mob.val(res.mobile);
            phone.val(res.phone);
            depart.val(res.department);
            city.val(res.city);
            state.val(res.state);
            desig.val(res.designation);
            zip.val(res.zip);
            web.val(res.website);
            fb.val(res.facebook);
            tw.val(res.twitter);
            type.val(res.type);
            addr.val(res.address);
        }
    });
});