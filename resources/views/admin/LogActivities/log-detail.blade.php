@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Log Activities</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Type:</label>
                        <input type="text" class="form-control" value="{{$isFound->type}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>DATE & TIME:</label>
                        <input type="text" class="form-control" value="{{$isFound->created_at}}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Method:</label>
                        <input type="text" class="form-control" value="{{$isFound->method}}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>IP Address:</label>
                        <input type="text" class="form-control" value="{{$isFound->ip}}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Created by::</label>
                        <input type="text" class="form-control" value="{{$isFound->created_by}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">URL:</label>
                    <input type="url" class="form-control" value="{{$isFound->url}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control" id="" cols="30" rows="10" style="color:#03ff03;background:black;font-weight:bold;">{{$isFound->message}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Detail</label>
                        <div class="well" style="background:black">
                            @if ($isFound->old_data !== 'null')
                            <?php $data = json_decode($isFound->old_data)?>

                                @foreach ($data as $key=> $value )
                               <span style="color:#03ff03">{{$key}}</span> <span style="color:white">:</span> <span style="color:red">{{print_r($value,true)}}</span><br>
                                @endforeach
                                @else
                                {{'--'}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Code</label>
                       <div class="well" style="background:black">
                        @if ($isFound->new_data !== 'null')
                        <?php $data = json_decode($isFound->new_data)?>
                    
                        @foreach ($data as $key=> $value )
                        <span style="color:#03ff03">{{$key}}</span> <span style="color:white">:</span> <span
                            style="color:red">{{print_r($value,true)}}</span><br>
                        @endforeach
                        @else
                        {{'--'}}
                        @endif
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection