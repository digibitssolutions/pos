@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Log Activities</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th width="50">Type</th>
                                <th>Message</th>
                                <th width="100">Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($logs as $log)
                            <tr>
                                <td>{{$log['id']}}</td>
                                <td>
                                    @if ($log['type'] === 'Error')
                                    <span class="label label-warning">
                                        {{$log['type']}}
                                    </span>
                                    @else
                                    <span class="label label-success">
                                        {{$log['type']}}
                                    </span>
                                    @endif
                                </td>
                                <td class="exception-message">
                                    <a href="{{route('log-detail', $log['id'])}}">
                                        {{$log['message']}}</a></td>
                                <td><b style="color: black">{{$log['created_at']->diffForHumans()}}</b></td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>
</section>

@endsection