@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Recent Activities</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary collapsed-box filters">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('recent-activities')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Client</label>
                                    <select name="client_id" class="form-control select2">
                                        <option value=""> -- Choose an option --</option>
                                        @foreach ($clients as $client)
                                        <option value="{{$client->id}}">{{$client->user->first_name. ' '.$client->user->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Party</label>
                                    <select name="party_id" class="form-control select2">
                                        <option value=""> -- Choose an option --</option>
                                        @foreach ($parties as $party)
                                        <option value="{{$party->id}}">{{$party->party_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="value={{app('request')->input('to')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('recent-activities')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i>
                            Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Relation</th>
                                <th>Created By</th>
                                <th>Message</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($activities as $activity)
                            <tr>
                                <td>{{$activity->id}}</td>
                                <td>{{$activity->relation_id}}</td>
                                <td>{{$activity->user->first_name .' '.$activity->user->last_name}}</td>
                                <td>{{$activity->message}}</td>
                                <td><b class="text-dark">{{$activity->created_at}}</b></td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="text-danger"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$activities->links()}}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection