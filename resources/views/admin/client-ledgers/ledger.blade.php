<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Ledgers List</h3>
        <div class="box-tools">
            <a href="{{route('client-ledger-report', $filters)}}"><i class="fa fa-fw fa-file-pdf-o"
                    style="font-size: 30px;color:red;"></i></a>
        </div>
    </div>

    <div class="box-body table-responsive">
        <table id="" class="table table-hover table-striped table-bordered table-condensed client-ledger-table">
            <thead>
                <tr>
                    <th>Sr.</th>
                    <th>Description</th>
                    <th>Client</th>
                    <th>Amount</th>
                    <th>Recieved</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $totalPayable = 0;
                    $totalRecieved = 0;
                    $total = 0;

                    $totalPayable1 = 0;
                    $totalPaid1 = 0;
                    $total1 = 0;
                ?>
                @forelse($ledgers as $key => $ledger)
                <?php 
                    $total1 += $ledger->payable;
                    $totalPaid1 += $ledger->paid;
                    $totalPayable1 = $total1 - $totalPaid1;
                ?>
                <tr>
                    <td>{{++$key}}</td>
                    <td>
                        @if($ledger->status == 'purchased' || !is_null($ledger->invoice_no))
                        <a href="{{route('client-invoice', $ledger->invoice_no)}}" target="_blank"
                            rel="noopener noreferrer">{{$ledger->description}}</a>
                        @else
                        {{$ledger->description}}
                        @endif
                    </td>
                    <td>
                        <b>{{$ledger->first_name.' '.$ledger->last_name}}</b>
                    </td>
                    <td>{{$ledger->payable}}</td>
                    <td>{{$ledger->paid}}</td>
                    @if($ledger->status == 'paid')
                    <td>{{$key == 1 ? $ledger->payable : $totalPayable1}}</td>
                    @else
                    <td>{{$key == 1 ? $ledger->payable : $ledger->payable+$totalPayable}}</td>
                    @endif
                    <td>{{$ledger->status}}</td>
                    <td>{{$ledger->invoice_date}}</td>
                </tr>
                <?php
                    $total += $ledger->payable;
                    $totalRecieved += $ledger->paid;
                    $totalPayable = $total - $totalRecieved;
                ?>
                @empty
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: red"><b>No Record</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Total</th>
                    <th rowspan="1" colspan="1">{{'Rs. '. $total}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Recieved</th>
                    <th rowspan="1" colspan="1">{{'Rs. '.$totalRecieved}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Recievable</th>
                    <th rowspan="1" colspan="1">{{'Rs. '. $totalPayable}}</th>
                </tr>
            </tfoot>
        </table>

    </div>
    <!-- /.box-body -->
</div>