@extends('admin.layouts.app')
@section('main-content')

<section class="content-header">
    <h1>Statement of Accounts</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('clients-list')}}"><i class="fa fa-list"></i> Client List</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            {{-- <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('products-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Product Name</label>
                                    <input type="text" class="form-control" placeholder="Product Name"
                                        name="product_name" value={{app('request')->input('product_name')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Product Code</label>
                                    <input type="text" class="form-control" placeholder="Product Code" name="code"
                                        value={{app('request')->input('code')}}>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Measurement Unit</label>
                                    <select class="form-control" name="measurement_unit" style="width: 100%;">
                                        <option value=""> --Select Measurement Unit -- </option>
                                        @foreach($measurementUnits as $key => $measurementUnit)
                                        <option value="{{$measurementUnit->id}}">{{$measurementUnit->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Category</label>
                                    <select class="form-control" name="category_id" style="width: 100%;">
                                        <option value=""> --Select Category --</option>
                                        @foreach($categories as $key => $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Exipry Date</label>
                                    <input type="date" class="form-control" placeholder="From Exipry Date"
                                        name="from_expiry_date" value={{app('request')->input('from_expiry_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Exipry Date</label>
                                    <input type="date" class="form-control" placeholder="Until Exipry Date"
                                        name="to_expiry_date" value={{app('request')->input('to_expiry_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" placeholder="From Date" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control" placeholder="To Date" name="to"
                                        value={{app('request')->input('to')}}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('products-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i>
                            Clear</a>
                    </div>
                </form>
            </div> --}}
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Transactions List</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Details</th>
                                <th>Amount</th>
                                <th>Recieved</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($transactions as $key => $transaction)
                            <tr>
                                <td>{{$transaction->transaction_date ? $transaction->transaction_date :
                                    $transaction->created_at}}
                                </td>
                                <td>
                                    @if ($transaction->invoice)
                                    {{ 'INV-'.$transaction->invoice->id.' due on
                                    '.date_format($transaction->invoice->created_at, 'd-M-Y')}}
                                    @else
                                    Ammount Recieved
                                    @endif
                                </td>
                                <td>
                                    @if ($transaction->invoice)
                                    {{$transaction->debit}}
                                    @else
                                    0.0
                                    @endif
                                </td>
                                <td>
                                    @if ($transaction->invoice)
                                    0.0
                                    @else
                                    {{$transaction->credit}}
                                    @endif
                                </td>
                                <td>
                                    @if($transaction->invoice)
                                    {{$transaction->invoice->total_amount}}
                                    @else
                                    {{$transaction->client->total_remaining_balance - $transaction->debit}}
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection