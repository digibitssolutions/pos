@extends('admin.layouts.app')

@section('main-content')



<section class="content-header">
    <h1>Client Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('clients-list')}}"><i class="fa fa-list-ul"></i> Clients List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Client Account Detail</h3>
        </div>
        <form action="{{route('store-client')}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> First Name</label>
                            <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                value="{{old('first_name')}}" autofocus="true" required>

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Middle Name</label>
                            <input type="text" class="form-control" placeholder="Middle Name" name="middle_name"
                                value="{{old('middle_name')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                value="{{old('last_name')}}" required>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email"
                                value="{{old('email')}}">
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password"
                                value="{{old('password')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" placeholder="Confirm Password"
                                name="password_confirmation" value="{{old('password_confirmation')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile </label>
                            <input type="number" class="form-control" placeholder="Mobile" name="mobile"
                                value="{{old('mobile')}}" required>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone </label>
                            <input type="number" class="form-control" placeholder="Phone" name="phone"
                                value="{{old('phone')}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" placeholder="City" name="city"
                                value="{{old('city')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" placeholder="State" name="state"
                                value="{{old('state')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Designation</label>
                            <input type="text" class="form-control" placeholder="Designation" name="designation"
                                value="{{old('designation')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip Code</label>
                            <input type="text" class="form-control" placeholder="Zip Code" name="zip"
                                value="{{old('zip')}}">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Department</label>
                            <input type="text" class="form-control" placeholder="Department" name="department"
                                value="{{old('department')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Website</label>
                            <input type="text" class="form-control" placeholder="Website" name="website"
                                value="{{old('website')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Facebook</label>
                            <input type="text" class="form-control" placeholder="Facebook" name="facebook"
                                value="{{old('facebook')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Twitter</label>
                            <input type="text" class="form-control" placeholder="Twitter" name="twitter"
                                value="{{old('twitter')}}">
                        </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Type </label>
                            <select class="form-control" name="type" required>
                                <option value=""> --Select type--</option>
                                <option value="Individual">Individual</option>
                                <option value="Shop">Shop</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Select Currency </label>
                            <select class="form-control" name="currency">
                                <option value="PKR" selected>PKR</option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>
                                <option value="INR">INR</option>
                                <option value="GBP">GBP</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Address</label>
                            <textarea class="form-control" placeholder="Address.."
                                name="address">{{old('address')}}</textarea>
                        </div>
                    </div>

                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary  "><i class="fa fa-plus"></i> Submit</button>
            </div>
        </form>
    </div>
</section>
@endsection