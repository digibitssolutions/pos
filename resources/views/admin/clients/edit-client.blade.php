@extends('admin.layouts.app')

@section('main-content')



<section class="content-header">
    <h1>Client Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('clients-list')}}"><i class="fa fa-list-ul"></i> Clients List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Client Account Detail</h3>
        </div>
        <form action="{{route('update-client',$client->id)}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> First Name</label>
                            <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                value="{{$client->first_name}}" autofocus="true">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Middle Name</label>
                            <input type="text" class="form-control" placeholder="Middle Name" name="middle_name"
                                value="{{$client->middle_name}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                value="{{$client->last_name}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="text" class="form-control" placeholder="Enter Email" name="email"
                                value="{{$client->email}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    {{-- <div class="col-lg-3">
                        <div class="form-group">
                            <label> Password</label>
                            <input type="password" class="form-control" id="show-pass" placeholder="Enter Password"
                                name="password" maxlength="10" value="">
                            <span class="fa fa-eye" style="float: right;margin: -25px 10px 0 0;cursor: pointer;"
                                onclick="passFunction()"></span>
                        </div>
                    </div> --}}
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile </label>
                            <input type="text" class="form-control" placeholder="Mobile" name="mobile"
                                value="{{$client->mobile}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone </label>
                            <input type="text" class="form-control" placeholder="Phone" name="phone"
                                value="{{$client->mobile}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Department</label>
                            <input type="text" class="form-control" placeholder="Department" name="department"
                                value="{{$client->department}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" placeholder="City" name="city"
                                value="{{$client->city}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" placeholder="State" name="state"
                                value="{{$client->state}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Designation</label>
                            <input type="text" class="form-control" placeholder="Designation" name="designation"
                                value="{{$client->designation}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip Code</label>
                            <input type="text" class="form-control" placeholder="Zip Code" name="zip"
                                value="{{$client->zip}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Website</label>
                            <input type="text" class="form-control" placeholder="Website" name="website"
                                value="{{$client->website}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Facebook</label>
                            <input type="text" class="form-control" placeholder="Facebook" name="facebook"
                                value="{{$client->facebook}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Twitter</label>
                            <input type="text" class="form-control" placeholder="Twitter" name="twitter"
                                value="{{$client->twitter}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Type </label>
                            <select class="form-control" name="type">
                                <option value=""> --Select type--</option>
                                <option value="Individual" {{$client->type == 'Individual' ? 'selected' :
                                    ''}}>Individual
                                </option>
                                <option value="Shop" {{$client->type == 'Shop' ? 'selected' : ''}}>Shop</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Address</label>
                            <textarea class="form-control" placeholder="Address.."
                                name="address">{{$client->address}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
            </div>
        </form>
    </div>
</section>
@endsection