@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Client Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('clients-list')}}"><i class="fa fa-list"></i> Clients Lists</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> First Name</label>
                        <input type="text" class="form-control" value="{{$client->user->first_name}}" readonly>

                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Middle Name</label>
                        <input type="text" class="form-control" value="{{$client->user->middle_name}}" readonly>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Last Name</label>
                        <input type="text" class="form-control" value="{{$client->user->last_name}}" readonly>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Email</label>
                        <input type="email" class="form-control" value="{{$client->user->email}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Mobile </label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->mobile}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Phone </label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->phone}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> City</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->city}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> State</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->state}}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Designation</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->designation}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Zip Code</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->zip}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Department</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->department}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Website</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->website}}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Facebook</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->facebook}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Twitter</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->twitter}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Type</label>
                        <input type="text" class="form-control" value="{{$client->type}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Currency</label>
                        <input type="text" class="form-control" value="{{$client->user->userDetail->currency}}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label> Address</label>
                        <textarea class="form-control" readonly>{{$client->user->userDetail->address}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="{{route('clients-list')}}" class="btn btn-primary  "><i class="fa fa-arrow-left"></i> Go
                Back</a>
        </div>
    </div>
</section>

@endsection