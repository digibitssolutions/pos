@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetags-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Products</span>
                    <span class="info-box-number">760</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-printer-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Invoices</span>
                    <span class="info-box-number">{{$invoices}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Clients</span>
                    <span class="info-box-number">{{$clients}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Suppliers</span>
                    <span class="info-box-number">{{$suppliers}}</span>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-star-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Overall Sales</span>
                    <span class="info-box-number">{{$sales}}</span>
                    <div class="pull-right">
                        <span>Last Month : </span>
                        <span class="text-bold">{{$lastMonthSale}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-teal"><i class="ion ion-ios-calculator-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Overall Profit</span>
                    <span class="info-box-number">{{$totalProfit}}</span>
                    <div class="pull-right">
                        <span>Last Month : </span>
                        <span class="text-bold">{{$lastMonthProf}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

</section>
<!-- /.content -->

@endsection