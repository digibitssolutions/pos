@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Expense Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('expenses-list')}}"><i class="fa fa-list"></i> Expenses List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <form action="{{route('store-expense')}}" method="post">
        @csrf
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Basic Detail</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expense Name</label>
                            <input type="text" class="form-control" name="expense_name" value="{{old('expense_name')}}"
                                placeholder="Expense Name">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expense Type</label>
                            <input type="text" class="form-control" name="expense_type" value="{{old('expense_type')}}"
                                placeholder="Expense Type">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expense Amount</label>
                            <input type="text" class="form-control" name="expense_amount"
                                value="{{old('expense_amount')}}" placeholder="Expense Amount">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expense Date</label>
                            <input type="date" name="expense_date" class="form-control" value="{{old('expense_date')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Expense Note</label>
                            <textarea class="form-control" name="note" rows="5"
                                placeholder="Expense Note">{{old('note')}}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Expense Description</label>
                            <textarea class="form-control" name="description" rows="5"
                                placeholder="Expense Description">{{old('description')}}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Submit</button>
            </div>
        </div>
    </form>
</section>
@endsection