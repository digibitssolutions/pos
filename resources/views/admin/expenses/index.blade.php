@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Expense Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('create-expense')}}"><i class="fa fa-plus"></i> Add New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="#">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" class="form-control" name="product_id">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice No</label>
                                    <input type="text" class="form-control" placeholder="Invoice No" name="invoice_no"
                                        value={{app('request')->input('invoice_no')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice Date</label>
                                    <input type="date" class="form-control product-code" name="invoice_date"
                                        value={{app('request')->input('invoice_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Delivery Date</label>
                                    <input type="date" class="form-control" name="delivery_date"
                                        value={{app('request')->input('delivery_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="value={{app('request')->input('to')}}">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('invoices-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i>Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Expenses Listing</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="example2">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Ex. Name</th>
                                <th>Ex. Type</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($expenses as $key => $expense)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$expense->expense_name}}</td>
                                <td>{{$expense->expense_type}}</td>
                                <td>{{$expense->expense_amount}}</td>
                                <td>{{$expense->expense_date}}</td>
                                <td>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>

        </div>
    </div>
</section>

@endsection