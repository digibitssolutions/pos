<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hamza Traders</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        .company {
            font-family: sans-serif;
            font-size: 12px;
            color: #5B6165;
            margin-top: -10px;
        }

        /* .company h2 {
            margin-top: -30px !important;
        } */

        .company-info {
            color: black;
            font: normal normal 700 30px Times;
            margin: 0px;
        }

        .line {
            width: 240px;
            height: 2px;
            margin: 5px auto;
            border-bottom: 2px solid black;
        }

        .left-div {
            width: 70%;
            float: left;
            font-family: arial, sans-serif;
            margin-left: -6px;
        }

        .left-div p {
            color: black;
            font-size: 11px;
            margin: 3px;
        }

        .left-div .span-1 {
            margin-left: 20px;
        }

        .left-div .span-2 {
            margin-left: 60px;
        }

        .left-div .span-3 {
            margin-left: 35px;
        }

        .right-div {
            width: 30%;
            float: right;
            font-family: arial, sans-serif;
        }

        .right-div p {
            color: black;
            font-size: 11px;
            margin: 3px;
        }

        .right-div .span-1 {
            margin-left: 15px;
        }

        .right-div .span-2 {
            margin-left: 23px;
        }

        .right-div .span-3 {
            margin-left: 15px;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        .table-style {
            margin-top: 70px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            table-layout: auto;
        }

        .table-heading {
            text-align: left;
            padding: 5px;
            font-size: 11px;
            color: black;
            font-family: arial, sans-serif;
        }

        .table-data {
            padding: 5px;
            font-size: 10px;
            border: 1px solid black;
            color: black !important;
            text-transform: uppercase;
        }

        .items {
            color: black;
            font: normal bold 12px arial, sans-serif;
        }

        .footer-right-div {
            width: 40%;
            float: right;
        }

        .footer-right-div p {
            font-family: arial, sans-serif;
            color: #5B6165;
            font-size: 12px;
        }

        .total {
            font-family: arial, sans-serif;
            font-size: 12px;
            color: black;
        }

        .float-right {
            float: right;
        }

        .address-detail,
        .contact-detail {
            font-family: arial, sans-serif;
            font-size: 13px;
            color: black !important;
        }

        .first-part {
            margin-top: 10px;
        }

        .new {
            margin-left: -8px;
            margin-right: -8px;
        }

        .note {
            font-family: arial, sans-serif;
            font-size: 12px;
        }

        .note p {
            margin: 0;
        }
    </style>
</head>

<body>

    <center>
        <div class="company">
            <h2 class="company-info">{{Auth::user()->first_name.' '.Auth::user()->last_name}}</h2>
            <div class="line"></div>
            <span class="address-detail">{{Auth::user()->userDetail->address}}</span><br>
            <span class="contact-detail">
                <label for="mob">Contact:</label>
                <strong id="mob">{{Auth::user()->userDetail->mobile. ',' .Auth::user()->userDetail->phone}}</strong>
            </span>
        </div>
    </center>

    <div class="first-part">
        <div class="left-div">
            <p>Customer Name: <b> <span class="span-1">{{$data['customer_name']}}</span> </b></p>
            <p>Address: <span class="span-2">{{$data['city']}}</span></p>
            <p>Sales Person: <span class="span-3">{{$data['sales_person']}}</span> </p>
        </div>

        <div class="right-div">
            <p>Inv No: <b> <span class="span-1">{{'#'.$data['invoice_no']}}</span> </b> </p>
            <p>Date: <b> <span class="span-2">{{$data['invoice_date']}}</span> </b> </p>
            <p>Mobile: <span class="span-3">{{$data['mobile']}}</span></p>
        </div>
    </div>


    <div class="new">
        <table class="table table-style table-bordered">
            <thead>
                <tr>
                    <th class="table-heading">Sr #</th>
                    <th class="table-heading">Product Name</th>
                    <th class="table-heading">Quantity</th>
                    <th class="table-heading">Price</th>
                    <th class="table-heading">Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $key =>$product)
                <tr>
                    <td class="table-data"><b>{{++$key}}</b></td>
                    <td class="table-data"><b>{{$product->item_details}}</b></td>
                    <td class="table-data"><b>{{$product->quantity}}</b></td>
                    <td class="table-data"><b>{{$product->price}}</b></td>
                    <td class="table-data"><b>{{$product->amount}}</b></td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="footer-right-div">
            <p> <b class="total">Total: {{$data['total_amount']}} </b></p>
            <p> <b class="total">Previous Balance: {{$data['previousBalance']}} </b></p>
            <p> <b class="total">Net Amount: {{$data['netAmount']}}</b></p>
        </div>

        <p class="items">Total Items : {{$data['invoiceItems']}}</p>
        <p class="items">Total Pieces : {{$data['itemPieces']}}</p>

        <div class="note">
            <p><b>Note: </b></p>
            <p>No Expiry, No Damage</p>
            <p>No Replacement for Imported Items</p>
        </div>

    </div>

</body>

</html>