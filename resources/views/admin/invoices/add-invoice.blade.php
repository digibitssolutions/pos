@extends('admin.layouts.app')

@section('extra-js-files')
    <script src="{{ asset('js/invoice.js') }}"></script>
@endsection

@section('main-content')

<section class="content-header">
    <h1>Invoice Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('invoices-list')}}"><i class="fa fa-list"></i> Invoices List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <p id="message"></p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form action="{{route('store-invoice')}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Customer Name <strong class="text-danger">*</strong></label>
                            <select class="form-control select2" name="client_id"
                                id="customer" value="{{old('client_id')}}" required>
                                <option value=""> -- Select Customer -- </option>
                                @foreach($customers as $key => $customer)
                                <option value="{{$customer->id}}">{{$customer->user->first_name}} {{$customer->user->last_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice No</label>
                            <input type="text" class="form-control input-sm" name="invoice_no" id="invoice_no" readonly
                                value="{{$invoice}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice Date</label>
                            <input type="date" class="form-control input-sm" name="invoice_date" id="invoice_date"
                                value="{{date('Y-m-d', strtotime('now'))}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Delivery Date</label>
                            <input type="date" class="form-control input-sm" name="delivery_date" id="delivery_date"
                                value="{{date('Y-m-d', strtotime('now + 1day'))}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Payment Type <strong class="text-danger">*</strong></label>
                            <select class="form-control input-sm" name="payment_type" required>
                                <option value=""> -- Select Payment Type -- </option>
                                <option value="cash">Cash</option>
                                <option value="credit">Credit</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Subject</label>
                            <input type="text" class="form-control input-sm" placeholder="Subject" name="subject" id="subject"
                                value="{{old('subject')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Sales Person</label>
                            <select class="form-control input-sm select2" placeholder="Sales Person" name="sale_person_id"
                                id="customer" value="{{old('sale_person_id')}}">
                                <option value="" selected disabled> -- Select Sales Person -- </option>
                                @foreach($salePersons as $key => $salePerson)
                                <option value="{{$salePerson->id}}">{{$salePerson->user->first_name}}
                                    {{$salePerson->user->last_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label> Recent Price </label>
                        <input class="form-control" type="text" id="last-product" readonly>
                    </div>
                    <div class="col-lg-3">
                        <label for="switch">Enable/Disable</label>
                        <br>
                        <label class="switch invoice-switch">
                            <input type="checkbox" id="switch">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>

                <!-- /.box-header -->
                <h3 class="box-title">Invoice Products</h3>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="bg-dark">
                                <th width="400">P.CODE</th>
                                <th width="200">ITEM DETAILS</th>
                                <th>STOCK</th>
                                <th>PRICE</th>
                                <th>QTY.</th>
                                <th>AMOUNT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody class="tbody">
                            <tr id="product">
                                <td>
                                    <select class="form-control input-sm select2" id="invoice-product">
                                        <option> -- Select Product --</option>
                                        @foreach($products as $key => $product)
                                        <option value="{{$product->id}}">{{$product->product_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm invoice-product-name" id="item_details">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm" value="{{0}}"
                                        id="invoice-product-master-stock" readonly>
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-product-price" id="price">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-product-quantity" id="quantity"
                                        value="1">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-product-amount" id="amount"
                                        value="0">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success" id="add-product" disabled="true"><i
                                            class="fa fa-plus"></i> Add</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-body">
                <h3 class="box-title">Reciept</h3>
                <div class="table-responsive">
                    <table class="table table-bordered" id="receipt_bill">
                        <thead class="bg-success">
                            <tr>
                                <th>No.</th>
                                <th>ITEM DETAILS</th>
                                <th>PRICE</th>
                                <th>QTY.</th>
                                <th>AMOUNT</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="tbody" id="new">
                        </tbody>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td class="text-right text-dark">
                                <h5><strong>Sub Total: </strong></h5>
                                <p><strong>Tax (%) : </strong></p>
                            </td>
                            <td class="text-center text-dark">
                                <h5> <strong><span id="subTotal">0.0</strong></h5>
                                <h5> <strong><span id="taxAmount">0.0</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td class="text-right bg-danger">
                                <h5><strong>Gross Total: </strong></h5>
                                <input type="hidden" name="total_amount" id="invoice-total-amount">
                            </td>
                            <td class="text-center text-danger bg-danger" colspan="2">
                                <h5 id="totalPayment" style="font-size:20px;font-weight: 700"><strong> 0.0</strong></h5>
                            </td>
                        </tr>
                    </table>
                    <button type="submit" class="btn btn-success" id="invoice-save-btn" disabled="true"><i
                            class="fa fa-plus"></i>
                        Save</button>
                </div>
                <!-- /.box-body -->
            </div>
        </form>
    </div>
</section>
@endsection