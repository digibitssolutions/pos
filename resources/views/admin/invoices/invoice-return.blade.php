@extends('admin.layouts.app')

@section('extra-js-files')
<script src="{{asset('js/invoice-return.js')}}"></script>
@endsection

@section('main-content')

<section class="content-header">
    <h1>Invoice Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('invoices-list')}}"><i class="fa fa-list"></i> Invoices List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <form action="{{route('store-invoice-return')}}" method="post">
        @csrf
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Basic Detail</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label> Customer Name <strong class="text-danger">*</strong></label>
                            <select class="form-control select2" name="client_id" id="customer"
                                value="{{old('client_id')}}" - required>
                                <option value=""> -- Select Customer -- </option>
                                @foreach($clients as $key => $client)
                                <option value="{{$client->id}}">{{$client->user->first_name}}
                                    {{$client->user->last_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label> Invoice No</label>
                            <input type="text" class="form-control input-sm" name="invoice_no" id="invoice_no" readonly
                                value="{{$invoiceNo}}">
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label> Invoice Date</label>
                            <input type="date" class="form-control input-sm" name="invoice_date" id="invoice_date"
                                value="{{date('Y-m-d', strtotime('now'))}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" name="subject"
                                placeholder="Subject here...">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="sale_person">Sale Person</label>
                            <select id="sale_person" class="form-control select2" name="sale_person" required>
                                <option value="" selected> -- Select an Option -- </option>
                                @foreach ($salePersons as $sp)
                                <option value="{{$sp->id}}">{{$sp->user->first_name. ' '. $sp->user->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <h3 class="box-title">Invoice Products</h3>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="bg-dark">
                                <th width="350">P.CODE</th>
                                <th width="200">ITEM DETAILS</th>
                                <th>STOCK</th>
                                <th>PRICE</th>
                                <th>QTY.</th>
                                <th>AMOUNT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody class="tbody">
                            <tr id="product">
                                <td>
                                    <select class="form-control input-sm select2" id="invoice-return-product">
                                        <option disabled selected> -- Select Product --</option>
                                        @foreach($products as $key => $product)
                                        <option value="{{$product->id}}">{{$product->product_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm invoice-product-name"
                                        id="item_details">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm" value="{{0}}"
                                        id="invoice-return-master-stock" readonly>
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-product-price" id="price">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-return-quantity"
                                        id="quantity" value="1">
                                </td>
                                <td>
                                    <input type="number" class="form-control input-sm invoice-product-amount"
                                        id="amount" value="0">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary" id="return-product" disabled><i
                                            class="fa fa-plus"></i> Add</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">
                    <h3 class="box-title pull-left">Reciept</h3>
                    <span class="pull-right">
                        <label for="switch">Enable/Disable</label>
                        <br>
                        <label class="switch return-switch">
                            <input type="checkbox" id="switch">
                            <span class="slider round"></span>
                        </label>
                    </span>
                </div>
                <form action="{{route('store-invoice-return')}}" method="POST" class="inv-return-form">
                    @csrf
                    <div class="table-responsive">
                        <table class="table table-bordered" id="receipt_bill">
                            <thead class="bg-success">
                                <tr>
                                    <th>No.</th>
                                    <th>ITEM DETAILS</th>
                                    <th>PRICE</th>
                                    <th>QTY.</th>
                                    <th>AMOUNT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <input type="hidden" name="client_id" class="set-client">
                            <input type="hidden" name="invoice_date" class="set-invoice-date">
                            <tbody class="tbody" id="new">
                            </tbody>
                            <tr>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td class="text-right text-dark">
                                    <h5><strong>Sub Total: </strong></h5>
                                    <p><strong>Tax (%) : </strong></p>
                                </td>
                                <td class="text-center text-dark">
                                    <h5> <strong><span id="subTotal">0.0</strong></h5>
                                    <h5> <strong><span id="taxAmount">0.0</strong></h5>
                                </td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td class="text-right bg-danger">
                                    <h5><strong>Gross Total: </strong></h5>
                                    <input type="hidden" name="total_amount" id="invoice-total-amount">
                                </td>
                                <td class="text-center text-danger bg-danger" colspan="2">
                                    <h5 id="totalPayment" style="font-size: 20px;font-weight:700;"><strong> 0.0</strong>
                                    </h5>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-success" id="return-save-btn" disabled><i
                                class="fa fa-plus"></i>
                            Save</button>
                    </div>
                </form>
            </div>
        </div>
    </form>
</section>
@endsection