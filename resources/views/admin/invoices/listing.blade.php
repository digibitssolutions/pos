@extends('admin.layouts.app')

@section('extra-js-files')
    <script src="{{ asset('js/invoice.js') }}"></script>
@endsection

@section('main-content')

<section class="content-header">
    <h1>Invoices</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('create-invoice')}}"><i class="fa fa-plus"></i> Add New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary filters collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('invoices-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" class="form-control" name="product_id">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="client_id"> Customer Name</label>
                                    <select class="select2" name="client_id">
                                        <option value=""> -- Select Customer --</option>
                                        @foreach($clients as $key => $client)
                                        <option value="{{$client->id}}">{{$client->user->first_name.'
                                            '.$client->user->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice No</label>
                                    <input type="text" class="form-control" placeholder="Invoice No" name="invoice_no"
                                        value={{app('request')->input('invoice_no')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice Date</label>
                                    <input type="date" class="form-control product-code" name="invoice_date"
                                        value={{app('request')->input('invoice_date')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Delivery Date</label>
                                    <input type="date" class="form-control" name="delivery_date"
                                        value={{app('request')->input('delivery_date')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="value={{app('request')->input('to')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('invoices-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Invoice Listing</h3>
                    <div class="box-tools">
                        <a class="btn-outlined success" href="{{route('invoice-return')}}" target="_blank"><i class="fa fa-plus"></i> Add Return</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Customer</th>
                                <th>Invoice No</th>
                                <th>Amount</th>
                                <th>Invoice Date</th>
                                <th>Print</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($invoices as $key => $invoice)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$invoice->client->user->first_name.' '.$invoice->client->user->last_name}}</td>
                                <td>{{$invoice->invoice_no}}</td>
                                <td>{{$invoice->total_amount}}</td>
                                <td>{{$invoice->invoice_date}}</td>
                                <td>
                                    <a href="javascript:;" data-id="{{$invoice->id}}" class="print-invoice">
                                        <i class="fa fa-fw fa-print" style="font-size:30px;"></i>
                                    </a>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenu1" style="color: #fff;background-color: #29549E;"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-fw fa-bolt"></i>
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li>
                                                <a href="{{route('single-invoice-detail',$invoice->id)}}"><strong
                                                        class="text-primary">Show</strong></a>
                                            </li>
                                            <li>
                                                <a href="{{route('print-invoice',$invoice->id)}}"><strong
                                                        class="text-warning">Download</strong></a>
                                            </li>
                                            <li>
                                                <a href="" onclick="if(confirm('Are your sure you want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-{{$invoice->id}}').submit();
                                                        } else {
                                                        event.preventDefault();
                                                        }">
                                                    <strong class="text-dark">Delete</strong>
                                                </a>
                                                <form id="delete-{{$invoice->id}}"
                                                    action="{{route('delete-invoice',$invoice->id)}}" method="POST"
                                                    hidden>
                                                    @csrf
                                                    @method('delete')
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$invoices->links()}}
                    </div>
                </div>
                <!-- /.box-body -->

            </div>

        </div>
    </div>
</section>

@endsection