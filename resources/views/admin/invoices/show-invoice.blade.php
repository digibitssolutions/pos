@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Invoice Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('invoices-list')}}"><i class="fa fa-cubes"></i> Invoices List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <p id="message"></p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Customer Name</label>
                        <input type="text" class="form-control" readonly
                            value="{{$invoice->client->user->first_name.' '.$invoice->client->user->last_name}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Invoice No</label>
                        <input type="text" class="form-control" readonly value="{{$invoice->invoice_no}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Invoice Date</label>
                        <input type="text" class="form-control" readonly value="{{$invoice->invoice_date}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Delivery Date</label>
                        <input type="date" class="form-control" readonly value="{{$invoice->delivery_date}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label> Subject</label>
                        <input type="text" class="form-control" placeholder="Subject" readonly
                            value="{{$invoice->subject}}">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Sale Person</label>
                        <input type="text" class="form-control" readonly
                            value="{{$invoice->salePerson ? $invoice->salePerson->user->first_name.' '.$invoice->salePerson->user->last_name : '--'}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <!-- /.box-header -->
            <h3 class="box-title">Invoice Products</h3>
            <div class="box-body table-responsive">
                <table class="table table-hover" id="receipt_bill">
                    <thead class="bg-success">
                        <tr>
                            <th>No.</th>
                            <th>ITEM DETAILS</th>
                            <th>QTY.</th>
                            <th>PRICE</th>
                            <th>DISC</th>
                            <th>TAX</th>
                            <th>AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        @foreach($invoiceProducts as $key => $invoiceProduct)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$invoiceProduct->item_details}}</td>
                            <td>{{$invoiceProduct->quantity}}</td>
                            <td>{{$invoiceProduct->price}}</td>
                            <td>{{$invoiceProduct->discount}}</td>
                            <td>{{$invoiceProduct->tax}}</td>
                            <td>{{$invoiceProduct->amount}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tr class="mt-5">
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="text-right bg-danger">
                            <h5><strong>Gross Total: </strong></h5>
                            <input type="hidden" name="total_amount" id="invoice-total-amount">
                        </td>
                        <td class="text-center text-danger bg-danger">
                            <h5 id="totalPayment" style="font-size:20px;font-weight: 700">
                                <strong>{{$invoice->total_amount}}</strong>
                            </h5>
                        </td>
                    </tr>
                </table>
                <a href="{{route('invoices-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
@endsection