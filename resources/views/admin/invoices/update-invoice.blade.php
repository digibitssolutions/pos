@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Invoices Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('invoices-list')}}"><i class="fa fa-list"></i> Invoice Lists</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <form action="{{route('update-invoice-record', $invoice->id)}}" method="post">
        @csrf
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Basic Detail</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Customer Name</label>
                            <select class="form-control select2" placeholder="Customer Name" name="client_id"
                                value="{{old('client_id')}}" autofocus="true">
                                <option value=""> -- Select Customer -- </option>
                                @foreach($clients as $key => $client)
                                <option value="{{$client->id}}" {{$client->id == $invoice->client_id ?
                                    'selected' :
                                    ''}}>{{$client->first_name}} {{$client->last_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice no.</label>
                            <input type="text" class="form-control" placeholder="Invoice no." name="invoice_no" readonly
                                value="{{$invoice->invoice_no}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice Date</label>
                            <input type="date" class="form-control" placeholder="Invoice Date" name="invoice_date"
                                value="{{$invoice->invoice_date}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Delivery Date</label>
                            <input type="date" class="form-control" placeholder="Delivery Date" name="delivery_date"
                                value="{{$invoice->delivery_date}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Payment Type</label>
                            <select class="form-control" name="payment_type">
                                <option value=""> -- Select Payment Type -- </option>
                                <option value="cash" {{$invoice->payment_type == 'cash' ? 'selected' :
                                    ''}}>Cash</option>
                                <option value="credit" {{$invoice->payment_type == 'credit' ? 'selected' :
                                    ''}}>Credit</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label> Subject</label>
                        <input type="text" class="form-control" placeholder="Subject" name="subject"
                            value="{{$invoice->subject}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Sales Person</label>
                            <select class="form-control select2" placeholder="Sales Person" name="sale_person_id"
                                value="{{old('sale_person_id')}}">
                                <option value=""> -- Select Sales Person -- </option>
                                @foreach($salePersons as $key => $salePerson)
                                <option value="{{$salePerson->id}}" {{$salePerson->id ==
                                    $invoice->sale_person_id ?
                                    'selected' : ''}}>
                                    {{$salePerson->first_name}} {{$salePerson->last_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="bg-dark">
                                <th>P.CODE</th>
                                <th>ITEM DETAILS</th>
                                <th>PRICE</th>
                                <th>QTY.</th>
                                <th>STOCK</th>
                                <th>AMOUNT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody class="tbody">
                            <tr id="product">
                                <td>
                                    <select class="form-control select2 update-invoice-product">
                                        <option> -- Select Product -- </option>
                                        @foreach($products as $key => $product)
                                        <option value="{{$product->id}}">{{$product->product_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control invoice-product-name-1" id="item_details">
                                </td>
                                <td>
                                    <input type="number" class="form-control invoice-product-price-1" id="price">
                                </td>
                                <td>
                                    <input type="number" class="form-control invoice-product-quantity-1" id="quantity"
                                        value="1">
                                </td>
                                <td>
                                    <input type="number" class="form-control invoice-product-master-stock-1"
                                        value="{{0}}" readonly>
                                </td>
                                <td>
                                    <input type="number" class="form-control invoice-product-amount-1" id="amount"
                                        value="0">
                                </td>
                                <td>
                                    <button type="button" disabled class="btn btn-primary add-product-1"><i
                                            class="fa fa-plus"></i>
                                        Add</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="box-title">Invoice Products</h3>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-success">
                            <th>P.Code</th>
                            <th>Item Details</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </thead>
                        <tbody class="tbody new receipt_bill">
                            @foreach($invoiceProducts as $key => $invoiceProduct)
                            <tr id="product">
                                <td>
                                    <select class="form-control select2 update-invoice-product-select"
                                        id="update-invoice-product" name="product_id[]">
                                        <option> -- Select Product -- </option>
                                        @foreach($products as $key => $product)
                                        <option value="{{$product->id}}" {{$invoiceProduct->product_id ==
                                            $product->id ?
                                            'selected' : ''}}>
                                            {{$product->product_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control invoice-product-name" name="item_details[]"
                                        value="{{$invoiceProduct->item_details}}">
                                </td>
                                <td>
                                    <input type="number" min="1" class="form-control invoice-product-price"
                                        name="price[]" value="{{$invoiceProduct->price}}">
                                </td>
                                <td>
                                    <input type="number" min="1" class="form-control invoice-product-quantity"
                                        name="quantity[]" value="{{$invoiceProduct->quantity}}">
                                </td>
                                <td>
                                    <input type="number" class="form-control invoice-product-amount total"
                                        name="amount[]" value="{{$invoiceProduct->amount}}">
                                </td>
                                <td>
                                    <a class="remove-id" data-id={{$invoiceProduct->id}} type="submit">
                                        <i class="fa fa-times remove" style="cursor:pointer;"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td class="text-right text-dark">
                                <h5><strong>Sub Total: </strong></h5>
                                <p><strong>Tax (%) : </strong></p>
                            </td>
                            <td class="text-center text-dark">
                                <h5> <strong><span class="subTotal"></strong></h5>
                                <h5> <strong><span id="taxAmount">0.0</strong></h5>
                            </td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td class="text-right bg-danger">
                                <h5><strong>Gross Total: </strong></h5>
                                <input type="hidden" name="total_amount" class="invoice-total-amount"
                                    value="{{$invoice->total_amount}}">
                            </td>
                            <td class="text-center text-danger bg-danger">
                                <h5 class="totalPayment" style="font-size:20px;font-weight: 700"><strong>
                                    </strong></h5>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box-footer">
                <a href="{{route('invoices-list')}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm">Update <i class="fa fa-refresh"></i></button>
            </div>
        </div>
    </form>
</section>
@endsection