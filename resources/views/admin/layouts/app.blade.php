<!DOCTYPE html>
<html>

<head>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Hamza Traders | Dashboard</title>
  <link rel="shortcut icon" href="{{asset('admin/images/hamza-traders-logos.jpeg')}}" type="image/x-icon">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @include('admin.layouts.header')

  

</head>

<body class="skin-blue sidebar-mini fixed">

  <div class="wrapper">

    @include('admin.layouts.navbar')

    @include('admin.layouts.sidebar')

    <div class="content-wrapper">

      @section('main-content')
      @show

    </div> 

    @include('admin.layouts.footer')

  </div>
  
</body>

</html>