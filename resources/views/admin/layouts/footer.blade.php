<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <p>Developed and Powered by BetterServe Technology</p>
    </div>
    <strong>Copyright © (
        <?php echo date('Y'); ?>) BetterServe All rights reserved.
    </strong>
</footer>

<!-- jQuery 2.2.3 -->
<script src="{{ asset('admin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('admin/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin/dist/js/demo.js') }}"></script>

<!-- Js Files -->
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/intake-stock.js') }}"></script>
<script src="{{ asset('js/client.js') }}"></script>
<script src="{{ asset('js/party.js') }}"></script>
<script src="{{ asset('js/sale-person.js') }}"></script>
<script src="{{ asset('js/ledger.js') }}"></script>
<script src="{{ asset('js/client-ledger.js') }}"></script>
<script src="{{ asset('js/sale-person-ledger.js') }}"></script>

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    //Initialize Select2 Elements
    $('.select2').select2();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength": 25
    });
</script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>

<script>
    function hideShow() {
        if (window.innerWidth >= 768) {
            var abc = $(".filters").hasClass('collapsed-box');
            if (abc) {
                $(".filters").removeClass('collapsed-box');
            }
        } else {
            var abc = $(".filters").hasClass('collapsed-box');
            if (!abc) {
                $(".filters").addClass('collapsed-box');
            }
        }
    }

    $(document).load(hideShow());
</script>

@section('extra-js-files')
@show
