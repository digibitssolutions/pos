<aside class="main-sidebar">

  <section class="sidebar">

    {{-- <div class="user-panel">
      <div class="pull-left info">
        <p>@if (Auth::check()) {{Auth::user()->first_name}} @endif</p>
    </div>
    <br><br>
    </div> --}}

    <ul class="sidebar-menu">
      <li class="treeview <?php if(Request::segment(1)=='dashboard'){echo 'active menu-open'; } ?>">
        <a href="{{route('dashboard')}}">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li
        class="treeview
      <?php if(Request::segment(1)== 'users-list' || Request::segment(2)== 'clients-list'  || Request::segment(2)== 'parties-list' || Request::segment(2)== 'sales-person-list'){print 'active'; }?>">

        <a href="#">
          <i class="fa fa-fw fa-users"></i>
          <span>People</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li class="<?php if(Request::segment(1)=='users-list'){echo 'active menu-open'; } ?>"><a
              href="{{route('users-list')}}"><i class="fa fa-circle-o"></i> Users</a></li>

          <li class="treeview <?php if(Request::segment(2)== 'clients-list' ){print 'active menu-open'; } ?>">
            <a href="{{route('clients-list')}}"><i class="fa fa-circle-o"></i> Clients</a>
          </li>
          <li class="<?php if(Request::segment(2)== 'parties-list' ){print 'active menu-open'; } ?>"><a
              href="{{route('parties-list')}}"><i class="fa fa-circle-o"></i> Suppliers</a></li>
          <li class="<?php if(Request::segment(2)== 'sales-person-list' ){print 'active menu-open'; } ?>"><a
              href="{{route('sales-person-list')}}"><i class="fa fa-circle-o"></i> Sales Person</a></li>
        </ul>

      </li>
      <li class="treeview <?php if(Request::segment(2)=='products-list'){echo 'active menu-open'; } ?>">
        <a href="{{route('products-list')}}">
          <i class="fa fa-cubes"></i>
          <span>Products</span>
        </a>
      </li>
      <li class="treeview <?php if(Request::segment(2)=='create-stock-intake'){echo 'active menu-open'; } ?>">
        <a href="{{route('create-stock-intake')}}">
          <i class="fa fa-barcode"></i>
          <span>Stock Intake</span>
        </a>
      </li>
      <li class="treeview <?php if(Request::segment(2)=='invoices-list'){echo 'active menu-open'; } ?>">
        <a href="{{route('invoices-list')}}">
          <i class="fa fa-fw fa-print"></i>
          <span>Invoices</span>
        </a>
      </li>
      {{-- <li class="treeview <?php if(Request::segment(2)=='sales-list'){echo 'active menu-open'; } ?>">
        <a href=" {{route('sales-list')}}">
          <i class="fa fa-fw fa-dollar"></i>
          <span>Sales</span>
        </a>
      </li> --}}
      <li class="treeview <?php if(Request::segment(2)=='reports'){echo 'active menu-open'; } ?>"">
              <a href=" {{route('reports')}}">
        <i class="fa fa-fw fa-pie-chart"></i>
        <span>Reports</span>
        </a>
      </li>
      <?php
        use App\Http\Controllers\ShortStock\ShortStockController;
        $shortStocks = ShortStockController::shortStockNotify();
      ?>
      <li class="treeview <?php if(Request::segment(2)=='short-stock'){echo 'active menu-open'; } ?>"">
        <a href=" {{route('short-stock')}}">
        <i class="fa fa-fw fa-bar-chart-o"></i>
        <span>Short Stock</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-yellow">{{$shortStocks}}</small>
        </span>
        </a>
      </li>

      <li class="treeview <?php if(Request::segment(2)=='ledgers-list'){echo 'active menu-open'; } ?>"">
        <a href=" {{route('ledgers-list')}}">
        <i class="fa fa-balance-scale"></i>
        <span>Purchase Ledger</span>
        <span class="pull-right-container">
          <i class="fa fa-circle-o text-aqua pull-right"></i>
        </span>
        </a>
      </li>

      <li class="treeview <?php if(Request::segment(2)=='client-ledgers-list'){echo 'active menu-open'; } ?>"">
        <a href=" {{route('client-ledgers')}}">
        <i class="fa fa-book"></i>
        <span>Client Ledger</span>
        <span class="pull-right-container">
          <i class="fa fa-circle-o text-aqua pull-right"></i>
        </span>
        </a>
      </li>

      <li class="treeview <?php if(Request::segment(2)=='sale-person-ledger'){echo 'active menu-open'; } ?>"">
        <a href=" {{route('sale-person-ledger')}}">
        <i class="fa fa-area-chart"></i>
        <span>Sale Person Ledger</span>
        <span class="pull-right-container">
          <i class="fa fa-circle-o text-aqua pull-right"></i>
        </span>
        </a>
      </li>

      <li
        class="treeview <?php if(Request::segment(2)=='log-activities' || Request::segment(2) == 'recent-activities'){echo 'active menu-open'; } ?>">
        <a href="#">
          <i class="fa fa-cogs"></i> <span>Settings</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if(Request::segment(2)=='log-activities'){echo 'active'; } ?>">
            <a href=" {{route('log-activities')}}">
              <i class="fa fa-fw fa-500px"></i>
              <span>Log Activities</span>
            </a>
          </li>
          <li class="<?php if(Request::segment(2)=='recent-activities'){echo 'active'; } ?>">
            <a href="{{route('recent-activities')}}">
              <i class="fa fa-fw fa-unlock"></i>
              <span>Recent Activities</span>
            </a>
          </li>
        </ul>
      </li> --}}
    </ul>
  </section>
</aside>