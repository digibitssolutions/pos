@extends('admin.layouts.app')

@section('main-content')



<section class="content-header">
    <h1>Party Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('parties-list')}}"><i class="fa fa-list-ul"></i> Parties List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Party Account Detail</h3>
        </div>
        <form action="{{route('store-party')}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Party Name</label>
                            <input type="text" class="form-control" placeholder="Party Name" name="party_name" required
                                value="{{old('party_name')}}" autofocus="true">

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email"
                                value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Type </label>
                            <select class="form-control" name="type" required>
                                <option value=""> --Select type--</option>
                                <option value="individual">Individual</option>
                                <option value="shop">Shop</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile </label>
                            <input type="text" class="form-control" placeholder="Mobile" name="mobile"
                                value="{{old('mobile')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone </label>
                            <input type="text" class="form-control" placeholder="Phone" name="phone"
                                value="{{old('phone')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" placeholder="City" name="city"
                                value="{{old('city')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" placeholder="State" name="state"
                                value="{{old('state')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip Code</label>
                            <input type="text" class="form-control" placeholder="Zip Code" name="zip"
                                value="{{old('zip')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Address</label>
                            <textarea class="form-control" placeholder="Address.."
                                name="address">{{old('address')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary  "><i class="fa fa-plus"></i> Submit</button>
            </div>
        </form>
    </div>
</section>
@endsection