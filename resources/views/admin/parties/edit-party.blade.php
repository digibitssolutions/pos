@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Party Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('parties-list')}}"><i class="fa fa-list"></i> Parties Lists</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form action="{{route('update-party',$party->id)}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Name</label>
                            <input type="text" class="form-control" name="party_name" value="{{$party->party_name}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="text" class="form-control" name="email" value="{{$party->email}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Type</label>
                            <select class="form-control" name="type">
                                <option value=""> --Select type--</option>
                                <option value="individual" {{$party->type == 'individual' ? 'selected' : ''}}>Individual
                                </option>
                                <option value="shop" {{$party->type == 'shop' ? 'selected' : ''}}>Shop</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{$party->phone}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile</label>
                            <input type="text" class="form-control" name="mobile" value="{{$party->mobile}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" name="city" value="{{$party->city}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip</label>
                            <input type="text" class="form-control" name="zip" value="{{$party->zip}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" name="state" value="{{$party->state}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" name="address" rows="5">{{$party->address}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
            </div>
        </form>
    </div>
</section>

@endsection