@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Party Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('parties-list')}}"><i class="fa fa-list"></i> Parties Lists</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Name</label>
                            <input type="text" class="form-control" value="{{$party->party_name}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="text" class="form-control" value="{{$party->email}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Type</label>
                            <input type="text" class="form-control" value="{{$party->type}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone</label>
                            <input type="text" class="form-control" value="{{$party->phone}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile</label>
                            <input type="text" class="form-control" value="{{$party->mobile}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" value="{{$party->city}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip</label>
                            <input type="text" class="form-control" value="{{$party->zip}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" value="{{$party->state}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" rows="5">{{$party->address}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{route('parties-list')}}" class="btn btn-primary  "><i class="fa fa-arrow-left"></i> Go
                    Back</a>
            </div>
        </form>
    </div>
</section>

@endsection