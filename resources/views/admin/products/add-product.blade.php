@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Product Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('products-list')}}"><i class="fa fa-list"></i> Products Lists</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form action="{{route('store-product')}}" method="post">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Name</label>
                            <input type="text" class="form-control" placeholder="Product Name" name="product_name"
                                value="{{old('product_name')}}" autofocus="true">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Code</label>
                            <input type="text" class="form-control" name="code" placeholder="Product Code"
                                value="{{old('code')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Color</label>
                            <input type="text" class="form-control" placeholder="Color" name="varient"
                                value="{{old('varient')}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expiry Date</label>
                            <input type="date" class="form-control" placeholder="Expiry Date" name="expiry_date"
                                value="{{old('expiry_date')}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Units Detail</h3>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Group</label>
                            <select class="form-control" name="group_id" style="width: 100%;">
                                <option value=""> --Select group--</option>
                                @foreach($groups as $key => $group)
                                <option value="{{$group->id}}">{{$group->group_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Category</label>
                            <select class="form-control" style="width: 100%;" name="category_id">
                                <option value=""> --Select category--</option>
                                @foreach($categories as $key => $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Measurement Unit</label>
                            <select class="form-control" style="width: 100%;" name="measurement_unit">
                                <option value=""> --Select Measurement Unit--</option>
                                @foreach($measurementUnits as $key => $measurementUnit)
                                <option value="{{$measurementUnit->id}}">{{$measurementUnit->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Prices Detail</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Unit Price </label>
                            <input type="text" class="form-control" name="unit_price" placeholder="0.00"
                                value="{{old('unit_price')}}">
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Purchase Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="purchase_price"
                                value="{{old('purchase_price')}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Sale Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="sale_price"
                                value="{{old('sale_price')}}">
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Special Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="special_price"
                                value="{{old('special_price')}}">
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Discount </label>
                            <input type="text" class="form-control" placeholder="0.00" name="discount"
                                value="{{old('discount')}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Tax Detail</h3>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax </label>
                            <input type="text" class="form-control" name="tax" placeholder="0.00"
                                value="{{old('tax')}}">
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax Type </label>
                            <select class="form-control" style="width: 100%;" name="tax_type"
                                value="{{old('tax_type')}}">
                                <option value=""> --Select Tax Type--</option>
                                <option value="Included">Included</option>
                                <option value="Excluded">Excluded</option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Description</label>
                            <textarea class="form-control" name="short_description" value="{{old('short_description')}}"
                                placeholder="Description..."></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary  "><i class="fa fa-plus"></i> Submit</button>
            </div>
        </form>
    </div>
</section>
@endsection