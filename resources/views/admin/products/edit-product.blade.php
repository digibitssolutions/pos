@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Product Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('products-list')}}"><i class="fa fa-list"></i> Products Lists</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form action="{{route('update-product', $product->id)}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <input type="hidden" name="id">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Name</label>
                            <input type="text" class="form-control" placeholder="Product Name" name="product_name"
                                value="{{$product->product_name}}" autofocus="true">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Code</label>
                            <input type="text" class="form-control" placeholder="Product Code" name="code"
                                value="{{$product->code}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Color</label>
                            <input type="text" class="form-control" placeholder="Color" name="varient"
                                value="{{$product->varient}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expiry Date</label>
                            <input type="date" class="form-control" placeholder="Expiry Date" name="expiry_date"
                                value="{{$product->expiry_date}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Units Detail</h3>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Group</label>
                            <select class="form-control" name="group_id" style="width: 100%;">
                                @foreach($groups as $key => $group)
                                <option value="{{$group->id}}" {{$group->id == $product->group_id ? 'selected' :
                                    '--'}}>{{$group->group_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Category</label>
                            <select class="form-control" name="category_id" style="width: 100%;">
                                @foreach($categories as $key => $category)
                                <option value="{{$category->id}}" {{$category->id == $product->category_id ? 'selected'
                                    : '--'}}>{{$category->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Select Measurement Unit</label>
                            <select class="form-control" name="measurement_unit" style="width: 100%;">
                                @foreach($measurementUnits as $key => $measurementUnit)
                                <option value="{{$measurementUnit->id}}" {{$measurementUnit->id ==
                                    $product->measurement_unit_id ? 'selected'
                                    : '--'}}>{{$measurementUnit->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Prices Detail</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Unit Price </label>
                            <input type="text" class="form-control" name="unit_price" placeholder="0.00"
                                value="{{$product->unit_price}}">
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Purchase Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="purchase_price"
                                value="{{$product->purchase_price}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Sale Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="sale_price"
                                value="{{$product->sale_price}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Special Price </label>
                            <input type="text" class="form-control" placeholder="0.00" name="special_price"
                                value="{{$product->special_price}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Discount </label>
                            <input type="text" class="form-control" placeholder="0.00" name="discount"
                                value="{{$product->discount}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Tax Detail</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax </label>
                            <input type="text" class="form-control" placeholder="0.00" name="tax"
                                value="{{$product->tax}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax Type </label>
                            <select class="form-control" name="tax_type" style="width: 100%;">
                                <option value=""> --Select Tax Type--</option>
                                <option value="Included" {{$product->tax_type == 'Included' ? 'selected' : ''}}>Included
                                </option>
                                <option value="Excluded" {{$product->tax_type == 'Excluded' ? 'selected' : ''}}>Excluded
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Description</label>
                            <textarea class="form-control" placeholder="Description..."
                                name="short_description">{{$product->short_description}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
            </div>
        </form>
    </div>
</section>

@endsection