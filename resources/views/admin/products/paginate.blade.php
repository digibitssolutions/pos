<div class="box-body table-responsive">
    <table class="table table-hover table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th>Sr.</th>
                <th>Name</th>
                <th>M.Stock</th>
                <th>P.Price</th>
                <th>S.Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse($products as $key => $product)
            <tr>
                <td>{{$key + 1}}</td>
                <td>
                    <b>
                        <a href="{{route('show-product', $product->id)}}">{{$product->product_name}}</a>
                    </b>
                </td>
                <td>{{!empty($product->master_stock) ? $product->master_stock : '--'}}</td>
                <td>{{$product->purchase_price}}</td>
                <td>{{$product->sale_price}}</td>
                <td>
                    <a class="btn btn-xs btn-primary edit-product-btn" data-id="{{$product->id}}" href="#"
                        data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-edit"></i>
                        Edit
                    </a>

                    <a class="btn btn-xs btn-danger" data-id="{{$product->id}}" href="#" data-toggle="modal"
                        data-target="#delete-{{$product->id}}"><i class="fa fa-trash"></i> Delete</a>

                    <!-- Delete Product Modal -->
                    <form method="post" action="{{route('delete-product',$product->id)}}">
                        @method('delete')
                        @csrf
                        <div class="modal fade" id="delete-{{$product->id}}" data-backdrop="static"
                            data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Delete</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <b>Are you sure to want to delete this?</b>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save
                                            changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td></td>
                <td></td>
                <td style="color: red"><b>No Record</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
<div class="pull-right">
    {{ $products->links() }}
</div>