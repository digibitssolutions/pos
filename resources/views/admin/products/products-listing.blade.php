@extends('admin.layouts.app')

@section('extra-js-files')
<script src="{{asset('js/product.js')}}"></script>

@endsection

@section('main-content')

<section class="content-header">
    <h1>All Products</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>
            <a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="fa fa-plus"></i> Add New </a>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary filters collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('products-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Product Name</label>
                                    <input type="text" class="form-control" placeholder="Product Name"
                                        name="product_name" value={{app('request')->input('product_name')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Product Code</label>
                                    <input type="text" class="form-control" placeholder="Product Code" name="code"
                                        value={{app('request')->input('code')}}>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Measurement Unit</label>
                                    <select class="form-control" name="measurement_unit" style="width: 100%;">
                                        <option value=""> --Select Measurement Unit -- </option>
                                        @foreach($measurementUnits as $key => $measurementUnit)
                                        <option value="{{$measurementUnit->id}}">{{$measurementUnit->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Category</label>
                                    <select class="form-control" name="category_id" style="width: 100%;">
                                        <option value=""> --Select Category --</option>
                                        @foreach($categories as $key => $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Exipry Date</label>
                                    <input type="date" class="form-control" placeholder="From Exipry Date"
                                        name="from_expiry_date" value={{app('request')->input('from_expiry_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Exipry Date</label>
                                    <input type="date" class="form-control" placeholder="Until Exipry Date"
                                        name="to_expiry_date" value={{app('request')->input('to_expiry_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" placeholder="From Date" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control" placeholder="To Date" name="to"
                                        value={{app('request')->input('to')}}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('products-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Products List</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Name</th>
                                <th>M.Stock</th>
                                <th>P.Price</th>
                                <th>S.Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($products as $key => $product)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td><a href="{{route('show-product', $product->id)}}">{{$product->product_name}}</a></td>
                                <td>{{!empty($product->master_stock) ? $product->master_stock : '--'}}</td>
                                <td>{{$product->purchase_price}}</td>
                                <td>{{$product->sale_price}}</td>
                                <td>
                                    <a class="btn btn-xs btn-primary edit-product-btn" data-id="{{$product->id}}"
                                        href="#" data-toggle="modal" data-target="#staticBackdrop"><i
                                            class="fa fa-edit"></i>
                                        Edit
                                    </a>

                                    <a class="btn btn-xs btn-danger" data-id="{{$product->id}}" href="#"
                                        data-toggle="modal" data-target="#delete-{{$product->id}}"><i
                                            class="fa fa-trash"></i> Delete</a>

                                    <!-- Delete Product Modal -->
                                    <form method="post" action="{{route('delete-product',$product->id)}}">
                                        @method('delete')
                                        @csrf
                                        <div class="modal fade" id="delete-{{$product->id}}" data-backdrop="static"
                                            data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="staticBackdropLabel">Delete</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <b>Are you sure to want to delete this?</b>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{-- <div id="table_data">
                        @include('admin.products.paginate')
                    </div> --}}
                </div>
                <!-- /.box-body -->
            </div>

            <!-- Add Product Modal -->
            <div class="modal fade" id="add-product-modal" data-backdrop="static" data-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" style="width: 85%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Add Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Basic Detail</h3>
                                </div>
                                <form action="{{route('store-product')}}" method="post">
                                    @csrf
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Product Name</label>
                                                    <input type="text" class="form-control" required
                                                        placeholder="Product Name" name="product_name"
                                                        value="{{old('product_name')}}" autofocus="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Product Code</label>
                                                    <input type="text" class="form-control" name="code"
                                                        placeholder="Product Code" value="{{old('code')}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Color</label>
                                                    <input type="text" class="form-control" placeholder="Color"
                                                        name="varient" value="{{old('varient')}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Expiry Date</label>
                                                    <input type="date" class="form-control" placeholder="Expiry Date"
                                                        name="expiry_date" value="{{old('expiry_date')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Units Detail</h3>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Group</label>
                                                    <select class="form-control" name="group_id" style="width: 100%;">
                                                        <option value=""> --Select group--</option>
                                                        {{-- @foreach($groups as $key => $group)
                                                        <option value="{{$group->id}}">{{$group->group_name}}
                                                        </option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Category</label>
                                                    <select class="form-control" style="width: 100%;"
                                                        name="category_id">
                                                        <option value=""> --Select category--</option>
                                                        {{-- @foreach($categories as $key => $category)
                                                        <option value="{{$category->id}}">
                                                            {{$category->category_name}}</option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Measurement Unit</label>
                                                    <select class="form-control" style="width: 100%;"
                                                        name="measurement_unit">
                                                        <option value=""> --Select Measurement Unit--</option>
                                                        {{-- @foreach($measurementUnits as $key =>
                                                        $measurementUnit)
                                                        <option value="{{$measurementUnit->id}}">
                                                            {{$measurementUnit->name}}</option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Prices Detail</h3>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Unit Price </label>
                                                    <input type="number" class="form-control" required name="unit_price"
                                                        placeholder="0.00" value="{{old('unit_price')}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Purchase Price </label>
                                                    <input type="number" class="form-control" required
                                                        placeholder="0.00" name="purchase_price"
                                                        value="{{old('purchase_price')}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Sale Price </label>
                                                    <input type="number" class="form-control" placeholder="0.00"
                                                        name="sale_price" value="{{old('sale_price')}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Special Price </label>
                                                    <input type="number" class="form-control" placeholder="0.00"
                                                        name="special_price" value="{{old('special_price')}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Discount </label>
                                                    <input type="number" class="form-control" placeholder="0.00"
                                                        name="discount" value="{{old('discount')}}">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Tax Detail</h3>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Tax </label>
                                                    <input type="number" class="form-control" name="tax"
                                                        placeholder="0.00" value="{{old('tax')}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Tax Type </label>
                                                    <select class="form-control" style="width: 100%;" name="tax_type"
                                                        value="{{old('tax_type')}}">
                                                        <option value=""> --Select Tax Type--</option>
                                                        <option value="Included">Included</option>
                                                        <option value="Excluded">Excluded</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label> Description</label>
                                                    <textarea class="form-control" name="short_description"
                                                        value="{{old('short_description')}}"
                                                        placeholder="Description..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer pull-right">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-plus"></i> Submit</button>

                                        <button type="button" class="btn btn-primary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Product Modal -->

            <!-- Edit Product Modal -->
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" style="width: 85%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Update Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Basic Detail</h3>
                                </div>
                                <form action="{{route('update-product')}}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="row">
                                            <input type="hidden" class="edit-product-id" name="id">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Product Name <span class="text-red">*</span></label>
                                                    <input type="text" class="form-control edit-product-name"
                                                        placeholder="Product Name" name="product_name">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Product Code</label>
                                                    <input type="text" class="form-control edit-product-code"
                                                        placeholder="Product Code" name="code">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Color</label>
                                                    <input type="text" class="form-control edit-product-color"
                                                        placeholder="Color" name="varient">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Expiry Date</label>
                                                    <input type="date" class="form-control edit-product-expiry"
                                                        placeholder="Expiry Date" name="expiry_date">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Units Detail</h3>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Group</label>
                                                    <select class="form-control" name="group_id" style="width: 100%;">
                                                        <option value=""> -- Select Group -- </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Category</label>
                                                    <select class="form-control" name="category_id"
                                                        style="width: 100%;">
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Measurement Unit</label>
                                                    <select class="form-control" name="measurement_unit"
                                                        style="width: 100%;">
                                                        <option value=""> -- Select Measurement Unit -- </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Prices Detail</h3>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Unit Price <span class="text-red">*</span></label>
                                                    <input type="text" class="form-control edit-product-unit"
                                                        name="unit_price" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Purchase Price <span class="text-red">*</span></label>
                                                    <input type="text" class="form-control edit-product-purchase"
                                                        placeholder="0.00" name="purchase_price">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Sale Price <span class="text-red">*</span></label>
                                                    <input type="text" class="form-control edit-product-sale"
                                                        placeholder="0.00" name="sale_price">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Special Price </label>
                                                    <input type="text" class="form-control edit-product-special"
                                                        placeholder="0.00" name="special_price">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Discount </label>
                                                    <input type="text" class="form-control edit-product-discount"
                                                        placeholder="0.00" name="discount">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="box-header">
                                                <h3 class="box-title">Tax Detail</h3>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Tax </label>
                                                    <input type="text" class="form-control edit-product-tax"
                                                        placeholder="0.00" name="tax">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label> Tax Type </label>
                                                    <select class="form-control" name="tax_type" style="width: 100%;">
                                                        <option value=""> --Select Tax Type--</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label> Description</label>
                                                    <textarea class="form-control edit-product-description"
                                                        placeholder="Description..."
                                                        name="short_description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer pull-right">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i>
                                            Update</button>
                                        <button type="button" class="btn btn-primary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Edit Product Modal -->

        </div>
    </div>
</section>

@endsection