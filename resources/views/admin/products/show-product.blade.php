@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Product Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('products-list')}}"><i class="fa fa-list"></i> Products Lists</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Name</label>
                            <input type="text" class="form-control"
                                value="{{$product->product_name ? $product->product_name :'--'}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Product Code</label>
                            <input type="text" class="form-control" value="{{$product->code ? $product->code : '--'}}"
                                readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Color</label>
                            <input type="text" class="form-control"
                                value="{{$product->varient ? $product->varient : '--'}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Expiry Date</label>
                            <input type="text" class="form-control"
                                value="{{$product->expiry_date ? $product->expiry_date : '--'}}" readonly>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Units Detail</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Group</label>
                            <input type="text" class="form-control"
                                value="{{$product->group_id ? $product->group->group_name : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Category</label>
                            <input type="text" class="form-control"
                                value="{{$product->category_id ? $product->category->category_name : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Measurement Unit</label>
                            <input type="text" class="form-control"
                                value="{{$product->measurement_unit ? $product->measurementUnit->name : '--'}}"
                                readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Prices Detail</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Unit Price </label>
                            <input type="text" class="form-control"
                                value="{{$product->unit_price ? $product->unit_price : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Purchase Price </label>
                            <input type="text" class="form-control"
                                value="{{$product->purchase_price ? $product->purchase_price : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Sale Price </label>
                            <input type="text" class="form-control"
                                value="{{$product->sale_price ? $product->sale_price : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Special Price </label>
                            <input type="text" class="form-control"
                                value="{{$product->special_price ? $product->special_price : '--'}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Discount </label>
                            <input type="text" class="form-control"
                                value="{{$product->discount ? $product->discount : '--'}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="box-header">
                        <h3 class="box-title">Tax Detail</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax </label>
                            <input type="text" class="form-control" value="{{$product->tax ? $product->tax : '--'}}"
                                readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label> Tax Type </label>
                            <input type="text" class="form-control"
                                value="{{$product->tax_type ? $product->tax_type : '--'}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Description</label>
                            <textarea class="form-control"
                                readonly>{{$product->short_description ? $product->short_description : '--'}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{route('products-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go
                    Back</a>
            </div>
        </form>
    </div>
</section>

@endsection