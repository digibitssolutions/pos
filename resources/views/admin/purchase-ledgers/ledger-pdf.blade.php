<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hamza Traders</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        #line {
            color: #B32C39;
        }

        .left-div {
            width: 70%;
            float: left;
        }

        .right-div {
            width: 30%;
            float: left;
        }

        .left-div p {
            font-family: sans-serif;
            color: #5B6165;
            font-size: 11px;
        }

        .table-style {
            margin-top: 70px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            table-layout: auto;
        }

        .table-heading {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 4px;
            font-size: 10px;
            background: #F4846F;
            border: 1px solid #F4846F;
            color: white;
        }

        .table-data {
            padding: 2px;
            font-size: 10px;
            border: 1px solid #dddddd;
            color: #5B6165;
        }

        tr:nth-child(even) {
            background-color: #F7EDEB;
        }

        .right {
            float: right;
            font-family: sans-serif;
            font-size: 11px;
        }

        .right-div p {
            font-family: sans-serif;
            color: #5B6165;
            font-size: 11px;

        }

        .footer-right-div {
            width: 30%;
            float: right;
        }

        .footer-right-div p {
            font-family: sans-serif;
            color: #5B6165;
            font-size: 12px;
        }

        .total {
            font-family: sans-serif;
            font-size: 12px;
            color: #B32C39;
        }

        .company {
            font-family: sans-serif;
            font-size: 12px;
            color: #5B6165;
            margin-top: -35px;
        }

        .company-info {
            font-weight: bold;
            /* font-size: 22px; */
            color: #B32C39;
            font-family: sans-serif;
        }

        h2 {
            margin: 0px;
        }

        .footer-right-div p {
            margin: 2px;
        }

    </style>
</head>

<body>

    <center>
        <div class="company">
            <h2 class="company-info">Accounts Statement</h2>
            <hr>
        </div>
    </center>

    <div class="left-div">
        <p> <b> Party Name: </b> {{ $party->party_name }}</p>
        <p> <b> Address: </b> {{ $party->user->userDetail->city }}</p>
    </div>
    <div class="right-div">
        <p> <b> From Date: </b> -- </p>
        <p> <b> To Date: </b> -- </p>
    </div>

    <table class="table table-style table-bordered">
        <thead>
            <tr>
                <th class="table-heading">Sr.</th>
                <th class="table-heading">Description</th>
                <th class="table-heading">Supplier</th>
                <th class="table-heading">Payable</th>
                <th class="table-heading">Paid</th>
                <th class="table-heading">Total</th>
                <th class="table-heading">Status</th>
                <th class="table-heading">Date</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $totalPayable = 0;
                $totalPaid = 0;
                $total = 0;

                $totalPayable1 = 0;
                $totalPaid1 = 0;
                $total1 = 0;
            ?>
            @foreach ($ledgers as $key =>$ledger)
            <?php 
                $total1 += $ledger->payable;
                $totalPaid1 += $ledger->paid;
                $totalPayable1 = $total1 - $totalPaid1;
            ?>
            <tr>
                <td class="table-data"><b>{{++$key}}</b></td>
                <td class="table-data"><b>{{$ledger->description}}</b></td>
                <td class="table-data"><b>{{$ledger->party->party_name}}</b></td>
                <td class="table-data"><b>{{$ledger->payable}}</b></td>
                <td class="table-data"><b>{{$ledger->paid}}</b></td>
                @if($ledger->status == 'paid')
                <td class="table-data">{{$key == 1 ? $ledger->payable : $totalPayable1}}</td>
                @else
                <td class="table-data">{{$key == 1 ? $ledger->payable : $ledger->payable+$totalPayable}}</td>
                @endif
                <td class="table-data"><b>{{$ledger->status}}</b></td>
                <td class="table-data"><b>{{$ledger->bill_date}}</b></td>
            </tr>
            <?php
                $total += $ledger->payable;
                $totalPaid += $ledger->paid;
                $totalPayable = $total - $totalPaid;
            ?>
            @endforeach
        </tbody>
    </table>
    <br>

    <div class="footer-right-div">
        <hr id="line">
        <p> <b class="total">Total:</b> <b class="right"> {{'Rs. '. $total}}</b></p>
        <p> <b class="total">Paid:</b> <b class="right"> {{'Rs. '.$totalPaid}}</b></p>
        <p> <b class="total">Payable:</b> <b class="right"> {{'Rs. '. $totalPayable}}</b></p>
    </div>

</body>

</html>