<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Ledgers List</h3>
        <div class="box-tools">
            <a href="{{route('purchase-ledger-report', $filters)}}"><i class="fa fa-fw fa-file-pdf-o"
                    style="font-size: 30px;color:red;"></i></a>
        </div>
    </div>

    <div class="box-body table-responsive">
        <table class="table table-hover table-striped table-bordered table-condensed purchase-ledger-table">
            <thead>
                <tr>
                    <th>Sr.</th>
                    <th>Description</th>
                    <th>Supplier</th>
                    <th>Payable</th>
                    <th>Paid</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $totalPayable = 0;
                    $totalPaid = 0;
                    $total = 0;

                    $totalPayable1 = 0;
                    $totalPaid1 = 0;
                    $total1 = 0;
                ?>
                @forelse($ledgers as $key => $ledger)
                <?php 
                    $total1 += $ledger->payable;
                    $totalPaid1 += $ledger->paid;
                    $totalPayable1 = $total1 - $totalPaid1;
                ?>
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$ledger->description}}</td>
                    <td><b>{{$ledger->party_name}}</b></td>
                    <td>{{$ledger->payable}}</td>
                    <td>{{$ledger->paid}}</td>
                    @if($ledger->status == 'paid')
                    <td>{{$key == 1 ? $ledger->payable : $totalPayable1}}</td>
                    @else
                    <td>{{$key == 1 ? $ledger->payable : $ledger->payable+$totalPayable}}</td>
                    @endif
                    <td>{{$ledger->status}}</td>
                    <td>{{$ledger->bill_date}}</td>
                </tr>
                <?php
                    $total += $ledger->payable;
                    $totalPaid += $ledger->paid;
                    $totalPayable = $total - $totalPaid;
                ?>
                @empty
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: red"><b>No Record</b></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Total</th>
                    <th rowspan="1" colspan="1">{{'Rs. '. $total}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Paid</th>
                    <th rowspan="1" colspan="1">{{'Rs. '.$totalPaid}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Payable</th>
                    <th rowspan="1" colspan="1">{{'Rs. '. $totalPayable}}</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>