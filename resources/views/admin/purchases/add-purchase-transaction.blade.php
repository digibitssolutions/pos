@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Transaction Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('purchases-list')}}"><i class="fa fa-list"></i> Purchases List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <form action="{{route('store-purchase-transaction')}}" method="post">
        @csrf
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Basic Detail</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Party Name</label>
                            <input type="hidden" name="party_id" value="{{$purchase->party_id}}">
                            <input type="text" class="form-control" readonly value="{{$purchase->party->party_name}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice No</label>
                            <input type="hidden" name="purchase_id" value="{{$purchase->id}}">
                            <input type="text" class="form-control" readonly value="{{$purchase->invoice_no}}"
                                name="invoice_no">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice Date</label>
                            <input type="text" class="form-control" readonly value="{{$purchase->invoice_date}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Transaction Type</label>
                            <select class="form-control" name="transaction_type_id" required>
                                <option value=""> -- Select Transaction Type -- </option>
                                <option value="1">Cash</option>
                                <option value="2">Credit</option>
                                {{-- <option value="3">Debit</option> --}}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Transaction Amount</label>
                            <input type="text" class="form-control" name="transaction_amount"
                                value="{{old('transaction_amount')}}" placeholder="Transaction Amount" required>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Transaction Via</label>
                            <input type="text" class="form-control" name="payment_via" value="{{old('payment_via')}}"
                                placeholder="Transaction Via">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Transaction Via Mobile</label>
                            <input type="text" class="form-control" name="payment_via_mobile"
                                value="{{old('payment_via_mobile')}}" placeholder="Mobile">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Transaction Via CNIC</label>
                            <input type="text" class="form-control" name="payment_via_cnic"
                                value="{{old('payment_via _cnic')}}" placeholder="CNIC">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Transaction Note</label>
                            <textarea class="form-control" name="note"
                                placeholder="Transaction Note">{{old('note')}}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Submit</button>
            </div>
        </div>
    </form>
</section>
@endsection