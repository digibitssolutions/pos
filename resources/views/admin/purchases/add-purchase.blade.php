@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Purchase Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('purchases-list')}}"><i class="fa fa-list-ul"></i> Purchases List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Party Account Detail</h3>
        </div>
        <form action="{{route('store-purchase')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Party Name</label>
                            <select class="form-control select2" name="party_id" autofocus="true" required>
                                <option value=""> -- Select Party -- </option>
                                @foreach($parties as $key => $party)
                                <option value="{{$party->id}}">{{$party->party_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice No.</label>
                            <input type="text" class="form-control" placeholder="Invoice No." name="invoice_no"
                                value="{{old('invoice_no')}}" required>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice Date </label>
                            <input type="date" class="form-control" placeholder="Invoice Date" name="invoice_date"
                                value="{{old('invoice_date')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Total Amount </label>
                            <input type="text" class="form-control" placeholder="Total Amount" name="total_amount"
                                value="{{old('total_amount')}}" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Discount</label>
                            <input type="text" class="form-control" placeholder="Discount" name="discount"
                                value="{{old('discount')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Net Amount</label>
                            <input type="text" class="form-control" placeholder="Net Amount" name="net_amount"
                                value="{{old('net_amount')}}" required>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Images*</label>
                            <input type="file" class="form-control" name="images" value="{{old('images')}}" multiple>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Note</label>
                            <textarea class="form-control" placeholder="Note.." name="note">{{old('note')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Submit</button>
            </div>
        </form>
    </div>
</section>
@endsection