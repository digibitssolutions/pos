@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Party Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('parties-list')}}"><i class="fa fa-list"></i> Parties Lists</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form action="{{route('update-purchase',$purchase->id)}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Party Name</label>
                            <select class="form-control select2" name="party_id" autofocus="true">
                                <option value=""> -- Select Party -- </option>
                                @foreach($parties as $key => $party)
                                <option value="{{$party->id}}" {{$party->id == $purchase->party_id ? 'selected' :
                                    ''}}>{{$party->party_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice No.</label>
                            <input type="text" class="form-control" placeholder="Invoice No." name="invoice_no"
                                value="{{$purchase->invoice_no}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice Date </label>
                            <input type="date" class="form-control" placeholder="Invoice Date" name="invoice_date"
                                value="{{$purchase->invoice_date}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Total Amount </label>
                            <input type="text" class="form-control" placeholder="Total Amount" name="total_amount"
                                value="{{$purchase->total_amount}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Discount</label>
                            <input type="text" class="form-control" placeholder="Discount" name="discount"
                                value="{{$purchase->discount}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Net Amount</label>
                            <input type="text" class="form-control" placeholder="Net Amount" name="net_amount"
                                value="{{$purchase->net_amount}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Images*</label>
                            <input type="file" class="form-control" name="images" value="{{old('images')}}" multiple>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Note</label>
                            <textarea class="form-control" placeholder="Note.."
                                name="note">{{$purchase->note}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
            </div>
        </form>
    </div>
</section>

@endsection