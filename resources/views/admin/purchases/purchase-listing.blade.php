@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Purchases Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('create-purchase')}}"><i class="fa fa-plus"></i> Add New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('purchases-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Party Name</label>
                                    <select class="form-control select2" name="party_id">
                                        <option value=""> -- Select Party -- </option>
                                        @foreach($parties as $key => $party)
                                        <option value="{{$party->id}}">{{$party->party_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Mobile</label>
                                    <input type="text" class="form-control" placeholder="Mobile" name="mobile"
                                        value={{app('request')->input('mobile')}}>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Email</label>
                                    <input type="text" class="form-control" placeholder="Email" name="email"
                                        value={{app('request')->input('email')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> City</label>
                                    <input type="text" class="form-control" placeholder="City" name="city"
                                        value={{app('request')->input('city')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> State</label>
                                    <input type="text" class="form-control" placeholder="State" name="state"
                                        value={{app('request')->input('state')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" placeholder="From Date" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control" placeholder="To Date" name="to"
                                        value={{app('request')->input('to')}}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('purchases-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Parties List</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Party</th>
                                <th>Invoice No.</th>
                                <th>Net Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($purchases as $key => $purchase)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$purchase->party->party_name}}</td>
                                <td>{{$purchase->invoice_no}}</td>
                                <td><strong>{{$purchase->net_amount}}</strong></td>
                                <td>{{$purchase->invoice_date}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-success btn-xs dropdown-toggle" type="button"
                                            id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="true">
                                            Action
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li>
                                                <a href="{{route('show-purchase',$purchase->id)}}">
                                                    <strong class="text-primary"> View</strong></a>
                                            </li>
                                            <li>
                                                <a href="{{route('add-purchase-transaction',$purchase->id)}}"><strong
                                                        class="text-danger">Add
                                                        Transaction</strong></a>
                                            </li>
                                            <li>
                                                <a href="{{route('edit-purchase',$purchase->id)}}"><strong
                                                        class="text-success">Edit</strong></a>
                                            </li>
                                        </ul>
                                    </div>
                                    {{-- <a class="btn btn-xs btn-danger  " onclick="if(confirm('Are your sure you want to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-{{$purchase->id}}').submit();
                                    } else {
                                    e.preventDefault();
                                    }">Delete</a>
                                </td> --}}
                            </tr>
                            {{-- <form id="delete-{{$purchase->id}}" action="{{route('delete-product',$purchase->id)}}"
                                method="POST" hidden>
                                @csrf
                                @method('delete')
                            </form> --}}
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix" *ngIf="noRecord">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li>
                        </li>
                    </ul>
                    {{-- <p> Showing {{records?.from}} to {{records?.per_page}} of {{records?.total}}</p> --}}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('extra-js-files')
@endsection