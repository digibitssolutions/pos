@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Purchase Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('parties-list')}}"><i class="fa fa-list"></i> Parties Lists</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <form>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Party Name</label>
                            <input type="text" class="form-control" value="{{$purchase->party_id}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Invoice No.</label>
                            <input type="text" class="form-control" value="{{$purchase->invoice_no}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Total Amount</label>
                            <input type="text" class="form-control" value="{{$purchase->total_amount}}" readonly>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Discount %</label>
                            <input type="text" class="form-control" value="{{$purchase->discount}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Net Amount</label>
                            <input type="text" class="form-control" value="{{$purchase->net_amount}}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Note</label>
                            <textarea class="form-control" placeholder="Note.." readonly>{{$purchase->note}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{route('purchases-list')}}" class="btn btn-primary  "><i class="fa fa-arrow-left"></i> Go
                    Back</a>
            </div>
        </form>
    </div>
</section>

@endsection