@extends('admin.layouts.app')
@section('main-content')

<section class="content-header">
    <h1>Products Audit</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Products List</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('products-audit-generate','type=pdf')}}"><i class="fa fa-fw fa-file-pdf-o"
                                style="font-size: 30px;color:red;"></i></a>
                        <a href="{{route('products-audit-generate','type=excel')}}"><i class="fa fa-fw fa-file-excel-o"
                                style="font-size: 30px;color:green;"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Name</th>
                                <th>P.Price</th>
                                <th>M.Stock</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sum = 0;
                                $totalQty = 0;
                            ?>
                            @forelse($products as $key => $product)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$product->product_name}}</td>
                                <td>{{$product->purchase_price}}</td>
                                <td>{{!empty($product->masterStock) ? $product->masterStock->master_stock : '0'}}</td>
                                <td>{{!empty($product->masterStock) ? $product->purchase_price *
                                    $product->masterStock->master_stock : '0'}}</td>
                            </tr>
                            <?php 
                                $sum = ($sum) + (!empty($product->masterStock) ? $product->purchase_price *
                                $product->masterStock->master_stock : 0);

                                $totalQty += !empty($product->masterStock) ? $product->masterStock->master_stock : 0;
                            ?>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot class="bg-success">
                            <tr>
                                <th rowspan="1" colspan="3" class="text-center">Total</th>
                                <th rowspan="1" colspan="1">{{'Qty. '. $totalQty}}</th>
                                <th rowspan="1" colspan="1">{{'Rs. '.$sum}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection