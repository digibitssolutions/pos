@extends('admin.layouts.app')
@section('main-content')

<section class="content-header">
    <h1>All Products</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Products List</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('product-generate-report','type=pdf')}}"><i class="fa fa-fw fa-file-pdf-o"
                                style="font-size: 30px;color:red;"></i></a>
                        <a href="{{route('product-generate-report','type=excel')}}"><i class="fa fa-fw fa-file-excel-o"
                                style="font-size: 30px;color:green;"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Name</th>
                                <th>P.Price</th>
                                <th>S.Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($products as $key => $product)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$product->product_name}}</td>
                                </td>
                                <td>{{$product->purchase_price}}</td>
                                <td>{{$product->sale_price}}</td>
                                <td>
                                    <a class="btn btn-xs btn-success" href="{{route('show-product',$product->id)}}"><i
                                            class="fa fa-eye"></i> View</a>
                                    <a class="btn btn-xs btn-primary" href="{{route('edit-product',$product->id)}}"><i
                                            class="fa fa-edit"></i> Edit</a>
                                    <a class="btn btn-xs btn-danger" onclick="if(confirm('Are your sure you want to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-{{$product->id}}').submit();}">
                                        <i class="fa fa-trash"></i> Delete</a>
                                    <form id="delete-{{$product->id}}" action="{{route('delete-product',$product->id)}}"
                                        method="POST" hidden>
                                        @csrf
                                        @method('delete')
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection