@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1> Reports </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reports</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-bag"></i></span>

                <div class="info-box-content">
                    <a href="{{route('products-report')}}"><span class="info-box-number">Products</span></a>
                    <span class="info-box-number">{{$products}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <a href="{{route('clients-report')}}"><span class="info-box-number">Clients Balance</span></a>
                    <span class="info-box-number">{{$clients}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-hourglass-1"></i></span>

                <div class="info-box-content">
                    <a href="{{route('short-stock-report')}}"><span class="info-box-number">Short Stock</span></a>
                    <span class="info-box-number">{{$shortStocks}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-history"></i></span>

                <div class="info-box-content">
                    <a href="{{route('products-audit')}}"><span class="info-box-number">Products Audit</span></a>
                    <span class="info-box-number">{{$products}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-history"></i></span>
        
                <div class="info-box-content">
                    <a href="{{route('invoices-list-report')}}"><span class="info-box-number">Today Supply Report</span></a>
                    <span class="info-box-number">{{$todayInvoices}}</span>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- /.content -->

@endsection