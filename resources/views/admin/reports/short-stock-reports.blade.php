@extends('admin.layouts.app')
@section('main-content')

<section class="content-header">
    <h1>All Stocks</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('short-stock')}}"><i class="fa fa-plus"></i> Add New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('short-stock-report')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Product Name</label>
                                    <input type="text" class="form-control" placeholder="Product Name"
                                        name="product_name" value={{app('request')->input('product_name')}}>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Master Stock</label>
                                    <input type="number" class="form-control" placeholder="Master Stock"
                                        name="master_stock" value={{app('request')->input('master_stock')}}>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Required Stock</label>
                                    <input type="number" class="form-control" placeholder="Required Stock"
                                        name="required_stock" value={{app('request')->input('required_stock')}}>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-filter"></i> Filter</button>
                        <a href="{{route('short-stock-listing')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Short Stock List</h3>
                    <div class="box-tools">
                        <a href="{{route('short-generate-report','type=pdf')}}"><i class="fa fa-fw fa-file-pdf-o"
                                style="font-size: 30px;color:red;"></i></a>
                        <a href="{{route('short-generate-report','type=excel')}}"><i class="fa fa-fw fa-file-excel-o"
                                style="font-size: 30px;color:green;"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Product</th>
                                <th>Old Stock</th>
                                <th>Required</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($shortStocks as $key => $shortStock)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$shortStock->product->product_name}}</td>
                                <td><b>{{$shortStock->old_stock}}</b></td>
                                <td>{{$shortStock->required_stock}}</td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection