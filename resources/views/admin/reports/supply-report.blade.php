@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Supply Report</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

@php
use Illuminate\Support\Facades\Request;

$filters = [];

if (Request::has('client_id')) {
$filters['client_id'] = Request::get('client_id');
}

if (Request::has('from')) {
$filters['from'] = Request::get('from');
}

if (Request::has('to')) {
$filters['to'] = Request::get('to');
}

@endphp

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary collapsed-box filters">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('invoices-list-report')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Customer Name</label>
                                    <br>
                                    <select class="form-control select2" name="client_id">
                                        <option value=""> -- Select Customer --</option>
                                        @foreach($clients as $key => $client)
                                        <option value="{{$client->id}}">{{$client->user->first_name}} {{$client->user->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value="{{app('request')->input('from')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="to" value="{{app('request')->input('to')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('invoices-list-report')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Invoice Listing</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('supply-report', ['type' => 'pdf', 'filter' => $filters])}}"><i
                                class="fa fa-fw fa-file-pdf-o" style="font-size: 30px;color:red;"></i></a>
                        <a href="{{route('supply-report', ['type' => 'excel', 'filter' => $filters])}}"><i
                                class="fa fa-fw fa-file-excel-o" style="font-size: 30px;color:green;"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="example2">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Customer</th>
                                <th>Invoice No</th>
                                <th>Sales Person</th>
                                <th>Invoice Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($invoices as $key => $invoice)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$invoice->client->user->first_name.' '.$invoice->client->user->last_name}}</td>
                                <td>{{$invoice->invoice_no}}</td>
                                <td>{{$invoice->salePerson == null ? '--' : $invoice->salePerson->user->first_name.'
                                    '.$invoice->salePerson->user->last_name}}</td>
                                <td>{{$invoice->invoice_date}}</td>
                                <td>{{$invoice->total_amount ? $invoice->total_amount : '0.0'}}</td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection