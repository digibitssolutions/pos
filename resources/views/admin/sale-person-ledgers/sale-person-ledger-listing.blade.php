@extends('admin.layouts.app')
@section('main-content')

<section class="content-header">
    <h1>All Sale Ledgers</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="javascript:;" data-toggle="modal" data-target="#ledger-modal"><i class="fa fa-plus"></i> Add
                New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p></p>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('sale-person-ledger')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label> Select Sale Person </label>
                                    <select class="form-control select2" name="sale_person_id">
                                        <option value="" selected> -- Select Sale Person --</option>
                                        @foreach($salePersons as $sPerson)
                                        <option value="{{$sPerson->id}}">
                                            {{$sPerson->user->first_name.' '.$sPerson->user->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-sm" name="submit"><i
                                            class="fa fa-fw fa-filter"></i> Filter</button>
                                    <a href="{{route('sale-person-ledger')}}" class="btn btn-success btn-sm"><i
                                            class="fa fa-fw fa-paper-plane"></i> Clear</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            @if(isset($_GET['submit']))

            @include('admin.sale-person-ledgers.sale-person-ledger')

            @endif

            <!-- Modal -->
            <div class="modal fade" id="ledger-modal" data-backdrop="static" data-keyboard="false"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" style="width: 85%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Add Transaction</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Basic Detail</h3>
                                </div>
                                <form id="add-sale-ledger-form">
                                    @csrf
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Select Sale Person </label>
                                                    <select class="select2" name="sale_person_id">
                                                        <option value=""> -- Select Sale Person -- </option>
                                                        @foreach($salePersons as $sPerson)
                                                        <option value="{{$sPerson->id}}">
                                                            {{$sPerson->user->first_name.' '.$sPerson->user->last_name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Select Client </label>
                                                    <select class="select2" name="client_id">
                                                        <option value=""> -- Select Client -- </option>
                                                        @foreach($clients as $client)
                                                        <option value="{{$client->id}}">
                                                            {{$client->user->first_name.' '.$client->user->last_name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Date </label>
                                                    <input type="date" class="form-control" name="date"
                                                        value="{{date('Y-m-d', strtotime('now'))}}" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <label>Payment Type</label><strong style="color: red">*</strong>
                                                <select class="form-control" name="payment_type" required>
                                                    <option value="" selected> -- Payment Type --</option>
                                                    <option value="cash"> Cash </option>
                                                    <option value="bank"> Bank </option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="row">

                                            <div class="col-lg-3">
                                                <label>Transaction Mode</label><strong style="color: red">*</strong>
                                                <select class="form-control" name="mode" required>
                                                    <option value="" selected> -- Mode --</option>
                                                    <option value="payable"> Payable </option>
                                                    <option value="paid"> Paid </option>
                                                </select>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Amount <strong class="text-danger">*</strong></label>
                                                    <input type="text" class="form-control" required
                                                        placeholder="Amount" name="amount">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> Description</label>
                                                    <input type="text" class="form-control" placeholder="Description"
                                                        name="description" value="amount recieved">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer pull-right" style="border: none">
                                        <button type="submit" class="btn btn-primary add-sale-ledger-btn">
                                            <i class="fa fa-plus"></i> Submit</button>

                                        <button type="button" class="btn btn-success"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>
</section>

@endsection