<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Ledgers List</h3>
        <div class="box-tools">
        </div>
    </div>

    <div class="box-body table-responsive">
        <table id="" class="table table-hover table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th>Sr.</th>
                    <th>Description</th>
                    <th>Client</th>
                    <th>Payable</th>
                    <th>Paid</th>
                    <th>Total</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $totalPayable = 0;
                    $totalPaid = 0;
                    $total = 0;

                    $totalPayable1 = 0;
                    $totalPaid1 = 0;
                    $total1 = 0;
                ?>
                @forelse($ledgers as $key => $ledger)
                <?php 
                    $total1 += $ledger->payable;
                    $totalPaid1 += $ledger->paid;
                    $totalPayable1 = $total1 - $totalPaid1;
                ?>
                <tr>
                    <td>{{++$key}}</td>
                    <td width="20%">{{$ledger->description}}</td>
                    <td width="20%"><b>{{$ledger->client->user->first_name.' '.$ledger->client->user->last_name}}</b></td>
                    <td>{{$ledger->payable}}</td>
                    <td>{{$ledger->paid}}</td>
                    <td>{{$key == 1 ? $ledger->payable : $totalPayable1}}</td>
                    <td>{{$ledger->date}}</td>
                </tr>
                <?php
                    $total += $ledger->payable;
                    $totalPaid += $ledger->paid;
                    $totalPayable = $total - $totalPaid;
                ?>
                @empty
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: red"><b>No Record</b></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Total</th>
                    <th rowspan="1" colspan="1" class="text-success">{{'Rs. '. $total}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Paid</th>
                    <th rowspan="1" colspan="1" class="text-primary">{{'Rs. '.$totalPaid}}</th>
                </tr>
                <tr>
                    <th rowspan="1" colspan="6" class="text-right">Payable</th>
                    <th rowspan="1" colspan="1" class="text-danger">{{'Rs. '. $totalPayable}}</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>