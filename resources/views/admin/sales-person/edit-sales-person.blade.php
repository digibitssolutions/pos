@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Client Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('sales-person-list')}}"><i class="fa fa-list-ul"></i> Sales Person List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Client Account Detail</h3>
        </div>
        <form action="{{route('update-sales-person',$salesPerson->id)}}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> First Name</label><strong style="color: red">*</strong>
                            <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                value="{{$salesPerson->first_name}}" autofocus="true" required>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Middle Name</label>
                            <input type="text" class="form-control" placeholder="Middle Name" name="middle_name"
                                value="{{$salesPerson->middle_name}}">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Last Name</label><strong style="color: red">*</strong>
                            <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                value="{{$salesPerson->last_name}}" required>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email"
                                value="{{$salesPerson->email}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Mobile </label>
                            <input type="text" class="form-control" placeholder="Mobile" name="mobile"
                                value="{{$salesPerson->mobile}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Phone </label>
                            <input type="text" class="form-control" placeholder="Phone" name="phone"
                                value="{{$salesPerson->phone}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> City</label>
                            <input type="text" class="form-control" placeholder="City" name="city"
                                value="{{$salesPerson->city}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> State</label>
                            <input type="text" class="form-control" placeholder="State" name="state"
                                value="{{$salesPerson->state}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Zip Code</label>
                            <input type="text" class="form-control" placeholder="Zip Code" name="zip"
                                value="{{$salesPerson->zip}}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label> Address</label>
                            <textarea class="form-control" placeholder="Address.."
                                name="address">{{$salesPerson->address}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
            </div>
        </form>
    </div>
</section>
@endsection