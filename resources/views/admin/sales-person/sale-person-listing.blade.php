@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>SalesPerson Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="javascript:;" data-toggle="modal" data-target="#add-sale-person-modal"><i class="fa fa-plus"></i>
                Add New</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-success alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p></p>
            </div>
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('sales-person-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                        value={{app('request')->input('first_name')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Last Name</label>
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                        value={{app('request')->input('last_name')}}>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Mobile</label>
                                    <input type="text" class="form-control" placeholder="Mobile" name="mobile"
                                        value={{app('request')->input('mobile')}}>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Email</label>
                                    <input type="text" class="form-control" placeholder="Email" name="email"
                                        value={{app('request')->input('email')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> City</label>
                                    <input type="text" class="form-control" placeholder="City" name="city"
                                        value={{app('request')->input('city')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> State</label>
                                    <input type="text" class="form-control" placeholder="State" name="state"
                                        value={{app('request')->input('state')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" placeholder="From Date" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control" placeholder="To Date" name="to"
                                        value={{app('request')->input('to')}}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('sales-person-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Sales Person List</h3>
                    <div class="box-tools">
                    </div>
                </div>

                <div class="box-body table-responsive">
                    <table id="example2" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>City</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($salesPersons as $key => $sPerson)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>
                                    <a href="#" class="view-person-btn" data-toggle="modal"
                                        data-target="#view-sale-person-modal" data-id="{{$sPerson->id}}">
                                        <strong>{{$sPerson->first_name.' '.$sPerson->last_name}}</strong>
                                    </a>
                                </td>
                                <td>{{$sPerson->phone}}</td>
                                <td>{{$sPerson->city}}</td>
                                <td>
                                    <a class="btn btn-xs btn-primary edit-person-btn"
                                        href="#" data-id="{{$sPerson->id}}" data-toggle="modal" data-target="#update-person-modal">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <!-- Add Person Modal -->
                    <div class="modal fade" id="add-sale-person-modal" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
                            style="width: 85%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Add SalePerson</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Basic Detail</h3>
                                        </div>
                                        <form id="add-sale-person-form">
                                            @csrf
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> First Name <strong
                                                                    class="text-danger">*</strong></label>
                                                            <input type="text" class="form-control"
                                                                placeholder="First Name" name="first_name"
                                                                value="{{old('first_name')}}" autofocus="true" required>

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Middle Name</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Middle Name" name="middle_name"
                                                                value="{{old('middle_name')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Last Name <strong
                                                                    class="text-danger">*</strong></label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Last Name" name="last_name"
                                                                value="{{old('last_name')}}" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Email <strong class="text-danger">*</strong></label>
                                                            <input type="email" class="form-control"
                                                                placeholder="Enter Email" name="email"
                                                                value="{{old('email')}}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Password <strong
                                                                    class="text-danger">*</strong></label>
                                                            <input type="password" class="form-control"
                                                                placeholder="Enter Password" name="password"
                                                                value="{{old('password')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label>Confirm Password <strong
                                                                    class="text-danger">*</strong></label>
                                                            <input type="password" class="form-control"
                                                                placeholder="Confirm Password"
                                                                name="password_confirmation"
                                                                value="{{old('password_confirmation')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Mobile <strong
                                                                    class="text-danger">*</strong></label>
                                                            <input type="number" class="form-control"
                                                                placeholder="Mobile" name="mobile"
                                                                value="{{old('mobile')}}" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Phone </label>
                                                            <input type="number" class="form-control"
                                                                placeholder="Phone" name="phone"
                                                                value="{{old('phone')}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> City</label>
                                                            <input type="text" class="form-control" placeholder="City"
                                                                name="city" value="{{old('city')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> State</label>
                                                            <input type="text" class="form-control" placeholder="State"
                                                                name="state" value="{{old('state')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Designation</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Designation" name="designation"
                                                                value="{{old('designation')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Zip Code</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Zip Code" name="zip"
                                                                value="{{old('zip')}}">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Department</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Department" name="department"
                                                                value="{{old('department')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Website</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Website" name="website"
                                                                value="{{old('website')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Facebook</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Facebook" name="facebook"
                                                                value="{{old('facebook')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Twitter</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Twitter" name="twitter"
                                                                value="{{old('twitter')}}">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label> Address</label>
                                                            <textarea class="form-control" placeholder="Address.."
                                                                name="address">{{old('address')}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="box-footer pull-right" style="border: none">
                                                <button type="submit" class="btn btn-primary add-sale-person-btn">
                                                    <i class="fa fa-plus"></i> Submit</button>

                                                <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add Person Modal -->

                    <!-- View Person Modal -->
                    <div class="modal fade" id="view-sale-person-modal" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
                            style="width: 85%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">View Sale Person</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Basic Detail</h3>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> First Name</label>
                                                        <input type="text" class="form-control view-person-first"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Middle Name</label>
                                                        <input type="text" class="form-control view-person-middle"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Last Name</label>
                                                        <input type="text" class="form-control view-person-last"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Email</label>
                                                        <input type="text" class="form-control view-person-email"
                                                            readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Mobile </label>
                                                        <input type="text" class="form-control view-person-mob"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Phone </label>
                                                        <input type="text" class="form-control view-person-phone"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Department</label>
                                                        <input type="text" class="form-control view-person-depart"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> City</label>
                                                        <input type="text" class="form-control view-person-city"
                                                            readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> State</label>
                                                        <input type="text" class="form-control view-person-state"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Designation</label>
                                                        <input type="text" class="form-control view-person-designation"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Zip Code</label>
                                                        <input type="text" class="form-control view-person-zip"
                                                            readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Website</label>
                                                        <input type="text" class="form-control view-person-web"
                                                            readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Facebook</label>
                                                        <input type="text" class="form-control view-person-fb" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Twitter</label>
                                                        <input type="text" class="form-control view-person-tw" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label> Type </label>
                                                        <input type="text" class="form-control view-person-type"
                                                            readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label> Address</label>
                                                        <textarea class="form-control view-person-addr"
                                                            readonly></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-footer pull-right">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                <i class="fa fa-times"></i>
                                                Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- View Person Modal -->

                    <!-- Update Person Modal -->
                    <div class="modal fade" id="update-person-modal" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
                            style="width: 85%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Update Client</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="max-height: calc(100vh - 130px);overflow-y: auto;">
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Basic Detail</h3>
                                        </div>
                                        <form id="update-person-form">
                                            @csrf
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <input type="hidden" class="edit-person-id" name="id">
                                                            <label> First Name</label>
                                                            <input type="text" class="form-control edit-person-first"
                                                                placeholder="First Name" name="first_name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Middle Name</label>
                                                            <input type="text" class="form-control edit-person-middle"
                                                                placeholder="Middle Name" name="middle_name">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Last Name</label>
                                                            <input type="text" class="form-control edit-person-last"
                                                                placeholder="Last Name" name="last_name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Email</label>
                                                            <input type="text" class="form-control edit-person-email"
                                                                placeholder="Enter Email" name="email" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Mobile </label>
                                                            <input type="text" class="form-control edit-person-mob"
                                                                placeholder="Mobile" name="mobile">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Phone </label>
                                                            <input type="text" class="form-control edit-person-phone"
                                                                placeholder="Phone" name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Department</label>
                                                            <input type="text" class="form-control edit-person-depart"
                                                                placeholder="Department" name="department">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> City</label>
                                                            <input type="text" class="form-control edit-person-city"
                                                                placeholder="City" name="city">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> State</label>
                                                            <input type="text" class="form-control edit-person-state"
                                                                placeholder="State" name="state">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Designation</label>
                                                            <input type="text"
                                                                class="form-control edit-person-designation"
                                                                placeholder="Designation" name="designation">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Zip Code</label>
                                                            <input type="text" class="form-control edit-person-zip"
                                                                placeholder="Zip Code" name="zip">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Website</label>
                                                            <input type="text" class="form-control edit-person-web"
                                                                placeholder="Website" name="website">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Facebook</label>
                                                            <input type="text" class="form-control edit-person-fb"
                                                                placeholder="Facebook" name="facebook">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label> Twitter</label>
                                                            <input type="text" class="form-control edit-person-tw"
                                                                placeholder="Twitter" name="twitter">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label> Address</label>
                                                            <textarea class="form-control edit-person-addr"
                                                                placeholder="Address.." name="address"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="box-footer pull-right">
                                                <button type="submit" class="btn btn-success update-person-btn"><i
                                                        class="fa fa-refresh"></i> Update</button>

                                                <button type="button" class="btn btn-primary"
                                                    data-dismiss="modal">Close</button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Update Party Modal -->
                </div>

            </div>
        </div>
    </div>
</section>

@endsection

@section('extra-js-files')
@endsection