@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Sale Reports Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('sales-list')}}">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Customer Name</label>
                                    <select class="form-control select2 w-100" name="client_id">
                                        <option value=""> -- Select Customer -- </option>
                                        @foreach($clients as $key => $client)
                                        <option value="{{$client->id}}">{{$client->user->first_name.'
                                            '.$client->user->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice No</label>
                                    <input type="text" class="form-control" placeholder="Invoice No" name="invoice_no"
                                        value={{app('request')->input('invoice_no')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice Date</label>
                                    <input type="date" class="form-control product-code" name="invoice_date"
                                        value={{app('request')->input('invoice_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Delivery Date</label>
                                    <input type="date" class="form-control" name="delivery_date"
                                        value={{app('request')->input('delivery_date')}}>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="value={{app('request')->input('to')}}">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('sales-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Sale Reports Listing</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="example2">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Invoice</th>
                                <th>Customer</th>
                                <th>Profit</th>
                                <th>Loss</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $profit = 0;
                                $loss = 0;
                            ?>
                            @forelse($salesReport as $key => $sale)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>
                                    <a href="{{route('single-invoice-detail',$sale->invoice_id)}}" target="_blank">{{$sale->invoice_id}}</a>
                                </td>
                                <td>{{$sale->client->user->first_name.' '.$sale->client->user->last_name}}</td>
                                <td>{{(!is_null($sale->profit)) ? $sale->profit : '0.0' }}</td>
                                <td>{{(!is_null($sale->loss)) ? $sale->loss : '0.0' }}</td>
                            </tr>
                            <?php
                                $profit += $sale->profit;
                                $loss += $sale->loss;
                            ?>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot class="bg-success">
                            <tr>
                                <th rowspan="1" colspan="3" class="text-right">Total: </th>
                                <th rowspan="1" colspan="1">{{$profit}}</th>
                                <th rowspan="1" colspan="1">{{$loss}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>

        </div>
    </div>
</section>

@endsection