@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Short Stock</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('short-stock-listing')}}"><i class="fa fa-list"></i> Short Stock Listing</a></li>
    </ol>
</section>

<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Short Stock Listing</h3>
            <div class="box-tools">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <form action="{{route('add-short-stock')}}" method="post">
                @csrf
                <table id="example2" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Product</th>
                            <th>P.Price</th>
                            <th>M.Stock</th>
                            <th>Required Stock</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($shortStocks as $key => $shortStock)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>
                                <input type="hidden" name="product_id[]" value="{{$shortStock->product->id}}">
                                {{$shortStock->product->product_name}}
                            </td>
                            <td>{{$shortStock->product->purchase_price}}</td>
                            <td>
                                <input type="hidden" name="old_stock[]" value="{{$shortStock->master_stock}}">
                                {{$shortStock->master_stock}}
                            </td>
                            <td>
                                <input class="form-control required-stock" type="number" name="required_stock[]" min="1"
                                    value="{{old('required_stock')}}">
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="color: red"><b>No Record</b></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <button type="submit" class="btn btn-primary required-stock-btn" disabled>
                                    <i class="fa fa-plus"></i> Submit </button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
        <!-- /.box-body -->

    </div>
</section>

@endsection