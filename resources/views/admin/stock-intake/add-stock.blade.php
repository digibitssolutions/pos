@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Stock Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('stock-intake-list')}}"><i class="fa fa-list"></i> Stocks List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <div id="message" class="alert alert-success alert-dismissible" style="display: none">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p></p>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4">
                    <label>Select Party:</label><strong style="color: red">*</strong>
                    <select class="form-control stock-intak-party select2" name="party_id" required>
                        <option value="" selected> -- Select Party --</option>
                        @foreach($parties as $key => $party)
                        <option value="{{$party->id}}">
                            {{$party->party_name}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-2">
                    <label>Bill No.</label><strong class="text-danger">*</strong>
                    <input type="text" name="bill_no" class="form-control" id="purchase-bill-no" placeholder="Bill No.">
                </div>
                <div class="col-lg-3">
                    <label>Bill Date:</label>
                    <input type="date" name="bill_date" class="form-control" id="purchase-bill-date"
                        value="{{date('Y-m-d', strtotime('now'))}}">
                </div>
                <div class="col-lg-3">
                    <label>Payment Type:</label><strong style="color: red">*</strong>
                    <select class="form-control" required id="purchase-payment">
                        <option value> -- Payment Type --</option>
                        <option value="cash" selected> Cash </option>
                        <option value="credit"> Credit </option>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-lg-4">
                    <label>Paid Amount:</label><strong class="text-danger">*</strong>
                    <input type="number" name="paid_amount" class="form-control" placeholder="Paid Amount"
                        id="purchase-paid-amount">
                </div>
            </div>
            <h3 class="box-title">Add Stock</h3>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr class="bg-dark">
                            <th width="500">Product Name</th>
                            <th>P.Price</th>
                            <th>Sale Price</th>
                            <th>M.STOCK</th>
                            <th>STOCK</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <tr>
                            <td>
                                <select class="form-control select-product-name select2" id="stock-product"
                                    name="product_id">
                                    <option value> -- Select Product --</option>
                                    @foreach($products as $key => $product)
                                    <option value="{{$product->id}}">
                                        {{$product->product_name}}
                                    </option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="hidden" class="form-control" id="stock-product-name">
                                <input type="number" class="form-control product-purchase-price" placeholder="P.Price">
                            </td>
                            <td>
                                <input type="number" class="form-control product-sale-price" placeholder="Sale Price">
                            </td>
                            <td>
                                <input type="number" class="form-control product-master-stock" placeholder="M.Stock"
                                    name="master_stock" readonly>
                            </td>
                            <td>
                                <input type="number" class="form-control product-stock" placeholder="Stock"
                                    name="stock">
                            </td>
                            <td>
                                <button type="button" class="btn btn-success" id="add-stock" disabled>
                                    <i class="fa fa-plus"></i>
                                    Add
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-body table-responsive">
            <h3 class="box-title pull-left">Reciept</h3>
            <span class="pull-right">
                <label for="switch">Enable/Disable</label>
                <br>
                <label class="switch stock-switch">
                    <input type="checkbox" id="switch">
                    <span class="slider round"></span>
                </label>
            </span>
            <form action="{{route('store-stock-intake')}}" method="post" enctype="multipart/form-data">
                @csrf
                <table class="table table-hover table-bordered" id="receipt_bill">
                    <thead class="bg-success">
                        <tr>
                            <th>No.</th>
                            <th>ITEM DETAILS</th>
                            <th>M.STOCK</th>
                            <th>STOCK</th>
                            <th>PRICE</th>
                            <th>QTY</th>
                            <th>AMOUNT</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody class="tbody" id="new-stock">
                        <input type="hidden" name="party_id" class="reciept-party">
                        <input type="hidden" name="bill_no" class="purchase-bill-no">
                        <input type="hidden" name="bill_date" class="purchase-bill-date">
                        <input type="hidden" name="payable" class="purchase-payable">
                        <input type="hidden" name="payment_type" class="purchase-payment">
                        <input type="hidden" name="received_amount" class="purchase-paid-amount">
                        <input type="hidden" name="module_id" value="11">
                    </tbody>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="text-right bg-danger">
                            <h5><strong>Gross Total: </strong></h5>
                        </td>
                        <td class="text-center text-danger bg-danger">
                            <h5 class="stock-payment" style="font-size:20px;font-weight: 700"><strong> 0.0</strong>
                            </h5>
                        </td>
                    </tr>
                </table>
                <div>
                    <label>Upload File:</label>
                    <input type="file" name="file" class="form-control">
                </div>
                <br>
                <button type="submit" class="btn btn-success" id="save-stock" disabled="true"><i class="fa fa-plus"></i>
                    Save</button>
            </form>
        </div>
    </div>
</section>
@endsection