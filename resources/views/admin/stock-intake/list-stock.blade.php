@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Stock Intake</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('create-stock-intake')}}"><i class="fa fa-plus"></i> Add Stock</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary filters collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title text-danger"><strong>Filters</strong></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-plus"></i></button>
            </div>
        </div>
        <form method="GET" action="{{route('stock-intake-list')}}">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="party_id"> Party</label>
                            <select class="select2" name="party_id">
                                <option value=""> -- Select Party --</option>
                                @foreach($parties as $party)
                                <option value="{{$party->id}}">{{$party->party_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Bill No</label>
                            <input type="text" class="form-control" placeholder="Bill No" name="bill_no"
                                value={{app('request')->input('bill_no')}}>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Bill Date</label>
                            <input type="date" class="form-control" name="bill_date"
                                value={{app('request')->input('bill_date')}}>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="pay"> Payment Type</label>
                            <select name="payment_type" id="pay" class="form-control">
                                <option value="cash">Cash</option>
                                <option value="credit">Credit</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> From Date</label>
                            <input type="date" class="form-control" name="from" value={{app('request')->input('from')}}>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Until Date</label>
                            <input type="date" class="form-control" name="to" value={{app('request')->input('to')}}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                    Filter</button>
                <a href="{{route('stock-intake-list')}}" class="btn btn-success"><i class="fa fa-fw fa-paper-plane"></i>
                    Clear</a>
            </div>
        </form>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Stock Listing</h3>
            <div class="box-tools">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>Party</th>
                        <th>Bill No.</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Remaining</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($totalStock as $key => $stock)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$stock->party->party_name}}</td>
                        <td>{{$stock->bill_no}}</td>
                        <td>
                            <span class="badge" style="background: #28a745">
                                {{$stock->total_amount}}
                            </span>
                        </td>
                        <td>
                            <span class="badge" style="background: #007bff">
                                {{!is_null($stock->received_amount) ? $stock->received_amount : '0.00'}}
                            </span>
                        </td>
                        <td>
                            <span class="badge badge-danger" style="background: #dc3545">
                                {{$stock->remaining_amount}}
                            </span>
                        </td>
                        <td>{{$stock->bill_date}}</td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="{{route('show-stock', $stock->id)}}">
                                <i class="fa fa-eye"></i>
                                view
                            </a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="color: red"><b>No Record</b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="text-right">
                {{$totalStock->links()}}
            </div>
        </div>
        <!-- /.box-body -->

    </div>
</section>

@endsection