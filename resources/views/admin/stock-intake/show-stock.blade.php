@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Stock Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('stock-intake-list')}}"><i class="fa fa-list"></i> Stocks List</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Party:</label>
                        <input type="text" class="form-control" readonly
                            value="{{$stock->party->user->first_name.' '.$stock->party->user->last_name}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Bill No:</label>
                        <input type="text" class="form-control" readonly value="{{$stock->bill_no}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Bill Date:</label>
                        <input type="text" class="form-control" readonly value="{{$stock->bill_date}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Payment Type:</label>
                        <input type="text" class="form-control" placeholder="Payment type" readonly
                            value="{{$stock->payment_type}}">
                    </div>
                </div>
            </div>
            <div class="row">
                @if ($media != ' ')    
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>File:</label>
                        <p>
                            <a class="btn btn-danger btn-sm btn-block" href="{{$media->url}}" download="{{$media->media_title}}">Download File <i class="fa fa-download"></i></a>
                        </p>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="box-body">
            <!-- /.box-header -->
            <h3 class="box-title">Intake Stocks</h3>
            <div class="box-body table-responsive">
                <table class="table table-hover" id="receipt_bill">
                    <thead class="bg-success">
                        <tr>
                            <th>No.</th>
                            <th>ITEM DETAILS</th>
                            <th>QTY.</th>
                            <th>PRICE</th>
                            <th>AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        @foreach($stock->intakeStock as $key => $stk)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$stk->product->product_name}}</td>
                            <td>{{$stk->stock}}</td>
                            <td>{{$stk->price}}</td>
                            <td>{{$stk->stock * $stk->price}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tr>
                        <td colspan="3"></td>
                        <td class="text-right bg-danger">
                            <h5><strong>Total: </strong></h5>
                            <h5><strong>Paid: </strong></h5>
                            <h5><strong>Remaining: </strong></h5>
                        </td>
                        <td class="bg-danger">
                            <h5>
                                <kbd>{{$stock->total_amount}}</kbd>
                            </h5>
                            <h5>
                                <kbd>{{!is_null($stock->received_amount) ? $stock->received_amount : '0.00'}}</kbd>
                            </h5>
                            <h5>
                                <kbd>{{$stock->remaining_amount}}</kbd>
                            </h5>
                        </td>
                    </tr>
                </table>
                <a href="{{route('stock-intake-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
@endsection