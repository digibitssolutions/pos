@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Transactions Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @include('includes.message')
            @include('includes.error')
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-danger"><strong>Filters</strong></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
                <form method="GET" action="{{route('invoices-list')}}">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" class="form-control" name="product_id">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice No</label>
                                    <input type="text" class="form-control" placeholder="Invoice No" name="invoice_no"
                                        value={{app('request')->input('invoice_no')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Invoice Date</label>
                                    <input type="date" class="form-control product-code" name="invoice_date"
                                        value={{app('request')->input('invoice_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Delivery Date</label>
                                    <input type="date" class="form-control" name="delivery_date"
                                        value={{app('request')->input('delivery_date')}}>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> From Date</label>
                                    <input type="date" class="form-control" name="from"
                                        value={{app('request')->input('from')}}>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label> Until Date</label>
                                    <input type="date" class="form-control"
                                        name="value={{app('request')->input('to')}}">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i>
                            Filter</button>
                        <a href="{{route('invoices-list')}}" class="btn btn-success"><i
                                class="fa fa-fw fa-paper-plane"></i> Clear</a>
                    </div>
                </form>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Transactions Listing</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="example2">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Invoice Id</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($invoices as $key => $invoice)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$invoice->id}}</td>
                                <td>{{$invoice->client->first_name.' '.$invoice->client->last_name}}</td>
                                <td>{{$invoice->created_at->format('d-m-Y')}}</td>
                                <td>
                                    @if($invoice->transactions->first())
                                    <a class="btn btn-success btn-xs"
                                        href="{{route('single-invoice-transaction',$invoice->id)}}">
                                        <i class="fa fa-eye"></i> View Transactions</a>
                                    @else
                                    <a class="btn btn-primary btn-xs" href="#">
                                        <i class="fa fa-fw fa-bullseye"></i> Empty</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="color: red"><b>No Record</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>

        </div>
    </div>
</section>

@endsection