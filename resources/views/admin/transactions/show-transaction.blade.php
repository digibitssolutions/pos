@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Transaction Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('invoice-transaction-list')}}"><i class="fa fa-cubes"></i> Transactions List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    @include('includes.error')
    <p id="message"></p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Customer Name</label>
                        <input type="text" class="form-control" readonly
                            value="{{$client['client']['first_name'].' '.$client['client']['last_name']}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Invoice No</label>
                        <input type="text" class="form-control" readonly
                            value="{{$transactions[0]['invoice']['invoice_no']}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Invoice Date</label>
                        <input type="date" class="form-control" readonly
                            value="{{$transactions[0]['invoice']['invoice_date']}}">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label> Delivery Date</label>
                        <input type="date" class="form-control" readonly
                            value="{{$transactions[0]['invoice']['delivery_date']}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label> Subject</label>
                        <input type="text" class="form-control" placeholder="Subject" readonly
                            value="{{$transactions[0]['invoice']['subject']}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <!-- /.box-header -->
            <h3 class="box-title">Transaction Listing</h3>
            <div class="box-body table-responsive">
                <table class="table table-hover" id="receipt_bill">
                    <thead class="bg-success">
                        <tr>
                            <th>No.</th>
                            <th>TYPE</th>
                            <th>CREDIT</th>
                            <th>DEBIT</th>
                            <th>VIA</th>
                            <th>CONTACT</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        @php
                        $tr = 0;
                        @endphp
                        @foreach($transactions as $key => $transaction)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>
                                @switch($transaction->transaction_type_id)
                                @case(1)
                                {{'Cash'}}
                                @break
                                @case(2)
                                {{'Credit'}}
                                @break
                                @case(3)
                                {{'Debit'}}
                                @break
                                @default
                                {{'--'}}
                                @endswitch
                            </td>

                            <td>{{$transaction->credit ? $transaction->credit : '0.0'}}</td>
                            <td>{{$transaction->debit ? $transaction->debit : '0.0'}}</td>
                            <td>{{$transaction->payment_via ? $transaction->payment_via : '--'}}</td>
                            <td>{{$transaction->payment_via_mobile ? $transaction->payment_via_mobile : '--'}}</td>
                            <td>{{$transaction->created_at}}</td>
                        </tr>
                        @php
                        $tr += $transaction->credit;
                        @endphp
                        @endforeach
                    </tbody>
                    <tr class="mt-5">
                        <td class="bg-info"></td>
                        <td class="bg-info"></td>
                        <td class="bg-info">
                            <h5><strong>Received = {{$tr}}</strong></h5>
                        </td>
                        <td></td>
                        <td> </td>
                        <td class="bg-danger">
                            <h5><strong>Gross Total: </strong></h5>
                        </td>
                        <td class="text-center text-danger bg-danger">
                            <h5 id="totalPayment" style="font-size:20px;font-weight: 700">
                                <strong>{{$transaction->invoice->total_amount}}</strong>
                            </h5>
                        </td>
                    </tr>
                </table>
                <br>
                <a href="{{route('invoice-transaction-list')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>
                    Back</a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
@endsection