@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1>Profile Section</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('users-list')}}"><i class="fa fa-list-ul"></i> Users List</a></li>
    </ol>
</section>
<section class="content">
    @include('includes.message')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">User Account Detail</h3>
        </div>
        <form method="POST" action="{{route('store-user')}}">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> User Name</label>
                            <input type="text" class="form-control" required placeholder="User Name" name="name"
                                autofocus="true">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email</label>
                            <input type="email" class="form-control" required placeholder="Enter Email" name="email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Password</label>
                            <input type="text" class="form-control" required placeholder="Enter Password"
                                name="password">
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary  " [disabled]="!UserAccount.valid">Submit</button>
            </div>
        </form>
    </div>
</section>

@endsection