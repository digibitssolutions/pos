@extends('admin.layouts.app')

@section('main-content')

<section class="content-header">
    <h1> All Users </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> <a href="{{route('add-user')}}"><i class="fa fa-plus"></i> Add New</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <form>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> User Name</label>
                            <input type="text" class="form-control" placeholder="User Name" formControlName="name">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Email </label>
                            <input type="email" class="form-control" placeholder="Email" formControlName="email">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> From Date</label>
                            <input type="date" class="form-control" placeholder="From Date" formControlName="from">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label> Until Date</label>
                            <input type="date" class="form-control" placeholder="To Date" formControlName="to">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary  "><i class="fa fa-fw fa-filter"></i> Filter</button>
                <button type="button" class="btn btn-success"><i class="fa fa-fw fa-paper-plane"></i> Clear</button>
            </div>
        </form>
    </div>

    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Users List</h3>
            <div class="box-tools">

            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email Verified</th>
                        <th>Status</th>
                        <th>Created Date</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                    <tr>
                        <td>{{$key + 1 }}</td>
                        <td>{{$user->first_name. ' '.$user->last_name}}</td>
                        <td>{{$user->email}}</td>
                        <td><span
                                class="@if($user->email_verified == 1) label label-success @else label label-danger @endif">
                                {{$user->email_verified == 1 ? 'Verified' : 'Not Verified' }}</span>
                        </td>
                        <td>
                            <span
                                class="@if($user->status == 'Active') label label-success @else label label-danger @endif">{{$user->status}}</span>
                        </td>
                        <td>{{$user->created_at}}</td>
                        {{-- <td>
                            <a href="#" class="btn btn-xs btn-primary  ">View</a>
                            <a href="#" class="btn btn-xs btn-success  ">Edit</a>
                            <a href="#" class="btn btn-xs btn-danger  ">Delete</a>
                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please confirm..</h4>
            </div>
            <div class="modal-body">
                <p><strong class="text-danger">Alert!</strong> Are you sure you want to delete this record.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger">Delete</button>
            </div>
        </div>

    </div>
</div>
@endsection