@if(Session::get('message'))

<div class="alert alert-success alert-dismissible" id="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon fa fa-check"></i>
    {{Session::get('message')}}
</div>

@endif

@if(Session::get('error-message'))

<div class="alert alert-danger alert-dismissible" id="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon fa fa-ban"></i>
    {{Session::get('error-message')}}
</div>

@endif

@section('extra-js-files')
<script>
    $("#alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#alert").slideUp(500);
    });
</script>
@endsection