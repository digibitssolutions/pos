<?php
use Illuminate\Support\Facades\Route;

    Route::post('create-brand', 'BrandsController@store');
    Route::get('brands-list', 'BrandsController@index');
    Route::get('single-brand-detail/{brand_id}', 'BrandsController@show');
    Route::post('update-brand-record', 'BrandsController@update');
    Route::delete('delete-brand-record/{brand_id}', 'BrandsController@destroy');
    Route::get('to-digest-brands', 'BrandsController@toDigest');



?>