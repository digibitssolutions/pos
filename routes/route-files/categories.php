<?php
use Illuminate\Support\Facades\Route;

    Route::post('create-category', 'CategoriesController@store');
    Route::get('categories-list', 'CategoriesController@index');
    Route::get('single-category-detail/{category_id}', 'CategoriesController@show');
    Route::post('update-category-record', 'CategoriesController@update');
    Route::delete('delete-category-record/{category_id}', 'CategoriesController@destroy');
    Route::get('to-digest-categories', 'CategoriesController@toDigest');



?>