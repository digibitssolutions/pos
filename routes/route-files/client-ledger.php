<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('client-ledgers-list', 'ClientLedgerController@index')->name('client-ledgers');
    Route::post('store-client-ledger', 'ClientLedgerController@store')->name('store-client-ledger');
    Route::get('client-ledger-report', 'ClientLedgerController@generateReport')->name('client-ledger-report');
    Route::get('client-invoice/{id?}', 'ClientLedgerController@clientInvoice')->name('client-invoice');

});
