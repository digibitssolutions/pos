<?php

use Illuminate\Support\Facades\Route;
/* ===== Clients Start =====*/

Route::group(['middelware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('clients-list', 'ClientController@clientsList')->name('clients-list');
    Route::get('create-client', 'ClientController@create')->name('create-client');
    Route::post('store-client', 'ClientController@store')->name('store-client');
    Route::post('update-client', 'ClientController@update')->name('update-client');
    
    Route::get('show-client/{id}', 'ClientController@show')->name('show-client');
    Route::get('edit-client/{id}', 'ClientController@edit')->name('edit-client');
    Route::get('client/{id}', 'ClientController@client')->name('client');
    

    Route::get('client-transaction/{id}', 'ClientController@clientTransaction')->name('client-transaction');
    Route::post('store-transaction', 'ClientController@storeClientTransaction')->name('store-client-transaction');
    Route::get('client-account-statement/{id}', 'ClientController@clientAccountStatement')->name('account-statement');
});

/* ===== Clients End =====*/
