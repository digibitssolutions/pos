<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('expenses-list', 'ExpensesController@index')->name('expenses-list');
    Route::get('create-expense', 'ExpensesController@create')->name('create-expense');
    Route::post('store-expense', 'ExpensesController@store')->name('store-expense');
    Route::delete('delete-expense/{id}', 'ExpensesController@destroy')->name('delete-expense');
});
