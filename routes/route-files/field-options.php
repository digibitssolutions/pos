<?php

use Illuminate\Support\Facades\Route;

Route::post('create-field-option_type', 'FieldOptionTypeController@store');
Route::get('field-option_type-list', 'FieldOptionTypeController@index');
Route::post('update-field-type-record', 'FieldOptionTypeController@update');
Route::delete('delete-field-option-type-record/{field_type_id}', 'FieldOptionTypeController@destroy');
