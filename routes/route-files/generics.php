<?php
use Illuminate\Support\Facades\Route;

    Route::get('generic/{type}', 'GenericsController@index');
    Route::post('generic/{type}', 'GenericsController@store');
    Route::get('generic/{type}/{id}', 'GenericsController@show');
    Route::post('generic/{type}/{id}', 'GenericsController@update');
    Route::delete('generic/{type}/{id}', 'GenericsController@destroy');

?>