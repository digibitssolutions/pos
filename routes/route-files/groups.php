<?php
use Illuminate\Support\Facades\Route;

    Route::post('create-group', 'GroupsController@store');
    Route::get('groups-list', 'GroupsController@index');
    Route::get('single-group-detail/{group_id}', 'GroupsController@show');
    Route::post('update-group-record', 'GroupsController@update');
    Route::delete('delete-group-record/{group_id}', 'GroupsController@destroy');
    Route::get('to-digest-groups', 'GroupsController@toDigest');




?>