<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('invoices-list', 'InvoicesController@index')->name('invoices-list');
    Route::get('update-status', 'InvoicesController@changeStatus')->name('update-status');
    Route::get('create-invoice', 'InvoicesController@create')->name('create-invoice');
    Route::post('store-invoice', 'InvoicesController@store')->name('store-invoice');
    Route::get('single-invoice-detail/{invoice_id}', 'InvoicesController@show')->name('single-invoice-detail');
    Route::get('edit-invoice-record/{invoice_id}', 'InvoicesController@edit')->name('edit-invoice-record');
    Route::post('update-invoice-record/{id}', 'InvoicesController@update')->name('update-invoice-record');
    Route::delete('delete-invoice-record/{invoice_id}', 'InvoicesController@destroy')->name('delete-invoice');
    Route::get('print-to-invoice/{id}', 'InvoicesController@printInvoice')->name('print-to-invoice');
    Route::get('get-last-product/{cId}/{pId}', 'InvoicesController@getLastProductPrice')->name('get-last-product');
    Route::get('invoice-return', 'InvoicesController@invoiceReturn')->name('invoice-return');
    Route::post('store-invoice-return', 'InvoicesController@storeInvoiceReturn')->name('store-invoice-return');


    Route::get('print-invoice/{id}', 'InvoicesController@saveInvoicePDF')->name('print-invoice');
    Route::get('single-product/{product_id}', 'InvoicesController@singleProduct')->name('single-product');
    Route::get('sales-list', 'InvoicesController@salesList')->name('sales-list');
    Route::post('delete-product/{id}', 'InvoicesController@deleteInoiveProduct')->name('delete-product');
});
