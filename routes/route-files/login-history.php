<?php
use Illuminate\Support\Facades\Route;

    Route::get('history-list', 'LoginHistoryController@index');
    Route::get('delete-all-history', 'LoginHistoryController@destroy');
    Route::get('delete-selected-history', 'LoginHistoryController@deleteSelectedHistory');

?>