<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'administrator'], function () {
    Route::post('store-file', 'MediaDriveController@store')->name('store-file');
    Route::get('show-file', 'MediaDriveController@show')->name('show-file');
    Route::get('user/files', 'MediaDriveController@fetchMediaByEntities');
    Route::delete('delete/{module_id}/{relation_id}/{media_id}', 'MediaDriveController@destroy');
});
