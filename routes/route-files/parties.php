<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('parties-list', 'PartyController@index')->name('parties-list');
    Route::get('create-party', 'PartyController@create')->name('create-party');
    
    Route::post('store-party', 'PartyController@store')->name('store-party');

    Route::get('show-party/{id}', 'PartyController@show')->name('show-party');

    Route::get('edit-party/{id}', 'PartyController@edit')->name('edit-party');
    Route::post('update-party', 'PartyController@update')->name('update-party');
    // Route::delete('delete-expense/{id}', 'ExpensesController@destroy')->name('delete-expense');
});
