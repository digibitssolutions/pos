<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('products-list', 'ProductsController@index')->name('products-list');
    Route::get('paginate', 'ProductsController@fetchPagination');

    Route::get('create-product', 'ProductsController@create')->name('add-product');
    Route::post('store-product', 'ProductsController@store')->name('store-product');
    Route::get('single-product-detail/{id}', 'ProductsController@show')->name('show-product');
    Route::get('edit-product-record/{id}', 'ProductsController@edit')->name('edit-product');
    Route::post('update-product-record', 'ProductsController@update')->name('update-product');
    Route::delete('delete-product/{id}', 'ProductsController@destroy')->name('delete-product');
    Route::get('to-digest-products', 'ProductsController@toDigest');
    Route::get('current_product_name/{code}', 'ProductsController@getProductNameId');
    Route::post('update-product-price/{id}/{price}/{price2}', 'ProductsController@updateProductById')->name('update-product-price');
});

