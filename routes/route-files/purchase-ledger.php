<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('ledgers-list', 'PurchaseLedgerController@index')->name('ledgers-list');
    Route::post('store-ledger', 'PurchaseLedgerController@store')->name('store-ledger');
    Route::get('purchase-ledger-report', 'PurchaseLedgerController@generateReport')->name('purchase-ledger-report');
});
