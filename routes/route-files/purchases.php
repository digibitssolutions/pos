<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('purchases-list', 'PurchaseController@index')->name('purchases-list');
    Route::get('create-purchase', 'PurchaseController@create')->name('create-purchase');
    Route::post('store-purchase', 'PurchaseController@store')->name('store-purchase');
    Route::get('show-purchase/{id}', 'PurchaseController@show')->name('show-purchase');
    Route::get('edit-purchase/{id}', 'PurchaseController@edit')->name('edit-purchase');
    Route::post('update-purchase/{id}', 'PurchaseController@update')->name('update-purchase');
    Route::get('add-purchase-transaction/{id}', 'PurchaseController@purchaseTransaction')->name('add-purchase-transaction');
    Route::post('store-purchase-transaction', 'PurchaseController@storePurchaseTransaction')->name('store-purchase-transaction');
    // Route::delete('delete-expense/{id}', 'ExpensesController@destroy')->name('delete-expense');
});
