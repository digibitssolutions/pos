<?php

use Illuminate\Support\Facades\Route;

Route::prefix('administrator')->group(function () {
    Route::get('reports', 'ReportsController@index')->name('reports');
    
    Route::get('products-report', 'ReportsController@productsReport')->name('products-report');
    Route::get('product-generate-report', 'ReportsController@productsReportGenerate')->name('product-generate-report');
    Route::get('clients-report', 'ReportsController@clientsReport')->name('clients-report');
    Route::get('clients-generate-report', 'ReportsController@clientsReportGenerate')->name('clients-generate');

    Route::get('short-stock-report', 'ReportsController@shortStockReport')->name('short-stock-report');
    Route::get('short-generate-report', 'ReportsController@shortReportGenerate')->name('short-generate-report');

    Route::get('products-audit', 'ReportsController@productsAudit')->name('products-audit');
    Route::get('products-audit-generate', 'ReportsController@productsAuditReportGenerate')->name('products-audit-generate');

    Route::get('invoices-list-report', 'ReportsController@getInvoices')->name('invoices-list-report');
    Route::get('supply-report', 'ReportsController@supplyReport')->name('supply-report');
});
