<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('sale-person-ledger', 'SalePersonLedgerController@index')->name('sale-person-ledger');
    Route::post('store-sale-person-ledger', 'SalePersonLedgerController@store')->name('store-sale-person-ledger');
});
