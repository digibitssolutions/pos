<?php

use Illuminate\Support\Facades\Route;
/* ===== SalesPerson Start =====*/

Route::group(['middelware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('sales-person-list', 'SalesPersonController@index')->name('sales-person-list');  
    Route::get('show-sales-person/{id}', 'SalesPersonController@show')->name('show-sales-person');
    Route::post('store-sales-person', 'SalesPersonController@store')->name('store-sales-person');
    Route::post('update-sales-person', 'SalesPersonController@update')->name('update-sales-person');
    
    Route::get('create-sales-person', 'SalesPersonController@create')->name('create-sales-person');
    Route::get('edit-sales-person/{id}', 'SalesPersonController@edit')->name('edit-sales-person');
    Route::delete('delete-sales-person/{id}', 'SalesPersonController@destroy')->name('delete-sales-person');
});

/* ===== SalesPerson End =====*/
