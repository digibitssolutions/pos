<?php
use Illuminate\Support\Facades\Route;

    Route::post('create-shop-account', 'ShopsController@store');
    Route::get('shops-list', 'ShopsController@index');
    Route::get('single-shop-detail/{shop_id}', 'ShopsController@show');
    Route::post('update-shop-account', 'ShopsController@update');
    Route::delete('delete-shop-account/{shop_id}', 'ShopsController@destroy');



?>