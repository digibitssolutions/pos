<?php

use Illuminate\Support\Facades\Route;

Route::prefix('administrator')->group(function () {
    Route::get('short-stock', 'ShortStockController@index')->name('short-stock');
    Route::post('short-stock', 'ShortStockController@store')->name('add-short-stock');
    Route::get('short-stock-listing', 'ShortStockController@shortStockListing')->name('short-stock-listing');
});
