<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::group(['prefix' => 'stock'], function () {
        Route::get('show/{id}', 'StockIntakeController@show')->name('show-stock');
        Route::get('list', 'StockIntakeController@index')->name('stock-intake-list');
    });
    Route::get('create-stock-intake', 'StockIntakeController@create')->name('create-stock-intake');
    Route::post('store-stock-intake', 'StockIntakeController@store')->name('store-stock-intake');
    Route::post('update-stock-intake-record', 'StockIntakeController@update')->name('update-stock-intake');
    Route::delete('delete-stock-intake-record/{stock_intake_id}', 'StockIntakeController@destroy')->name('delete-stock-intake');
    Route::get('single-stock-intake-product/{product_id}', 'StockIntakeController@singleStockIntakeProduct');
});
