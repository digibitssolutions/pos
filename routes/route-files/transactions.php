<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'prefix' => 'administrator'], function () {
    Route::get('invoice-transaction-list', 'TransactionController@index')->name('invoice-transaction-list');
    Route::get('create-invoice-transaction/{invoice_id}', 'TransactionController@create')->name('create-invoice-transaction');
    Route::post('store-invoice-transaction', 'TransactionController@store')->name('store-invoice-transaction');
    Route::get('single-invoice-transactions/{invoice_id}', 'TransactionController@show')->name('single-invoice-transaction');
});
