<?php

use Illuminate\Support\Facades\Route;

// Route::prefix('admin')->group(function () {
Route::get('users-list', 'UsersController@index')->name('users-list');
Route::get('create-user-account', 'UsersController@create')->name('add-user');
Route::get('edit-user', 'UsersController@edit')->name('edit-user');
Route::post('store-user-account', 'UsersController@store')->name('store-user');
Route::post('update-user-record', 'UsersController@update');
Route::delete('delete-user-record/{user_id}', 'UsersController@destroy');
Route::post('create-admin-account', 'UsersController@addAdmin')->name('add-amdin');
// });
