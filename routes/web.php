<?php

use App\Http\Controllers\Clients\Models\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Products\Models\Product;
use App\Http\Controllers\Invoices\Models\Invoice;
use App\Http\Controllers\Invoices\Models\SaleReport;
use App\Http\Controllers\Parties\Models\Party;

use function App\shopId;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    $start = new Carbon('first day of last month');
    $end = new Carbon('last day of last month');

    $products = Product::where('shop_id', shopId())->count();
    $invoices = Invoice::where('shop_id', shopId())->count();
    $clients = Client::where('shop_id', shopId())->count();
    $suppliers = Party::where('shop_id', shopId())->count();
    $sales = Invoice::where('shop_id', shopId())->where('deleted_at', null)->sum('total_amount');
    $lastMonthSale = Invoice::where('shop_id', shopId())->whereDate('created_at', '>=', $start->startOfMonth())->whereDate('created_at', '<=', $end->endOfMonth())->sum('total_amount');
    $totalProfit = SaleReport::where('shop_id', shopId())->sum('profit');
    $lastMonthProf = SaleReport::where('shop_id', shopId())->whereDate('created_at', '>=', $start->startOfMonth())->whereDate('created_at', '<=', $end->endOfMonth())->sum('profit');
    return view('admin.dashboard', compact('products', 'invoices', 'totalProfit', 'clients', 'suppliers', 'sales', 'lastMonthSale', 'lastMonthProf'));
})->name('dashboard')->middleware('auth');

Route::get('administrator/log-activities', 'LogActivities\LogsController@logActivities')->name('log-activities');
Route::get('administrator/log-detail/{log_id}', 'LogActivities\LogsController@showLogDetail')->name('log-detail');
Route::get('administrator/recent-activities', 'LogActivities\LogsController@index')->name('recent-activities');

require __DIR__ . '/auth.php';
